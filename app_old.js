
var express = require('express'), mongoose = require('mongoose'), fs = require('fs'), models_path = __dirname + '/mongodb/models';
var multer = require('multer');
var timezone = require('moment-timezone');
var https = require('https');
var json2csv = require('nice-json2csv');


var walk = function(path) {
	fs.readdirSync(path).forEach(function(file) {
		var newPath = path + '/' + file;
		var stat = fs.statSync(newPath);
		if (stat.isFile()) {
			if (/(.*)\.(js$|coffee$)/.test(file)) {
				require(newPath);
			}
		} else if (stat.isDirectory()) {
			walk(newPath);
		}
	});
};
walk(models_path);



var cors = require('cors'), corsOptions = {
	origin : true,
	credentials : true
}, config = require('./config'), passport = require('passport'), site = require('./site'), oauth2 = require('./oauth2'), token = require('./token'), https = require('https'), path = require('path'), dbToConnect = process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || (config.db.dbUrl + config.db.dbName), port = Number(process.env.PORT || 3000);


var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var morgan = require('morgan');


//Pull in the mongo store if we're configured to use it
//else pull in MemoryStore for the session configuration
console.log('Using MemoryStore for the Session');
var MemoryStore = session.MemoryStore;
sessionStorage = new MemoryStore();

mongoose.connect(dbToConnect);
var db = mongoose.connection.on('error', function() {
	throw new Error('unable to connect to database at ' + process.env.MONGOHQ_URL);
});

global.db = db;
// Express configuration
var app = express();  
app.use(cors(corsOptions));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.set('view engine', 'ejs');
app.use(morgan('dev'));
app.use(multer());

app.use(bodyParser.urlencoded({
	extended : false
}));

app.use(bodyParser.json());
app.use(methodOverride());

//Session Configuration
app.use(session({
	resave : true,
	saveUninitialized : true,
	secret : config.session.secret,
	store : sessionStorage,
	key : "authorization.sid",
	cookie : {
		maxAge : config.session.maxAge
	}
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(json2csv.expressDecorator);  
// Catch all for error messages.  Instead of a stack
// trace, this will log the json of the error message
// to the browser and pass along the status with it
app.use(function(err, req, res, next) {
	if (err) {
		res.status(err.status);
		res.json(err);
	} else {
		next();
	}
});

// Passport configuration
require('./auth');

require('./config/routes')(app, site, oauth2, token);

//static resources for stylesheets, images, javascript files
app.use(express.static(path.join(__dirname, 'public')));

//TODO: Change these for your own certificates.  This was generated
//through the commands:
//openssl genrsa -out privatekey.pem 1024
//openssl req -new -key privatekey.pem -out certrequest.csr
//openssl x509 -req -in certrequest.csr -signkey privatekey.pem -out certificate.pem
/*var options = {
	key : fs.readFileSync('certs/privatekey.pem'),
	cert : fs.readFileSync('certs/certificate.pem')
};
*/




var dgram = require("dgram");
var udpserver = dgram.createSocket("udp4");
udpserver.on("message", function (msg, rinfo) {

  var Status = mongoose.model('Status');
  var Event = mongoose.model('Event');
  var Device = mongoose.model('Device');
  var Parameter = mongoose.model('Parameter');
  var async = require('async');
  console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + " Data received from " + rinfo.address + ":" + rinfo.port);

  if(msg.length > 0){
  	var i = 2;
  	var id = "";
  	var sim = "";
  	var ns = "";
  	var ew = "";
    var operator = "";
    var device_name = "";
    var firmware = "";
    var apn = "";
    var ip_address = "";
    var send_int = 0;
  	var timetx = timezone();
  	var nrip = 0;
  	var timev = timezone();
  	var msg_len = 0;
  	var timefix = timezone();
  	var gpsx = 0.0;
  	var gpsy = 0.0;
  	var gpsz = 0.0;
  	var gpsd = 0.0;
  	var gpsst = 0;
  	var gpsstime = timezone();
  	var gpssat = 0;
  	var pdop = 0;
    var hdop = 0;
    var vdop = 0;
  	var gpsvel = 0.0;
  	var gsmlvl = 0;
  	var gsmst = 0;
  	var gsmstime = timezone();
  	var gsmmod = 0;
  	var accx = 0;
  	var accy = 0;
  	var accz = 0;
  	var tiltx = 0;
  	var tilty = 0;
  	var tiltz = 0;
    var acczflt = 0;
    var accxflt = 0;
    var accyflt = 0;
    var accxsqmflt = 0;    
    var accysqmflt = 0;    
    var acczsqmflt = 0;
  	var temp = 0;
  	var keyst = 0;
  	var keytime = timezone();
  	var pwrst = 0;
  	var pwrstime = timezone();
  	var almst = 0;
  	var almstime = timezone();
  	var blkst = 0;
  	var blkstime = timezone();
  	var neventi = 0;
  	var crc16 = 0;
    var response = 0;
  	var status = {};

    var end_of_msg = 0;
    var bytes_num = 0;
    for(var cont = msg.length; end_of_msg == 0 ;cont--){
        if((msg[cont-1]<<8) + msg[cont] == config.constants.EOT)
        {
            end_of_msg = cont;
        }

    }
    bytes_num = parseInt((end_of_msg-2));
  
    var msg_crc = new Buffer((bytes_num));
    msg.copy(msg_crc,0,0,((bytes_num)+1));
    // OLD PACKET
	if((msg[0]<<8) + msg[1] == config.constants.STX){
    	 while(i<msg.length){
        switch(i){
            case 2:
                while(msg[i] != 0)
                {
                   id += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 24;
              console.log("ID: "+id);
            break;
            
            case 24:
              while(msg[i] != 0)
                {
                   sim += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=40;
              
              //console.log("SIM: "+sim);
            break;
            
            case 40:            
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timetx = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              //timetx = timezone().clone().startOf('day').add(ms,'milliseconds').tz('Europe/Rome').format('DD/MM/YYYY HH:mm');
              i = i+4;
              //console.log("TIMTX: "+timetx);
              status.timetx = timetx;
            break;
            
            case 44:
              nrip = (msg[i]<<8) + msg[i+1];
              i = i+2;
              //console.log("NRIP: "+nrip);
              status.nrip = nrip;
            break;
            
            case 46:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timev = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("TIMEV: "+timev);
              status.timev = timev;
            break;
            
            case 50:
              while(msg[i] != 0)
                {
                   device_name += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=66;
              
              console.log("DEVICE_NAME: "+device_name);
              status.device_name = device_name;
            break;

            case 66:
              while(msg[i] != 0)
                {
                   firmware += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=71;
              
              //console.log("firmware_version: "+firmware);
              status.firmware_version = firmware;
            break;

            case 71:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              send_int = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("SEND INT: "+send_int);
              status.send_int = send_int;
            break;

            case 75:
               while(msg[i] != 0)
                {
                   apn += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=107;
              
              //console.log("APn: "+apn);
              status.apn = apn;
            break;

            case 107:
              msg_len = (msg[i]<<8) + msg[i+1];
              i = i+2;
              //console.log("LEN: "+msg_len);
            break;
            
            case 109:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timefix = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("TIMEFIX: "+timefix);
              status.timefix = timefix;
            break;
            
            case 113:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              //gpsx = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              gpsx = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              ns = String.fromCharCode(msg[i]);
              i = i+1;
             // console.log("GPSX: "+gpsx + " N/S: "+ns);
              status.gpsx = gpsx;
              status.ns = ns;
            break;
            
            case 118:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              //gpsx = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              gpsy = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsy = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
            
              ew = String.fromCharCode(msg[i]);
              i = i+1;
              //console.log("GPSY: "+gpsy + " E/W: "+ew);
              status.ew = ew;
              status.gpsy = gpsy;
            break;
            
            case 123:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              gpsz = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsz = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];
              i = i+4;
              //console.log("GPSZ: "+gpsz);
              status.gpsz = gpsz;
            break;
            
            case 127:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              gpsd = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("GPSD: "+gpsd);
              status.gpsd = gpsd;
            break;
            
            case 131:
              gpsst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("GPSST: "+gpsst);
              status.gpsst = gpsst;
            break;
            
            case 133:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              gpsstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("GPSSTIME: "+gpsstime);
              status.gpsstime = gpsstime;
            break;
            
            case 137:
              gpssat = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GPSSAT: "+gpssat);
              status.gpssat = gpssat;
            break;
            
            case 139:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              pdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Pdop: "+pdop);
              status.pdop = pdop;
            break;

            case 143:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              hdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Hdop: "+hdop);
              status.hdop = hdop;
            break;

            case 147:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              vdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Vdop: "+vdop);
              status.vdop = vdop;
            break;
            
            case 151:
              gpsvel = msg[i];  
              i = i+1;
              //console.log("GPSVEL: "+gpsvel);
              status.gpsvel = gpsvel;
            break;
            
            case 152:
              gsmlvl = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GSMLVL: "+gsmlvl);
              status.gsmlvl = gsmlvl;
            break;
            
            case 154:
              gsmst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("GSMST: "+gsmst);
              status.gsmst = gsmst;
            break;
            
            case 156:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              gsmstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("GSMSTIME: "+gsmstime);
              status.gsmstime = gsmstime;
            break;

            case 160:
              while(msg[i] != 0)
                {
                   operator += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 180;
             // console.log("GSMOPN: "+operator);
              status.gsmopn = operator;
            break;
            
            case 180:
              gsmmod = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GSMMOD: "+gsmmod);
              status.gsmmod = gsmmod;
            break;

            case 182:
               while(msg[i] != 0)
                {
                   ip_address += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 198;
              //console.log("IP Address: "+ip_address);
              status.ip_address = ip_address;
            break;
            
            case 198:
              accx = (msg[i] << 8) + msg[i+1];  
              if(accx > 32767)
              {
                accx = two_complement(accx);
              }
              i = i+2;
              //console.log("accXist: "+accx);
              status.accxist = accx;
            break;
            
            case 200:
              accy = (msg[i] << 8) + msg[i+1];
              if(accy > 32767)
              {
                accy = two_complement(accy);
              }
              i = i+2;
              //console.log("accYist: "+accy);
              status.accyist = accy;
            break;
            
            case 202:
              accz = (msg[i] << 8) + msg[i+1];  
              if(accz > 32767)
              {
                accz = two_complement(accz);
              }
              i = i+2;
              //console.log("accZist: "+accz);
              status.acczist = accz;
            break;
            
            case 204:
              tiltx = (msg[i] << 8) + msg[i+1];
              if(tiltx > 32767)
              {
                tiltx = two_complement(tiltx);
              }
              i = i+2;
              //console.log("accXsqm: "+tiltx);
              status.accxsqm = tiltx;
            break;
            
            case 206:
              tilty = (msg[i] << 8) + msg[i+1]; 
              if(tilty > 32767)
              {
                tilty = two_complement(tilty);
              }
              i = i+2;
              //console.log("accYsqm: "+tilty);
              status.accysqm = tilty;
            break;
            
            case 208:
              tiltz = (msg[i] << 8) + msg[i+1];
              if(tiltz > 32767)
              {
                tiltz = two_complement(tiltz);
              } 
              i = i+2;
              //console.log("accZsqm: "+tiltz);
              status.acczsqm = tiltz;
            break;

            case 210:
              accxflt = (msg[i] << 8) + msg[i+1]; 
              if(accxflt > 32767)
              {
                accxflt = two_complement(accxflt);
              }
              i = i+2;
              //console.log("accXflt: "+accxflt);
              status.accxflt = accxflt;
            break;
            
            case 212:
              accyflt = (msg[i] << 8) + msg[i+1]; 
              if(accyflt > 32767)
              {
                accyflt = two_complement(accyflt);
              }
              i = i+2;
              //console.log("accYflt: "+accyflt);
              status.accyflt = accyflt;
            break;
            
            case 214:
              acczflt = (msg[i] << 8) + msg[i+1]; 
              if(acczflt > 32767)
              {
                acczflt = two_complement(acczflt);
              }
              i = i+2;
              //console.log("accZflt: "+acczflt);
              status.acczflt = acczflt;
            break;

            case 216:
              accxsqmflt = (msg[i] << 8) + msg[i+1]; 
              if(accxsqmflt > 32767)
              {
                accxsqmflt = two_complement(accxsqmflt);
              }
              i = i+2;
              //console.log("accXsqmflt: "+accxsqmflt);
              status.accxsqmflt = accxsqmflt;
            break;
            
            case 218:
              accysqmflt = (msg[i] << 8) + msg[i+1]; 
              if(accysqmflt > 32767)
              {
                accysqmflt = two_complement(accysqmflt);
              }
              i = i+2;
              //console.log("accYsqmflt: "+accysqmflt);
              status.accysqmflt = accysqmflt;
            break;
            
            case 220:
              acczsqmflt = (msg[i] << 8) + msg[i+1];
              if(acczsqmflt > 32767)
              {
                acczsqmflt = two_complement(acczsqmflt);
              } 
              i = i+2;
              //console.log("accZsqmflt: "+acczsqmflt);
              status.acczsqmflt = acczsqmflt;
            break;
            
            case 222:
              temp = (msg[i] << 8) + msg[i+1];  
              
              i = i+2;
              //console.log("TEMP: "+temp);
              status.temp = parseFloat((parseFloat(temp)-620)*330/4096);
            break;
            
            case 224:
              keyst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("KEYST: "+keyst);
              status.keyst = keyst;
            break;
            
            case 226:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              keytime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("KEYTIME: "+keytime);
              status.keytime = keytime;
            break;
            
            case 230:
              pwrst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("PWRST: "+pwrst);
              status.pwrst = pwrst;
            break;
            
            case 232:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              pwrstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("PWRSTIME: "+pwrstime);
              status.pwrstime = pwrstime;
            break;
            
            case 236:
              almst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("ALSMT: "+almst);
              status.almst = almst;
            break;
            
            case 238:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              almstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("ALMSTIME: "+almstime);
              status.almstime = almstime;
            break;
            
            case 242:
              blkst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("BLKST: "+blkst);
              status.blkst = blkst;
            break;
            
            case 244:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              blkstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
             // console.log("BLKSTIME: "+blkstime);
              status.blkstime = blkstime;
            break;
            
            case 248:
              var event = {};
              var codev = (msg[i] << 8) + msg[i+1];
              i = i+2;
              
              var numdati = (msg[i] << 8) + msg[i+1];
              i = i+2;
              
              var cntdati = 0;
              for(var dati = 0; dati<numdati; dati++){
                var tipo = "";
                for(var j = i; j < i+2; j++) {
                    tipo += String.fromCharCode(msg[j]);
                }
                
                i=i+2;
                switch(tipo){
                  case "I2":
                  case "I4":
                  case "R4":
                  case "R8":
                    var numdata = {};
                    event.numdata = [];
                    numdata.name = "";
                    
                    numdata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    
                    if(tipo == "I2"){
                      numdata.data = (msg[i] << 8) + msg[i+1];
                      i=i+2;
                    }
                    if(tipo == "I4" || tipo == "R4"){
                      numdata.data = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
                      i=i+4;
                    }
                    if(tipo == "R8"){
                      numdata.data = (msg[i] << 52) + (msg[i+1] << 48) + (msg[i+2] << 40) + (msg[i+3] << 32) + (msg[i+4] << 24)+(msg[i+5] << 16) + (msg[i+6] << 8) + msg[i+7];
                      i=i+8;
                    }
                    
                    event.numdata.push(numdata);
                  break;
                  
                  case "TM":
                    var timedata = {};
                    event.timedata = [];
                    timedata.name = "";
                    timedata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    var year = 2000 + (msg[i] >> 2);
                    var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
                    var day = (msg[i+1] & 62) >> 1;
                    var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
                    var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
                    var second = (msg[i+3] & 63);
                    
                    timedata.data = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
                    i = i+4;
                    event.timedata.push(timedata);
                  break;
                  
                  case "C2":
                  case "C4":
                  case "C6":
                  case "C8":
                  case "10":
                  case "16":
                  case "20":
                  case "30":
                  case "50":
                    var stringdata = {};
                    event.stringdata = [];
                    stringdata.name = "";
                    stringdata.data = "";
                    stringdata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    
                    if(tipo == "C2"){
                      for(var j = i; j < i+2; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+2;
                    }
                    if(tipo == "C4"){
                      for(var j = i; j < i+4; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+4;
                    }
                    
                    if(tipo == "C6"){
                      for(var j = i; j < i+6; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+6;
                    }
                    
                    if(tipo == "C8"){
                      for(var j = i; j < i+8; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+8;
                    }
                    
                    if(tipo == "10"){
                      for(var j = i; j < i+10; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+10;
                    }
                    
                    if(tipo == "16"){
                      for(var j = i; j < i+16; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+16;
                    }
                    
                    if(tipo == "20"){
                      for(var j = i; j < i+20; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+20;
                    }
                    
                    if(tipo == "30"){
                      for(var j = i; j < i+30; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+30;
                    }
                    
                    if(tipo == "50"){
                      for(var j = i; j < i+50; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+50;
                    }
                    
                    event.stringdata.push(stringdata);
                  break;
                  
                }
                
                if(cntdati++ == numdati-1){
                  crc16 = msg[i++];
                  console.log('crc letto: '+crc16);
                  console.log(msg_crc.length);
                  if (CRC8(msg_crc, msg_crc.length) == crc16)
                  {
                    status.crc16 = 0;
                  }
                  else
                  {
                    status.crc16 = 1;
                  }
                    if((msg[i]<<8) + msg[i+1] == config.constants.EOT){             
                        i = msg.length;
                        event.codevent = codev;
                        event.timetx = timetx;
                        event.timev = timev;
                        event.nrip = nrip;  

                        async.parallel({
                          event: function(callback){
                            Device.findOne({
                                'device_code':id//,
                                //'sim':sim
                              },function(err,device){
                                if(err)
                                {
                                  console.log(err);
                                  callback(null, null);
                                }
                                
                                if(device)
                                {
                                  event.mail = device.mail;
                                  event.mail_sended = false;
                                  event.device_id = device._id;
                                  event.device_name = device.device_name;
                                  event.device_code = id;
                                  event.sim = sim;
                                  Event(event).save(function(err, e){
                                    if(err)
                                    {
                                      callback(null, null);
                                    } 
                                    else
                                    {
                                     
                                      callback(null,e);
                                    }

                                  });
                                
                              }
                              else
                              {
                                callback(null,null);
                              }
                            });
                           },
                            response: function(callback){
                                if(event.codevent == 8888){
                                   // Invio i parametri al device
                                    Device.findOne({
                                        'device_code':id
                                    }, function(err, device){
                                        console.log(device.send_parameter);
                                        console.log(event.codevent);
                                        if(device.send_parameter){
                                            Parameter.findOne({
                                                'device_id': device._id
                                            }, function(err, parameters){
                                                    if(err)
                                                    {
                                                        callback(null, null);
                                                    }   
                                                    var payload = New_Parameter_Payload(parameters);
                                                    udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                        if(err)
                                                         {
                                                            console.log(err);
                                                         }   
                                                         callback(null,null);
                                                    });    
                                            });
                                             
                                        }
                                        else 
                                        {
                                           
                                            var packet = [4];
                                            packet[0] = 8174 >> 8;
                                            packet[1] = 8174 - (packet[0] << 8);
                                            packet[2] = 6355 >> 8;
                                            packet[3] = 6355 - (packet[2] << 8);
                                            var payload = new Buffer(packet);
                                            console.log(packet);
                                            udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                 if(err)
                                                 {
                                                    console.log(err);
                                                 }   
                                                 callback(null,null);
                                            });
                                       
                                           
                                        }
                                        
                                    });
                                }
                                else if(event.codevent == 9999){
                                    Device.findOne({
                                        'device_code':id
                                    }, function(err, device){
                                      console.log(device);
                                       if(device){
                                          Parameter.findOne({
                                                'device_id': device._id
                                            }, function(err, parameters){
                                                    if(err)
                                                    {
                                                        callback(null, null);
                                                    }   
                                                    
                                                    var payload = New_Parameter_Payload(parameters);
                                                    console.log(rinfo.port);
                                                    udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                        if(err)
                                                         {
                                                            console.log(err);
                                                         }   
                                                         callback(null,null);
                                                    });    
                                            });
                                       }
                                            

                                    });
                                }
                                else{
                                    // Invio i parametri al device
                                    Device.findOne({
                                        'device_code':id
                                    }, function(err, device){
                                        console.log(device.send_parameter);
                                        console.log(event.codevent);
                                        if(device.send_parameter){
                                            Parameter.findOne({
                                                'device_id': device._id
                                            }, function(err, parameters){
                                                    if(err)
                                                    {
                                                        callback(null, null);
                                                    }   
                                                    var payload = New_Parameter_Payload(parameters);
                                                    udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                        if(err)
                                                         {
                                                            console.log(err);
                                                         }   
                                                         callback(null,null);
                                                    });    
                                            });
                                             
                                        }
                                        else
                                        {
                                           callback(null,null);
                                        }

                                        
                                    });
                                }
                                
                            }


                        }, function(err, results){
                          if(err)
                          {
                            console.log(err);
                          }

                          if(results.event){
                            
                            status.event_id = results.event._id;
                            Device.findOne({
                              'device_code':id//,
                              //'sim':sim
                            },function(err,device){
                              if(err)
                                console.log(err);
                              else
                              {
                                status.device_id = device._id;
                                status.device_name = device.device_name;
                                //status.device_name = device.device_name;
                                Status(status).save(function(err, stat){
                                  if(err){
                                    console.log(err);
                                  }
                                  
                                   console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD hh:mm:ss') + " Status salvato correttamente con id: "+stat._id);
                                  
                                   
                                });
                              }
                              
                             });
                        }
                        });
                      }     

                  }
                  else
                  {
                    console.log('EOT not received');
                  }
                
              }

            break;
          
        }//switch
      }// while
    }
    else if((msg[0]<<8) + msg[1] == config.constants.STX_NEW)// NEW PACKET
    {

      while(i<msg.length){
        switch(i){
            case 2:
                while(msg[i] != 0)
                {
                   id += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 24;
              console.log("ID: "+id);
            break;
            
            case 24:
              while(msg[i] != 0)
                {
                   sim += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=40;
              
              //console.log("SIM: "+sim);
            break;
            
            case 40:            
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timetx = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              //timetx = timezone().clone().startOf('day').add(ms,'milliseconds').tz('Europe/Rome').format('DD/MM/YYYY HH:mm');
              i = i+4;
              //console.log("TIMTX: "+timetx);
              status.timetx = timetx;
            break;
            
            case 44:
              nrip = (msg[i]<<8) + msg[i+1];
              i = i+2;
              //console.log("NRIP: "+nrip);
              status.nrip = nrip;
            break;
            
            case 46:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timev = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("TIMEV: "+timev);
              status.timev = timev;
            break;
            
            case 50:
              while(msg[i] != 0)
                {
                   device_name += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=66;
              
              console.log("DEVICE_NAME: "+device_name);
              status.device_name = device_name;
            break;

            case 66:
              while(msg[i] != 0)
                {
                   firmware += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=71;
              
              //console.log("firmware_version: "+firmware);
              status.firmware_version = firmware;
            break;

            case 71:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              send_int = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("SEND INT: "+send_int);
              status.send_int = send_int;
            break;

            case 75:
               while(msg[i] != 0)
                {
                   apn += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i=107;
              
              //console.log("APn: "+apn);
              status.apn = apn;
            break;

            case 107:
              msg_len = (msg[i]<<8) + msg[i+1];
              i = i+2;
              //console.log("LEN: "+msg_len);
            break;
            
            case 109:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              timefix = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("TIMEFIX: "+timefix);
              status.timefix = timefix;
            break;
            
            case 113:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              //gpsx = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              gpsx = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              ns = String.fromCharCode(msg[i]);
              i = i+1;
             // console.log("GPSX: "+gpsx + " N/S: "+ns);
              status.gpsx = gpsx;
              status.ns = ns;
            break;
            
            case 118:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              //gpsx = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              gpsy = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsy = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
            
              ew = String.fromCharCode(msg[i]);
              i = i+1;
              //console.log("GPSY: "+gpsy + " E/W: "+ew);
              status.ew = ew;
              status.gpsy = gpsy;
            break;
            
            case 123:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              gpsz = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsz = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];
              i = i+4;
              //console.log("GPSZ: "+gpsz);
              status.gpsz = gpsz;
            break;
            
            case 127:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              gpsd = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("GPSD: "+gpsd);
              status.gpsd = gpsd;
            break;
            
            case 131:
              gpsst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("GPSST: "+gpsst);
              status.gpsst = gpsst;
            break;
            
            case 133:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              gpsstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("GPSSTIME: "+gpsstime);
              status.gpsstime = gpsstime;
            break;
            
            case 137:
              gpssat = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GPSSAT: "+gpssat);
              status.gpssat = gpssat;
            break;
            
            case 139:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              pdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Pdop: "+pdop);
              status.pdop = pdop;
            break;

            case 143:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              hdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Hdop: "+hdop);
              status.hdop = hdop;
            break;

            case 147:
              var bytes = [4];
              bytes[0] = msg[i+3];
              bytes[1] = msg[i+2];
              bytes[2] = msg[i+1];
              bytes[3] = msg[i];
              
              vdop = decodeFloat(bytes, 1, 8, 23, -126, 127, true);
              //gpsd = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
              i = i+4;
              //console.log("Vdop: "+vdop);
              status.vdop = vdop;
            break;
            
            case 151:
              gpsvel = msg[i];  
              i = i+1;
              //console.log("GPSVEL: "+gpsvel);
              status.gpsvel = gpsvel;
            break;
            
            case 152:
              gsmlvl = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GSMLVL: "+gsmlvl);
              status.gsmlvl = gsmlvl;
            break;
            
            case 154:
              gsmst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("GSMST: "+gsmst);
              status.gsmst = gsmst;
            break;
            
            case 156:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              gsmstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("GSMSTIME: "+gsmstime);
              status.gsmstime = gsmstime;
            break;

            case 160:
              while(msg[i] != 0)
                {
                   operator += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 180;
             // console.log("GSMOPN: "+operator);
              status.gsmopn = operator;
            break;
            
            case 180:
              gsmmod = (msg[i] << 8) + msg[i+1];  
              i = i+2;
              //console.log("GSMMOD: "+gsmmod);
              status.gsmmod = gsmmod;
            break;

            case 182:
               while(msg[i] != 0)
                {
                   ip_address += String.fromCharCode(msg[i]);
                   i++;
                }
              
              i = 198;
              //console.log("IP Address: "+ip_address);
              status.ip_address = ip_address;
            break;
            
            case 198:
              accx = (msg[i] << 8) + msg[i+1];  
              if(accx > 32767)
              {
                accx = two_complement(accx);
              }
              i = i+2;
              //console.log("accXist: "+accx);
              status.accxist = accx;
            break;
            
            case 200:
              accy = (msg[i] << 8) + msg[i+1];
              if(accy > 32767)
              {
                accy = two_complement(accy);
              }
              i = i+2;
              //console.log("accYist: "+accy);
              status.accyist = accy;
            break;
            
            case 202:
              accz = (msg[i] << 8) + msg[i+1];  
              if(accz > 32767)
              {
                accz = two_complement(accz);
              }
              i = i+2;
              //console.log("accZist: "+accz);
              status.acczist = accz;
            break;
            
            case 204:
              tiltx = (msg[i] << 8) + msg[i+1];
              if(tiltx > 32767)
              {
                tiltx = two_complement(tiltx);
              }
              i = i+2;
              //console.log("accXsqm: "+tiltx);
              status.accxsqm = tiltx;
            break;
            
            case 206:
              tilty = (msg[i] << 8) + msg[i+1]; 
              if(tilty > 32767)
              {
                tilty = two_complement(tilty);
              }
              i = i+2;
              //console.log("accYsqm: "+tilty);
              status.accysqm = tilty;
            break;
            
            case 208:
              tiltz = (msg[i] << 8) + msg[i+1];
              if(tiltz > 32767)
              {
                tiltz = two_complement(tiltz);
              } 
              i = i+2;
              //console.log("accZsqm: "+tiltz);
              status.acczsqm = tiltz;
            break;

            case 210:
              accxflt = (msg[i] << 8) + msg[i+1]; 
              if(accxflt > 32767)
              {
                accxflt = two_complement(accxflt);
              }
              i = i+2;
              //console.log("accXflt: "+accxflt);
              status.accxflt = accxflt;
            break;
            
            case 212:
              accyflt = (msg[i] << 8) + msg[i+1]; 
              if(accyflt > 32767)
              {
                accyflt = two_complement(accyflt);
              }
              i = i+2;
              //console.log("accYflt: "+accyflt);
              status.accyflt = accyflt;
            break;
            
            case 214:
              acczflt = (msg[i] << 8) + msg[i+1]; 
              if(acczflt > 32767)
              {
                acczflt = two_complement(acczflt);
              }
              i = i+2;
              //console.log("accZflt: "+acczflt);
              status.acczflt = acczflt;
            break;

            case 216:
              accxsqmflt = (msg[i] << 8) + msg[i+1]; 
              if(accxsqmflt > 32767)
              {
                accxsqmflt = two_complement(accxsqmflt);
              }
              i = i+2;
              //console.log("accXsqmflt: "+accxsqmflt);
              status.accxsqmflt = accxsqmflt;
            break;
            
            case 218:
              accysqmflt = (msg[i] << 8) + msg[i+1]; 
              if(accysqmflt > 32767)
              {
                accysqmflt = two_complement(accysqmflt);
              }
              i = i+2;
              //console.log("accYsqmflt: "+accysqmflt);
              status.accysqmflt = accysqmflt;
            break;
            
            case 220:
              acczsqmflt = (msg[i] << 8) + msg[i+1];
              if(acczsqmflt > 32767)
              {
                acczsqmflt = two_complement(acczsqmflt);
              } 
              i = i+2;
              //console.log("accZsqmflt: "+acczsqmflt);
              status.acczsqmflt = acczsqmflt;
            break;
            
            case 222:
              temp = (msg[i] << 8) + msg[i+1];  
              
              i = i+2;
              //console.log("TEMP: "+temp);
              status.temp = parseFloat((parseFloat(temp)-620)*330/4096);
            break;
            
            case 224:
              keyst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("KEYST: "+keyst);
              status.keyst = keyst;
            break;
            
            case 226:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              keytime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("KEYTIME: "+keytime);
              status.keytime = keytime;
            break;
            
            case 230:
              pwrst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("PWRST: "+pwrst);
              status.pwrst = pwrst;
            break;
            
            case 232:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              pwrstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("PWRSTIME: "+pwrstime);
              status.pwrstime = pwrstime;
            break;
            
            case 236:
              almst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("ALSMT: "+almst);
              status.almst = almst;
            break;
            
            case 238:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              almstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
              //console.log("ALMSTIME: "+almstime);
              status.almstime = almstime;
            break;
            
            case 242:
              blkst = (msg[i] << 8) + msg[i+1]; 
              i = i+2;
              //console.log("BLKST: "+blkst);
              status.blkst = blkst;
            break;
            
            case 244:
              var year = 2000 + (msg[i] >> 2);
              var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
              var day = (msg[i+1] & 62) >> 1;
              var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
              var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
              var second = (msg[i+3] & 63);
              
              blkstime = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
              i = i+4;
             // console.log("BLKSTIME: "+blkstime);
              status.blkstime = blkstime;
            break;
            
            case 248:
              var event = {};
              var codev = (msg[i] << 8) + msg[i+1];
              i = i+2;
              
              var numdati = (msg[i] << 8) + msg[i+1];
              i = i+2;
              
              var cntdati = 0;
              for(var dati = 0; dati<numdati; dati++){
                var tipo = "";
                for(var j = i; j < i+2; j++) {
                    tipo += String.fromCharCode(msg[j]);
                }
                
                i=i+2;
                switch(tipo){
                  case "I2":
                  case "I4":
                  case "R4":
                  case "R8":
                    var numdata = {};
                    event.numdata = [];
                    numdata.name = "";
                    
                    numdata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    
                    if(tipo == "I2"){
                      numdata.data = (msg[i] << 8) + msg[i+1];
                      i=i+2;
                    }
                    if(tipo == "I4" || tipo == "R4"){
                      numdata.data = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];  
                      i=i+4;
                    }
                    if(tipo == "R8"){
                      numdata.data = (msg[i] << 52) + (msg[i+1] << 48) + (msg[i+2] << 40) + (msg[i+3] << 32) + (msg[i+4] << 24)+(msg[i+5] << 16) + (msg[i+6] << 8) + msg[i+7];
                      i=i+8;
                    }
                    
                    event.numdata.push(numdata);
                  break;
                  
                  case "TM":
                    var timedata = {};
                    event.timedata = [];
                    timedata.name = "";
                    timedata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    var year = 2000 + (msg[i] >> 2);
                    var month = ((msg[i] & 3) << 2) + (msg[i+1] >> 6);
                    var day = (msg[i+1] & 62) >> 1;
                    var hour = ((msg[i+1] & 1) << 4) + (msg[i+2] >> 4);
                    var minute = ((msg[i+2] & 15) << 2) + (msg[i+3] >> 6);
                    var second = (msg[i+3] & 63);
                    
                    timedata.data = timezone(new Date(year, month-1, day, hour, minute, second)).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
                    i = i+4;
                    event.timedata.push(timedata);
                  break;
                  
                  case "C2":
                  case "C4":
                  case "C6":
                  case "C8":
                  case "10":
                  case "16":
                  case "20":
                  case "30":
                  case "50":
                    var stringdata = {};
                    event.stringdata = [];
                    stringdata.name = "";
                    stringdata.data = "";
                    stringdata.name = (msg[i] << 8) + msg[i+1];
                    
                    i=i+2;
                    
                    if(tipo == "C2"){
                      for(var j = i; j < i+2; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+2;
                    }
                    if(tipo == "C4"){
                      for(var j = i; j < i+4; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+4;
                    }
                    
                    if(tipo == "C6"){
                      for(var j = i; j < i+6; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+6;
                    }
                    
                    if(tipo == "C8"){
                      for(var j = i; j < i+8; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+8;
                    }
                    
                    if(tipo == "10"){
                      for(var j = i; j < i+10; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+10;
                    }
                    
                    if(tipo == "16"){
                      for(var j = i; j < i+16; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+16;
                    }
                    
                    if(tipo == "20"){
                      for(var j = i; j < i+20; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+20;
                    }
                    
                    if(tipo == "30"){
                      for(var j = i; j < i+30; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+30;
                    }
                    
                    if(tipo == "50"){
                      for(var j = i; j < i+50; j++) {
                          stringdata.data += String.fromCharCode(msg[j]);
                      }
                      i=i+50;
                    }
                    
                    event.stringdata.push(stringdata);
                  break;
                  
                }
                
                if(cntdati++ == numdati-1){
                  crc16 = (msg[i] << 24)+(msg[i+1] << 16) + (msg[i+2] << 8) + msg[i+3];
                 
                    i = i+4;
                    if((msg[i]<<8) + msg[i+1] == config.constants.EOT){             
                        i = msg.length;
                        event.codevent = codev;
                        event.timetx = timetx;
                        event.timev = timev;
                        event.nrip = nrip;  

                        async.parallel({
                          event: function(callback){
                            Device.findOne({
                                'device_code':id//,
                                //'sim':sim
                              },function(err,device){
                                if(err)
                                {
                                  console.log(err);
                                  callback(null, null);
                                }
                                
                                if(device)
                                {
                                  event.mail = device.mail;
                                  event.mail_sended = false;
                                  event.device_id = device._id;
                                  event.device_name = device.device_name;
                                  event.device_code = id;
                                  event.sim = sim;
                                  Event(event).save(function(err, e){
                                    if(err)
                                    {
                                      callback(null, null);
                                    } 
                                    else
                                    {
                                     
                                      callback(null,e);
                                    }

                                  });
                                
                              }
                              else
                              {
                                callback(null,null);
                              }
                            });
                           },
                            response: function(callback){
                                if(event.codevent != 9999){
                                    // Invio i parametri al device
                                    Device.findOne({
                                        'device_code':id
                                    }, function(err, device){
                                        if(device.send_parameter){
                                            Parameter.findOne({
                                                'device_id': device._id
                                            }, function(err, parameters){
                                                    if(err)
                                                    {
                                                        callback(null, null);
                                                    }   
                                                    //console.log(parameters);
                                                    var payload = Parameter_Payload(parameters);
                                                    udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                        if(err)
                                                         {
                                                            console.log(err);
                                                         }   
                                                         callback(null,null);
                                                    });    
                                            });
                                             
                                        }
                                        else
                                        {
                                           
                                          callback(null,null);
                                           
                                        }

                                        
                                    });
                                }
                                else
                                {
                                    Device.findOne({
                                        'device_code':id
                                    }, function(err, device){
                                       
                                            Parameter.findOne({
                                                'device_id': device._id
                                            }, function(err, parameters){
                                                    if(err)
                                                    {
                                                        callback(null, null);
                                                    }   
                                                    //console.log(parameters);
                                                    var payload = Parameter_Payload(parameters);
                                                    udpserver.send(payload,0,payload.length,rinfo.port,rinfo.address, function(err, bytes){
                                                        if(err)
                                                         {
                                                            console.log(err);
                                                         }   
                                                         callback(null,null);
                                                    });    
                                            });
                                             
                                        
                                        

                                        
                                    });
                                }
                                
                            }


                        }, function(err, results){
                          if(err)
                          {
                            console.log(err);
                          }

                          if(results.event){
                            
                            status.event_id = results.event._id;
                            Device.findOne({
                              'device_code':id//,
                              //'sim':sim
                            },function(err,device){
                              if(err)
                                console.log(err);
                              else
                              {
                                status.device_id = device._id;
                                status.device_name = device.device_name;
                                //status.device_name = device.device_name;
                                Status(status).save(function(err, stat){
                                  if(err){
                                    console.log(err);
                                  }
                                  
                                   console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD hh:mm:ss') + " Status salvato correttamente con id: "+stat._id);
                                  
                                   
                                });
                              }
                              
                             });
                        }
                        });
                      }     

                  }
                  else
                  {
                    console.log('EOT not received');
                  }
                
              }

            break;
          
        }//switch
      }// while
    }
    else if((msg[0]<<8) + msg[1] == config.constants.STX_RES)// RESPONSE PACKET
    {
        while(i<msg.length){
            switch(i){
                case 2:
                    while(msg[i] != 0)
                    {
                         id += String.fromCharCode(msg[i]);
                         i++;
                         
                    }
                    console.log('ID: '+id);
                    i = 24;
                break;
                
                case 24:
                    while(msg[i] != 0)
                    {
                         sim += String.fromCharCode(msg[i]);
                         i++;
                         
                    }
                    console.log('SIM: '+sim);
                    i=40;
                    
                break;
                
                case 40:    
                    response = msg[i];
                    i++
                    console.log('response: '+response);
                break;

                case 41:
                    console.log(msg[i]<<8 + msg[i+1]);
                    if((msg[i]<<8) + msg[i+1] == config.constants.EOT){ 
                        i = msg.length; 
                        if(response == 1){
                             Device.update({
                                    'device_code':id
                                },{
                                    '$set':{
                                        'send_parameter': false
                                    }
                                },function(err,device_updated){
                                      if(err)
                                        {
                                            console.log(err);
                                        }
                                        else
                                        {
                                            console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD hh:mm:ss') + " Parametri ricevuti con successo dal device");  
                                        }
                                });
                        }
                        else
                        {
                            console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD hh:mm:ss') + " Parametri NON ricevuti correttamente dal device");  
                                      
                        }
                    }
                    else
                    {
                         console.log('Parametro EOT non trovato');
                         i = msg.length;
                    } 
                break;

            }
        }
    }
    else
    {
        console.log("Errore nel pacchetto. Manca STX");
    }
  }
 
});
udpserver.on("listening", function () {
  var address = udpserver.address();
  console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + "Udp Server listening on port " + address.port);
});
udpserver.bind(3001);


function FourByte(val, index, bytes)
{
  var input = val; 
  var binArray = new Array(32);
  var temp;
  var exp;
  var length;
  var position;
  var i;
  var b3="";
  var b2="";
  var b1="";
  var b0="";

  input=input*1.0;

  if(input<0)
  {
    binArray[31]=1;
    input = input* (-1);
  }
  else
    binArray[31]=0;

  temp= input;
  exp=0;

  if(temp>0)
  {
    while(temp>2)
    {
      temp = temp/2;
      exp++;
    }
  }
  else
  {
    while(temp<1)
    {
      temp = temp*2;
      exp--;
    }
  }

  exp = exp + 127;

  exp=exp.toString(2)
  length=exp.length+1;
  position = 0;

  for(i=30; i>=23; i--)
  {
    if(position<length-1)
      binArray[i]=exp.charAt(position);
    else
     binArray[i]=0;

    position++;
  }

  temp=temp-1;
  for(i=22; i>=0; i--)
  {
    temp=temp*2;
    if(temp>=1)
    {
      binArray[i]=1;
      temp = temp - 1;
    }
    else
     binArray[i]=0;

  }

  b3="";  //byte3
  b2="";  //byte2
  b1="";  //byte1
  b0="";  //byte0
  for(i=31;i>=24;i--)
  {
    b3= b3 + binArray[i].toString(2);
  }
  for(i=23;i>=16;i--)
  {
    b2= b2 + binArray[i].toString(2);
  }
  for(i=15;i>=8;i--)
  {
    b1= b1 + binArray[i].toString(2);
  }
  for(i=7;i>=0;i--)
  {
    b0= b0 + binArray[i].toString(2);
  }

  bytes[index++] = parseInt(b0,2);
  bytes[index++] = parseInt(b1,2);
  bytes[index++] = parseInt(b2,2);
  bytes[index++] = parseInt(b3,2);

  return 1;

}


function decodeFloat(bytes, signBits, exponentBits, fractionBits, eMin, eMax, littleEndian) {
  var totalBits = (signBits + exponentBits + fractionBits);
 
  var binary = "";
  for (var i = 0, l = bytes.length; i < l; i++) {
    var bits = bytes[i].toString(2);
    while (bits.length < 8) 
      bits = "0" + bits;
 
    if (littleEndian)
      binary = bits + binary;
    else
      binary += bits;
  }
 
  var sign = (binary.charAt(0) == '1')?-1:1;
  var exponent = parseInt(binary.substr(signBits, exponentBits), 2) - eMax;
  var significandBase = binary.substr(signBits + exponentBits, fractionBits);
  var significandBin = '1'+significandBase;
  var i = 0;
  var val = 1;
  var significand = 0;
 
  if (exponent == -eMax) {
      if (significandBase.indexOf('1') == -1)
          return 0;
      else {
          exponent = eMin;
          significandBin = '0'+significandBase;
      }
  }
 
  while (i < significandBin.length) {
      significand += val * parseInt(significandBin.charAt(i));
      val = val / 2;
      i++;
  }
 
  return sign * significand * Math.pow(2, exponent);
};

function two_complement(num){
  var bNumber = num.toString(2);
  var onesComplement = "";

  for (var i = 0; i < bNumber.length; ++i) {
      var b = bNumber.charAt(i);

      if (b == '0') {
          onesComplement += '1';    
      }
      else {
          onesComplement += '0';
      }
  }

  return ((parseInt(onesComplement,2)+1)*-1);
};

function CRC8(data, len)
{
 var sum = 0;
 var extract = 0x00; 
 var crc = 0x00;
 var i = 0;
 while (len--)
 {
  extract = data[i++];

  for (var tempI = 8; tempI; tempI--)
  {
   sum = (crc^extract) & 0x01;
   crc = crc >> 1;
   if (sum)
    crc = crc^0x8C;

   extract = extract >> 1;
  }
 }

 console.log('Crc: '+crc);
 return crc;
}
function New_Parameter_Payload(param){
    var bytes = [650];
    var events = '';
    var i = 2;
    var now = timezone(new Date());
    var year = (now.format('YYYY') -2000);
    var month = now.format('MM');
    var day = now.format('DD');
    var hour = now.format('HH');
    var minute = now.format('mm');
    var second = now.format('ss');
    var event_enable = param.event_enable;
    var fence_exit = 255;
    var out = 255;
    var neg_dur = 65535;

    // HEADER STX 2 bytes
    bytes[0] = 8172 >> 8;
    bytes[1] = 8172 - (bytes[0] << 8);

    // TIME SERVER 4 bytes
    bytes[i++] = (year << 2) + (month >> 2);
    bytes[i++] = ((month & 3) << 6) + (day << 1) + (hour >> 4);
    bytes[i++] = ((hour & 15) << 4) + (minute >> 2);
    bytes[i++] = ((minute & 3) << 6) + (second << 0);

    //LENGTH MESSAGE
    bytes[i++] = 607 >> 8;
    bytes[i++] = 607 - (bytes[6]<<8)

    // PARAMS FOR EVENT 0 2 bytes
    bytes[i++] = param.min0 >> 8;
    bytes[i++] = param.min0 - (bytes[i-2] << 8);
    bytes[i++] = param.max0 >> 8;
    bytes[i++] = param.max0 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,0) ){
      bytes[i++] = param.dur0 >> 8;
      bytes[i++] = param.dur0 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }
    
   
    // PARAMS FOR EVENT 1 2 bytes
    bytes[i++] = param.min1 >> 8;
    bytes[i++] = param.min1 - (bytes[i-2] << 8);
    bytes[i++] = param.max1 >> 8;
    bytes[i++] = param.max1 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,1) ){
      bytes[i++] = param.dur1 >> 8;
      bytes[i++] = param.dur1 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 2 2 bytes
    bytes[i++] = param.min2 >> 8;
    bytes[i++] = param.min2 - (bytes[i-2] << 8);
    bytes[i++] = param.max2 >> 8;
    bytes[i++] = param.max2 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,2) ){
      bytes[i++] = param.dur2 >> 8;
      bytes[i++] = param.dur2 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 3 2 bytes for parameter
    bytes[i++] = param.min3 >> 8;
    bytes[i++] = param.min3 - (bytes[i-2] << 8);
    bytes[i++] = param.max3 >> 8;
    bytes[i++] = param.max3 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,3) ){
      bytes[i++] = param.dur3 >> 8;
      bytes[i++] = param.dur3 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 4 2 bytes for parameter
    bytes[i++] = param.min4 >> 8;
    bytes[i++] = param.min4 - (bytes[i-2] << 8);
    bytes[i++] = param.max4 >> 8;
    bytes[i++] = param.max4 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,4) ){
      bytes[i++] = param.dur4 >> 8;
      bytes[i++] = param.dur4 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 5 2 bytes for parameter
    bytes[i++] = param.min5 >> 8;
    bytes[i++] = param.min5 - (bytes[i-2] << 8);
    bytes[i++] = param.max5 >> 8;
    bytes[i++] = param.max5 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,5) ){
      bytes[i++] = param.dur5 >> 8;
      bytes[i++] = param.dur5 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 6 2 bytes for parameter
    bytes[i++] = param.min6 >> 8;
    bytes[i++] = param.min6 - (bytes[i-2] << 8);
    bytes[i++] = param.max6 >> 8;
    bytes[i++] = param.max6 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,6) ){
      bytes[i++] = param.dur6 >> 8;
      bytes[i++] = param.dur6 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 7 2 bytes for parameter
    bytes[i++] = param.min7 >> 8;
    bytes[i++] = param.min7 - (bytes[i-2] << 8);
    bytes[i++] = param.max7 >> 8;
    bytes[i++] = param.max7 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,7) ){
      bytes[i++] = param.dur7 >> 8;
      bytes[i++] = param.dur7 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 8 2 bytes for parameter
    bytes[i++] = parseFloat((param.min8*4096/330)+620) >> 8;
    bytes[i++] = parseFloat((param.min8*4096/330)+620) - (bytes[i-2] << 8);
    bytes[i++] = parseFloat((param.max8*4096/330)+620) >> 8;
    bytes[i++] = parseFloat((param.max8*4096/330)+620) - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,8) ){
      bytes[i++] = param.dur8 >> 8;
      bytes[i++] = param.dur8 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 9 2 bytes for parameter
    bytes[i++] = parseFloat((param.min9*4096/330)+620) >> 8;
    bytes[i++] = parseFloat((param.min9*4096/330)+620) - (bytes[i-2] << 8);
    bytes[i++] = parseFloat((param.max9*4096/330)+620) >> 8;
    bytes[i++] = parseFloat((param.max9*4096/330)+620) - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,9) ){
      bytes[i++] = param.dur9 >> 8;
      bytes[i++] = param.dur9 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 10 2 bytes for parameter
    bytes[i++] = param.min10 >> 8;
    bytes[i++] = param.min10 - (bytes[i-2] << 8);
    bytes[i++] = param.max10 >> 8;
    bytes[i++] = param.max10 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,10) ){
      bytes[i++] = param.dur10 >> 8;
      bytes[i++] = param.dur10 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 11 2 bytes for parameter
    bytes[i++] = param.min11 >> 8;
    bytes[i++] = param.min11 - (bytes[i-2] << 8);
    bytes[i++] = param.max11 >> 8;
    bytes[i++] = param.max11 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,11) ){
      bytes[i++] = param.dur11 >> 8;
      bytes[i++] = param.dur11 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 12 2 bytes for parameter
    bytes[i++] = param.min12 >> 8;
    bytes[i++] = param.min12 - (bytes[i-2] << 8);
    bytes[i++] = param.max12 >> 8;
    bytes[i++] = param.max12 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,12) ){
      bytes[i++] = param.dur12 >> 8;
      bytes[i++] = param.dur12 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 13 2 bytes for parameter
    bytes[i++] = param.min13 >> 8;
    bytes[i++] = param.min13 - (bytes[i-2] << 8);
    bytes[i++] = param.max13 >> 8;
    bytes[i++] = param.max13 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,13) ){
      bytes[i++] = param.dur13 >> 8;
      bytes[i++] = param.dur13 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 14 2 bytes for parameter
    bytes[i++] = param.min14 >> 8;
    bytes[i++] = param.min14 - (bytes[i-2] << 8);
    bytes[i++] = param.max14 >> 8;
    bytes[i++] = param.max14 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,14)){
      bytes[i++] = param.dur14 >> 8;
      bytes[i++] = param.dur14 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 15 2 bytes for parameter
    bytes[i++] = param.min15 >> 8;
    bytes[i++] = param.min15 - (bytes[i-2] << 8);
    bytes[i++] = param.max15 >> 8;
    bytes[i++] = param.max15 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,15) ){
      bytes[i++] = param.dur15 >> 8;
      bytes[i++] = param.dur15 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 16 2 bytes for parameter
    bytes[i++] = param.min16 >> 8;
    bytes[i++] = param.min16 - (bytes[i-2] << 8);
    bytes[i++] = param.max16 >> 8;
    bytes[i++] = param.max16 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,16) ){
      bytes[i++] = param.dur16 >> 8;
      bytes[i++] = param.dur16 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 17 2 bytes for parameter
    bytes[i++] = param.min17 >> 8;
    bytes[i++] = param.min17 - (bytes[i-2] << 8);
    bytes[i++] = param.max17 >> 8;
    bytes[i++] = param.max17 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,17) ){
      bytes[i++] = param.dur17 >> 8;
      bytes[i++] = param.dur17 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 18 2 bytes for parameter
    bytes[i++] = param.min18 >> 8;
    bytes[i++] = param.min18 - (bytes[i-2] << 8);
    bytes[i++] = param.max18 >> 8;
    bytes[i++] = param.max18 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,18)){
      bytes[i++] = param.dur18 >> 8;
      bytes[i++] = param.dur18 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 13 2 bytes for parameter
    bytes[i++] = param.min19 >> 8;
    bytes[i++] = param.min19 - (bytes[i-2] << 8);
    bytes[i++] = param.max19 >> 8;
    bytes[i++] = param.max19 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,19) ){
      bytes[i++] = param.dur19 >> 8;
      bytes[i++] = param.dur19 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 20 2 bytes for parameter
    bytes[i++] = param.min20 >> 8;
    bytes[i++] = param.min20 - (bytes[i-2] << 8);
    bytes[i++] = param.max20 >> 8;
    bytes[i++] = param.max20 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,20) ){
      bytes[i++] = param.dur20 >> 8;
      bytes[i++] = param.dur20 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 21 2 bytes for parameter
    bytes[i++] = param.min21 >> 8;
    bytes[i++] = param.min21 - (bytes[i-2] << 8);
    bytes[i++] = param.max21 >> 8;
    bytes[i++] = param.max21 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,21)  ){
      bytes[i++] = param.dur21 >> 8;
      bytes[i++] = param.dur21 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 22 2 bytes for parameter
    bytes[i++] = param.min22 >> 8;
    bytes[i++] = param.min22 - (bytes[i-2] << 8);
    bytes[i++] = param.max22 >> 8;
    bytes[i++] = param.max22 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,22) ){
      bytes[i++] = param.dur22 >> 8;
      bytes[i++] = param.dur22 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 23 2 bytes for parameter
    bytes[i++] = param.min23 >> 8;
    bytes[i++] = param.min23 - (bytes[i-2] << 8);
    bytes[i++] = param.max23 >> 8;
    bytes[i++] = param.max23 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,23) ){
      bytes[i++] = param.dur23 >> 8;
      bytes[i++] = param.dur23 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 24 2 bytes for parameter
    bytes[i++] = param.min24 >> 8;
    bytes[i++] = param.min24 - (bytes[i-2] << 8);
    bytes[i++] = param.max24 >> 8;
    bytes[i++] = param.max24 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,24) ){
      bytes[i++] = param.dur24 >> 8;
      bytes[i++] = param.dur24 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 25 2 bytes for parameter
    bytes[i++] = param.min25 >> 8;
    bytes[i++] = param.min25 - (bytes[i-2] << 8);
    bytes[i++] = param.max25 >> 8;
    bytes[i++] = param.max25 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,25) ){
      bytes[i++] = param.dur25 >> 8;
      bytes[i++] = param.dur25 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 26 2 bytes for parameter
    bytes[i++] = param.min26 >> 8;
    bytes[i++] = param.min26 - (bytes[i-2] << 8);
    bytes[i++] = param.max26 >> 8;
    bytes[i++] = param.max26 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,26) ){
      bytes[i++] = param.dur26 >> 8;
      bytes[i++] = param.dur26 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 27 2 bytes for parameter
    bytes[i++] = param.min27 >> 8;
    bytes[i++] = param.min27 - (bytes[i-2] << 8);
    bytes[i++] = param.max27 >> 8;
    bytes[i++] = param.max27 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,27)){
      bytes[i++] = param.dur27 >> 8;
      bytes[i++] = param.dur27 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 28 2 bytes for parameter
    bytes[i++] = param.min28 >> 8;
    bytes[i++] = param.min28 - (bytes[i-2] << 8);
    bytes[i++] = param.max28 >> 8;
    bytes[i++] = param.max28 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,28) ){
      bytes[i++] = param.dur28 >> 8;
      bytes[i++] = param.dur28 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 29 2 bytes for parameter
    bytes[i++] = param.min29 >> 8;
    bytes[i++] = param.min29 - (bytes[i-2] << 8);
    bytes[i++] = param.max29 >> 8;
    bytes[i++] = param.max29 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,29) ){
      bytes[i++] = param.dur29 >> 8;
      bytes[i++] = param.dur29 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 30 2 bytes for parameter
    bytes[i++] = param.min30 >> 8;
    bytes[i++] = param.min30 - (bytes[i-2] << 8);
    bytes[i++] = param.max30 >> 8;
    bytes[i++] = param.max30 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,30)){
      bytes[i++] = param.dur30 >> 8;
      bytes[i++] = param.dur30 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 31 2 bytes for parameter
    bytes[i++] = param.min31 >> 8;
    bytes[i++] = param.min31 - (bytes[i-2] << 8);
    bytes[i++] = param.max31 >> 8;
    bytes[i++] = param.max31 - (bytes[i-2] << 8);
    if( param.event_enable & Math.pow(2,31) ){
      bytes[i++] = param.dur31 >> 8;
      bytes[i++] = param.dur31 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR EVENT 32 2 bytes for parameter
    if( param.event_enable & Math.pow(2,32)){
      bytes[i++] = param.dur32 >> 8;
      bytes[i++] = param.dur32 - (bytes[i-2] << 8);
    }
    else{
      bytes[i++] = neg_dur >> 8;
      bytes[i++] = neg_dur - (bytes[i-2] << 8);
    }

    // PARAMS FOR FUEL EVENT 33 2 bytes for parameter
    bytes[i++] = (param.max33 ? param.max33 : neg_dur) >> 8;
    bytes[i++] = (param.max33 ? param.max33 : neg_dur) - (bytes[i-2] << 8);


    //OUT Event
    if( (param.event_enable & Math.pow(2,15)) || 
        (param.event_enable & Math.pow(2,16)) ||
        (param.event_enable & Math.pow(2,17)) 
       )
    {
      out = 1;
    }

    //FENCE and EXIT EVENTS
    if( (param.event_enable & Math.pow(2,18)) || 
        (param.event_enable & Math.pow(2,19)) ||
        (param.event_enable & Math.pow(2,20)) 
       )
    {
      if( (param.event_enable & Math.pow(2,21)) || 
             (param.event_enable & Math.pow(2,22)) ||
             (param.event_enable & Math.pow(2,23)) 
       )
      {
        fence_exit = 3;
      }
      else{
          fence_exit = 1;
      }
    }
    else  if( (param.event_enable & Math.pow(2,21)) || 
             (param.event_enable & Math.pow(2,22)) ||
             (param.event_enable & Math.pow(2,23)) 
       )
    {
      fence_exit = 2;
    }

    // JFENCE for P0 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p1_x,i, bytes);
    convert_to_degree_coord(param.p1_y,i, bytes);
    i=i+8;
    // JFENCE for P1 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p2_x,i, bytes);
    convert_to_degree_coord(param.p2_y,i, bytes);
    i=i+8;
    // JFENCE for P2 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p3_x,i, bytes);
    convert_to_degree_coord(param.p3_y,i, bytes);
    i=i+8;
    // JFENCE for P3 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p4_x,i, bytes);
    convert_to_degree_coord(param.p4_y,i, bytes);
    i=i+8;
    // JFENCE for P4 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p5_x,i, bytes);
    convert_to_degree_coord(param.p5_y,i, bytes);
    i=i+8;
     // JFENCE for P5 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p6_x,i, bytes);
    convert_to_degree_coord(param.p6_y,i, bytes);
    i=i+8;
    // JFENCE for P6 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p7_x,i, bytes);
    convert_to_degree_coord(param.p7_y,i, bytes);
    i=i+8;
    // JFENCE for P7 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p8_x,i, bytes);
    convert_to_degree_coord(param.p8_y,i, bytes);
    i=i+8;
    // JFENCE for P8 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p9_x,i, bytes);
    convert_to_degree_coord(param.p9_y,i, bytes);
    i=i+8;
    // JFENCE for P9 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p10_x,i, bytes);
    convert_to_degree_coord(param.p10_y,i, bytes);
    i=i+8;
    // JFENCE for P10 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p11_x,i, bytes);
    convert_to_degree_coord(param.p11_y,i, bytes);
    i=i+8;
    // JFENCE for P11 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p12_x,i, bytes);
    convert_to_degree_coord(param.p12_y,i, bytes);
    i=i+8;
    // JFENCE for P12 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p13_x,i, bytes);
    convert_to_degree_coord(param.p13_y,i, bytes);
    i=i+8;
    // JFENCE for P13 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p14_x,i, bytes);
    convert_to_degree_coord(param.p14_y,i, bytes);
    i=i+8;
    // JFENCE for P14 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p15_x,i, bytes);
    convert_to_degree_coord(param.p15_y,i, bytes);
    i=i+8;
     // JFENCE for P15 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p16_x,i, bytes);
    convert_to_degree_coord(param.p16_y,i, bytes);
    i=i+8;
    // JFENCE for P16 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p17_x,i, bytes);
    convert_to_degree_coord(param.p17_y,i, bytes);
    i=i+8;
    // JFENCE for P17 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p18_x,i, bytes);
    convert_to_degree_coord(param.p18_y,i, bytes);
    i=i+8;
    // JFENCE for P18 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p19_x,i, bytes);
    convert_to_degree_coord(param.p19_y,i, bytes);
    i=i+8;
    // JFENCE for P19 point
    bytes[i++] = fence_exit;
    convert_to_degree_coord(param.p20_x,i, bytes);
    convert_to_degree_coord(param.p20_y,i, bytes);
    i=i+8;
    // JOUT for Q e R 0 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q1_x,i, bytes);
    convert_to_degree_coord(param.q1_y,i, bytes);
    convert_to_degree_coord(param.r1_x,i, bytes);
    convert_to_degree_coord(param.r1_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 1 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q2_x,i, bytes);
    convert_to_degree_coord(param.q2_y,i, bytes);
    convert_to_degree_coord(param.r2_x,i, bytes);
    convert_to_degree_coord(param.r2_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 2 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q3_x,i, bytes);
    convert_to_degree_coord(param.q3_y,i, bytes);
    convert_to_degree_coord(param.r3_x,i, bytes);
    convert_to_degree_coord(param.r3_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 3 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q4_x,i, bytes);
    convert_to_degree_coord(param.q4_y,i, bytes);
    convert_to_degree_coord(param.r4_x,i, bytes);
    convert_to_degree_coord(param.r4_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 4 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q5_x,i, bytes);
    convert_to_degree_coord(param.q5_y,i, bytes);
    convert_to_degree_coord(param.r5_x,i, bytes);
    convert_to_degree_coord(param.r5_y,i, bytes);
    i=i+16;
     // JOUT for Q e R 5 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q6_x,i, bytes);
    convert_to_degree_coord(param.q6_y,i, bytes);
    convert_to_degree_coord(param.r6_x,i, bytes);
    convert_to_degree_coord(param.r6_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 6 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q7_x,i, bytes);
    convert_to_degree_coord(param.q7_y,i, bytes);
    convert_to_degree_coord(param.r7_x,i, bytes);
    convert_to_degree_coord(param.r7_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 7 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q8_x,i, bytes);
    convert_to_degree_coord(param.q8_y,i, bytes);
    convert_to_degree_coord(param.r8_x,i, bytes);
    convert_to_degree_coord(param.r8_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 8 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q9_x,i, bytes);
    convert_to_degree_coord(param.q9_y,i, bytes);
    convert_to_degree_coord(param.r9_x,i, bytes);
    convert_to_degree_coord(param.r9_y,i, bytes);
    i=i+16;
    // JOUT for Q e R 9 point
    bytes[i++] = out;
    convert_to_degree_coord(param.q10_x,i, bytes);
    convert_to_degree_coord(param.q10_y,i, bytes);
    convert_to_degree_coord(param.r10_x,i, bytes);
    convert_to_degree_coord(param.r10_y,i, bytes);
    i=i+16;
    // COMBINED EVENT 0
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 1
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 2
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 3
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 4
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 5
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 6
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 7
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 8
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    // COMBINED EVENT 9
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;
    bytes[i++] = 0;

    //CRC 8
    bytes[i++] = CRC8(bytes, 604);

    // HEADER EOT 2 bytes
    bytes[i++] = 6354 >> 8;
    bytes[i] = 6354 - (bytes[i-1] << 8);
 

    return new Buffer(bytes);

}

function modulo(a, b) {
      return a - Math.floor(a/b)*b;
}
function ToUint8(x) {
    return modulo(ToInteger(x), Math.pow(2, 8));
}
function ToInteger(x) {
    x = Number(x);
    return x < 0 ? Math.ceil(x) : Math.floor(x);
}

function Parameter_Payload(param){
    var str = '';
    var bytes = [];
    var events = '';

    if(param.event_enable == null)
    {
        events = '000';
    } 
    else if(param.event_enable < 10)
    {
        events = '00'+param.event_enable;
    }
    else if(param.event_enable < 100)
    {
        events = '0'+param.event_enable;
    }
    else
    {
        events = param.event_enable;
    }

    str += 'EE:' + events +';';
    str += param.r1_x != null ? ('r01:'+ param.r1_x +'-'+param.r1_y+';') : '';
    str += param.r2_x != null ? ('r02:'+ param.r2_x +'-'+param.r2_y+';') : '';
    str += param.r3_x != null ? ('r03:'+ param.r3_x +'-'+param.r3_y+';') : '';
    str += param.r4_x != null ? ('r04:'+ param.r4_x +'-'+param.r4_y+';') : '';
    str += param.r5_x != null ? ('r05:'+ param.r5_x +'-'+param.r5_y+';') : '';
    str += param.r6_x != null ? ('r06:'+ param.r6_x +'-'+param.r6_y+';') : '';
    str += param.r7_x != null ? ('r07:'+ param.r7_x +'-'+param.r7_y+';') : '';
    str += param.r8_x != null ? ('r08:'+ param.r8_x +'-'+param.r8_y+';') : '';
    str += param.r9_x != null ? ('r09:'+ param.r9_x +'-'+param.r9_y+';') : '';
    str += param.r10_x != null ? ('r10:'+ param.r10_x +'-'+param.r10_y+';') : '';
    str += param.q1_x != null ? ('q01:'+ param.q1_x +'-'+param.q1_y+';') : '';
    str += param.q2_x != null ? ('q02:'+ param.q2_x +'-'+param.q2_y+';') : '';
    str += param.q3_x != null ? ('q03:'+ param.q3_x +'-'+param.q3_y+';') : '';
    str += param.q4_x != null ? ('q04:'+ param.q4_x +'-'+param.q4_y+';') : '';
    str += param.q5_x != null ? ('q05:'+ param.q5_x +'-'+param.q5_y+';') : '';
    str += param.q6_x != null ? ('q06:'+ param.q6_x +'-'+param.q6_y+';') : '';
    str += param.q7_x != null ? ('q07:'+ param.q7_x +'-'+param.q7_y+';') : '';
    str += param.q8_x != null ? ('q08:'+ param.q8_x +'-'+param.q8_y+';') : '';
    str += param.q9_x != null ? ('q09:'+ param.q9_x +'-'+param.q9_y+';') : '';
    str += param.q10_x != null ? ('q10:'+ param.q10_x +'-'+param.q10_y+';') : '';
    str += param.p1_x != null ? ('p01:'+ param.p1_x +'-'+param.p1_y+';') : '';
    str += param.p2_x != null ? ('p02:'+ param.p2_x +'-'+param.p2_y+';') : '';
    str += param.p3_x != null ? ('p03:'+ param.p3_x +'-'+param.p3_y+';') : '';
    str += param.p4_x != null ? ('p04:'+ param.p4_x +'-'+param.p4_y+';') : '';
    str += param.p5_x != null ? ('p05:'+ param.p5_x +'-'+param.p5_y+';') : '';
    str += param.p6_x != null ? ('p06:'+ param.p6_x +'-'+param.p6_y+';') : '';
    str += param.p7_x != null ? ('p07:'+ param.p7_x +'-'+param.p7_y+';') : '';
    str += param.p8_x != null ? ('p08:'+ param.p8_x +'-'+param.p8_y+';') : '';
    str += param.p9_x != null ? ('p09:'+ param.p9_x +'-'+param.p9_y+';') : '';
    str += param.p10_x != null ? ('p10:'+ param.p10_x +'-'+param.p10_y+';') : '';
    str += param.p11_x != null ? ('p11:'+ param.p11_x +'-'+param.p11_y+';') : '';
    str += param.p12_x != null ? ('p12:'+ param.p12_x +'-'+param.p12_y+';') : '';
    str += param.p13_x != null ? ('p13:'+ param.p13_x +'-'+param.p13_y+';') : '';
    str += param.p14_x != null ? ('p14:'+ param.p14_x +'-'+param.p14_y+';') : '';
    str += param.p15_x != null ? ('p15:'+ param.p15_x +'-'+param.p15_y+';') : '';
    str += param.p16_x != null ? ('p16:'+ param.p16_x +'-'+param.p16_y+';') : '';
    str += param.p17_x != null ? ('p17:'+ param.p17_x +'-'+param.p17_y+';') : '';
    str += param.p18_x != null ? ('p18:'+ param.p18_x +'-'+param.p18_y+';') : '';
    str += param.p19_x != null ? ('p19:'+ param.p19_x +'-'+param.p19_y+';') : '';
    str += param.p20_x != null ? ('p20:'+ param.p20_x +'-'+param.p20_y+';') : '';
    console.log('Stringa da inviare: '+str);
    for(var i = 0; i<= str.length-1; i++){
        bytes.push(str.charCodeAt(i));
    }
    return new Buffer(bytes);

}

function convert_to_degree_coord(coord, index, bytes){

  if(coord){
    var ns = 1;
    var coordinata = parseInt(coord);
    var diff = 0.0;
    if(coordinata < 0){
      ns = -1;
      coordinata = coordinata * ns;
      diff = -1*(coord + coordinata);
    }
    else
    {
      diff = coord -coordinata;
    }

    diff = (diff*60).toFixed(4);
    coordinata = coordinata + diff;
    coordinata = coordinata * ns;

    FourByte(parseFloat(coordinata), index , bytes);

  }
  else
  {
    bytes[index++] = 0;
    bytes[index++] = 0;
    bytes[index++] = 0;
    bytes[index++] = 0;
  }

  return 1;

}

/*
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var server = https.createServer({ 
    key: fs.readFileSync('../.ssh/server.key'), cert: fs.readFileSync('../.ssh/server.crt'), ca: fs.readFileSync('../.ssh/ca.crt'), rejectUnauthorized: false }, app).listen(3000, function() { console.log('Rototracer Api listen on Https on port 3000'); });
*/
//FORCE SSL
/*
app.use(function(req, res, next) {
  if(req.headers['x-forwarded-proto']==='http') {
    return res.redirect(['https://', req.get('Host'), req.url].join(''));
  }
  next();
});*/

var server = app.listen(3000, function() {
    console.log('Rototracer Api listen on Https on port 3000');
  
});


server.timeout = 300000;