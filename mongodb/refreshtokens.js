//The refresh tokens.
//You will use these to get access tokens to access your end point data through the means outlined
//in the RFC The OAuth 2.0 Authorization Framework: Bearer Token Usage
//(http://tools.ietf.org/html/rfc6750)

var mongoose = require('mongoose'),
	Token = mongoose.model('Token');

/**
 * Returns a refresh token if it finds one, otherwise returns
 * null if one is not found.
 * @param key The key to the refresh token
 * @param done The function to call next
 * @returns The refresh token if found, otherwise returns null
 */
exports.find = function(key, done) {
	Token.findOne(( {
		"token" : key
	}), function(err, token) {
		if (!err && token) {
			return done(null, token);
		}
		return done(null);
	});
};

/**
 * Saves a refresh token, user id, client id, and scope.
 * @param token The refresh token (required)
 * @param userID The user ID (required)
 * @param clientID The client ID (required)
 * @param scope The scope (optional)
 * @param done Calls this with null always
 * @returns returns this with null
 */
exports.save = function(token, userID, clientID, scope, done) {
	Token({
		"token" : code,
		"clientID" : clientID,
		"userID" : userID,
		"scope" : scope
	}).save(function(err, result) {
		if (err) {
			return done(err);
		} else {
			return done(null);
		}
	});
};

/**
 * Deletes a refresh token
 * @param key The refresh token to delete
 * @param done returns this when done
 */
exports.delete = function(key, done) {
	Token.findOneAndRemove({
		"token" : key
	}, function(err, result) {
		if (err) {
			return done(err, result);
		} else {
			return done(null, result);
		}
	});
};
