var passport = require('passport'), mongoose = require('mongoose'), Client = mongoose.model('Client');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id
 */

exports.index = function(req, res) {
	Client.find({
		'_tenant_id' : req.user._tenant_id
	}, function(err, clients) {
		if (err) {
			return res.status(500).send(err);
		}
		res.send({
			clients : clients
		});
	});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Single Client page searched by id
 */
exports.find = function(req, res) {
	Client.findOne({
		'_tenant_id' : req.user._tenant_id,
		'_id' : req.params.id
	}, function(err, client) {
		if (err) {
			return res.status(500).send(err);
		}
		res.send({
			client : client
		});
	});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New Client
 */
exports.create = function(req, res) {
	req.body.client['_tenant_id'] = req.user._tenant_id;

	Client(req.body.client).save(function(err, client) {
		if (err) {
			return res.status(500).send(err);
		}
		res.send({
			client : client
		});
	});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existent client searched by id
 */
exports.update = function(req, res) {
	Client.findOneAndUpdate({
		'_tenant_id' : req.user._tenant_id,
		'_id' : req.params.id
	}, req.body.client, {'new':true}, function(err, client) {
		if (err) {
			return res.status(500).send(err);
		}
		res.send({
			client : client
		});
	});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete a client
 */
exports.destroy = function(req, res) {
	Client.findOneAndRemove({
		'_tenant_id' : req.user._tenant_id,
		'_id' : req.params.id
	}, function(err, client) {
		if (err) {
			return res.status(500).send(err);
		}
		res.status(200).send('OK');
	});
};
