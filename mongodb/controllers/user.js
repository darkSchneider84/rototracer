var moment = require('moment'), passport = require('passport'), mongoose = require('mongoose'), User = mongoose.model('User');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
	
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific user searched by id
 */
exports.find = function(req, res) {

	
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New user, control if username and mail not yet exist in users collection
 * The new user it will be administrator of the company indicated during registration
 * It will be the only that can invite other collaborators
 * API create a new domain for the new company like [company].beebeeboard.com in AWS Route53
 * Send mail to user for confirm the registration
 */
exports.create = function(req, res) {
	
}; 

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing user searched by id
 * When a user change we must change also all embedded documents related
 */
exports.update = function(req, res) {
	
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing user searched by id
 */
exports.destroy = function(req, res) {
	
	
};


/**
 * 
 * req.authInfo is set using the `info` argument supplied by
 * `BearerStrategy`. It is typically used to indicate scope of the token,
 * and used in access control checks. For illustrative purposes, this
 * example simply returns the scope in the response.
 * 
 */
exports.info = function(req, res) {
	
	res.json({
		user_id : req.user.id,
		name : req.user.name,
		scope : req.authInfo.scope
	});
};

/**
 * 
 * req.authInfo is set using the `info` argument supplied by
 * `BearerStrategy`. It is typically used to indicate scope of the token,
 * and used in access control checks. For illustrative purposes, this
 * example simply returns the scope in the response.
 * 
 */
exports.me = function(req, res) {
	
	res.json({
		user : req.user,
		organization: org
	});

};


