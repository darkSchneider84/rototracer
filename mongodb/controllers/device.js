var moment = require('moment'), 
pg = require('pg'),
format = require('pg-format'), 
config = require('../../config');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index_admin = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT device_lists(null, null, null, null);',
			null);

		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(devices && devices.rows && devices.rows.length > 0){
          	res.json({'devices':devices.rows[0].device_lists});
          }
          else
          {
          	res.status(404).json({});
          }
        });

	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT device_lists(%L,null,null, null);',
			parseInt(req.query.user_id));

		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(devices && devices.rows && devices.rows.length > 0){
          	res.json({'devices':devices.rows[0].device_lists});
          }
          else
          {
          	res.status(404).json({});
          }
        });

	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.find_from_device_bb = function(req, res){
	
	var sql = format('SELECT device_parameter_last_status(%L);',
			parseInt(req.params.id));

		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(devices && devices.rows && devices.rows.length > 0){
      	     	res.status(200).json(devices.rows[0].device_parameter_last_status[0]);
          }
          else
          {
          	res.status(404).json({});
          }
        });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.find_from_iccid = function(req, res) {

//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT device_lists(null,null,%L, null);',
			req.query.device_code);

		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(devices && devices.rows && devices.rows.length > 0){
          	res.json({'devices':devices.rows[0].device_lists});
          }
          else
          {
          	res.status(404).json({});
          }
        });
		
/*	}
	else
	{
		res.status(401).send({});
	}*/
};



/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific device searched by id
 */
exports.find = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT device_lists(%L,null,null, %L);',
			req.query.user_id,
			req.params.id);

		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(devices && devices.rows && devices.rows.length > 0){
          	res.json({'devices':devices.rows[0].device_lists});
          }
          else
          {
          	res.status(404).json({});
          }
        });
		
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.update_from_bb = function(req, res){

	var sql = format('SELECT update_device_from_device_bbb_id(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
		req.params.id,
		null,
		req.body.device.device_beebeeboard_id,
		(req.body.device.device_code && req.body.device.device_code !== null ? req.body.device.device_code.replace(/'/g, "''") : ''),
		(req.body.device.device_name && req.body.device.device_name !== null ? req.body.device.device_name.replace(/'/g, "''") : ''),
		(req.body.device.lotto_produzione && req.body.device.lotto_produzione !== null ? req.body.device.lotto_produzione.replace(/'/g, "''") : ''),
		(req.body.device.mail && req.body.device.mail !== null  ? req.body.device.mail.replace(/'/g, "''") : ''),
		(req.body.device.piano_tariffario && req.body.device.piano_tariffario !== null  ? req.body.device.piano_tariffario.replace(/'/g, "''") : ''),
		req.body.device.send_parameter,
		(req.body.device.sim && req.body.device.sim !== null ? req.body.device.sim.replace(/'/g, "''") : ''),
		(req.body.device.status && req.body.device.status !== null ? req.body.device.status.replace(/'/g, "''") : ''),
		req.body.device.test_accensione,
		req.body.device.user_beebeeboard_id,
		(req.body.device.mail0 && req.body.device.mail0 !== null  ? req.body.device.mail0.replace(/'/g, "''") : ''),
		(req.body.device.mail1 && req.body.device.mail1 !== null  ? req.body.device.mail1.replace(/'/g, "''") : ''),
		(req.body.device.mail2 && req.body.device.mail2 !== null  ? req.body.device.mail2.replace(/'/g, "''") : ''),
		(req.body.device.mail3 && req.body.device.mail3 !== null  ? req.body.device.mail3.replace(/'/g, "''") : ''),
		(req.body.device.mail4 && req.body.device.mail4 !== null  ? req.body.device.mail4.replace(/'/g, "''") : ''),
		(req.body.device.mail5 && req.body.device.mail5 !== null  ? req.body.device.mail5.replace(/'/g, "''") : ''),
		(req.body.device.mail6 && req.body.device.mail6 !== null  ? req.body.device.mail6.replace(/'/g, "''") : ''),
		(req.body.device.mail7 && req.body.device.mail7 !== null  ? req.body.device.mail7.replace(/'/g, "''") : ''),
		(req.body.device.mail8 && req.body.device.mail8 !== null  ? req.body.device.mail8.replace(/'/g, "''") : ''),
		(req.body.device.mail9 && req.body.device.mail9 !== null  ? req.body.device.mail9.replace(/'/g, "''") : ''),
		(req.body.device.mail10 && req.body.device.mail10 !== null  ? req.body.device.mail10.replace(/'/g, "''") : ''),
		(req.body.device.mail11 && req.body.device.mail11 !== null  ? req.body.device.mail11.replace(/'/g, "''") : ''),
		(req.body.device.mail12 && req.body.device.mail12 !== null  ? req.body.device.mail12.replace(/'/g, "''") : ''),
		(req.body.device.mail13 && req.body.device.mail13 !== null  ? req.body.device.mail13.replace(/'/g, "''") : ''),
		(req.body.device.mail14 && req.body.device.mail14 !== null  ? req.body.device.mail14.replace(/'/g, "''") : ''),
		(req.body.device.mail15 && req.body.device.mail15 !== null  ? req.body.device.mail15.replace(/'/g, "''") : ''),
		(req.body.device.mail16 && req.body.device.mail16 !== null  ? req.body.device.mail16.replace(/'/g, "''") : ''),
		(req.body.device.mail17 && req.body.device.mail17 !== null  ? req.body.device.mail17.replace(/'/g, "''") : ''),
		(req.body.device.mail18 && req.body.device.mail18 !== null  ? req.body.device.mail18.replace(/'/g, "''") : ''),
		(req.body.device.mail19 && req.body.device.mail19 !== null  ? req.body.device.mail19.replace(/'/g, "''") : ''),
		(req.body.device.mail20 && req.body.device.mail20 !== null  ? req.body.device.mail20.replace(/'/g, "''") : ''),
		(req.body.device.mail21 && req.body.device.mail21 !== null  ? req.body.device.mail21.replace(/'/g, "''") : ''),
		(req.body.device.mail22 && req.body.device.mail22 !== null  ? req.body.device.mail22.replace(/'/g, "''") : ''),
		(req.body.device.mail23 && req.body.device.mail23 !== null  ? req.body.device.mail23.replace(/'/g, "''") : ''),
		(req.body.device.mail24 && req.body.device.mail24 !== null  ? req.body.device.mail24.replace(/'/g, "''") : ''),
		(req.body.device.mail25 && req.body.device.mail25 !== null  ? req.body.device.mail25.replace(/'/g, "''") : ''),
		(req.body.device.mail26 && req.body.device.mail26 !== null  ? req.body.device.mail26.replace(/'/g, "''") : ''), 
		req.body.device.send_mail_periodic,
		(req.body.device.apn && req.body.device.apn !== null  ? req.body.device.apn.replace(/'/g, "''") : ''));
      

	     pgClient.query(sql, function(err, devices) {
	      if (err) {
	        return res.status(500).send(err);
	      }
	      
	      
	      	res.json({
				device : req.body.device
			});
	     
	    });
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT update_device_from_user_bbb_id(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
		req.query.user_id,
		null,
		req.body.device.device_beebeeboard_id,
		(req.body.device.device_code && req.body.device.device_code !== null ? req.body.device.device_code.replace(/'/g, "''") : ''),
		(req.body.device.device_name && req.body.device.device_name !== null ? req.body.device.device_name.replace(/'/g, "''") : ''),
		(req.body.device.lotto_produzione && req.body.device.lotto_produzione !== null ? req.body.device.lotto_produzione.replace(/'/g, "''") : ''),
		(req.body.device.mail && req.body.device.mail !== null  ? req.body.device.mail.replace(/'/g, "''") : ''),
		(req.body.device.piano_tariffario && req.body.device.piano_tariffario !== null  ? req.body.device.piano_tariffario.replace(/'/g, "''") : ''),
		req.body.device.send_parameter,
		(req.body.device.sim && req.body.device.sim !== null ? req.body.device.sim.replace(/'/g, "''") : ''),
		(req.body.device.status && req.body.device.status !== null ? req.body.device.status.replace(/'/g, "''") : ''),
		req.body.device.test_accensione,
		req.user.user_beebeeboard_id,
		(req.body.device.mail0 && req.body.device.mail0 !== null  ? req.body.device.mail0.replace(/'/g, "''") : ''),
		(req.body.device.mail1 && req.body.device.mail1 !== null  ? req.body.device.mail1.replace(/'/g, "''") : ''),
		(req.body.device.mail2 && req.body.device.mail2 !== null  ? req.body.device.mail2.replace(/'/g, "''") : ''),
		(req.body.device.mail3 && req.body.device.mail3 !== null  ? req.body.device.mail3.replace(/'/g, "''") : ''),
		(req.body.device.mail4 && req.body.device.mail4 !== null  ? req.body.device.mail4.replace(/'/g, "''") : ''),
		(req.body.device.mail5 && req.body.device.mail5 !== null  ? req.body.device.mail5.replace(/'/g, "''") : ''),
		(req.body.device.mail6 && req.body.device.mail6 !== null  ? req.body.device.mail6.replace(/'/g, "''") : ''),
		(req.body.device.mail7 && req.body.device.mail7 !== null  ? req.body.device.mail7.replace(/'/g, "''") : ''),
		(req.body.device.mail8 && req.body.device.mail8 !== null  ? req.body.device.mail8.replace(/'/g, "''") : ''),
		(req.body.device.mail9 && req.body.device.mail9 !== null  ? req.body.device.mail9.replace(/'/g, "''") : ''),
		(req.body.device.mail10 && req.body.device.mail10 !== null  ? req.body.device.mail10.replace(/'/g, "''") : ''),
		(req.body.device.mail11 && req.body.device.mail11 !== null  ? req.body.device.mail11.replace(/'/g, "''") : ''),
		(req.body.device.mail12 && req.body.device.mail12 !== null  ? req.body.device.mail12.replace(/'/g, "''") : ''),
		(req.body.device.mail13 && req.body.device.mail13 !== null  ? req.body.device.mail13.replace(/'/g, "''") : ''),
		(req.body.device.mail14 && req.body.device.mail14 !== null  ? req.body.device.mail14.replace(/'/g, "''") : ''),
		(req.body.device.mail15 && req.body.device.mail15 !== null  ? req.body.device.mail15.replace(/'/g, "''") : ''),
		(req.body.device.mail16 && req.body.device.mail16 !== null  ? req.body.device.mail16.replace(/'/g, "''") : ''),
		(req.body.device.mail17 && req.body.device.mail17 !== null  ? req.body.device.mail17.replace(/'/g, "''") : ''),
		(req.body.device.mail18 && req.body.device.mail18 !== null  ? req.body.device.mail18.replace(/'/g, "''") : ''),
		(req.body.device.mail19 && req.body.device.mail19 !== null  ? req.body.device.mail19.replace(/'/g, "''") : ''),
		(req.body.device.mail20 && req.body.device.mail20 !== null  ? req.body.device.mail20.replace(/'/g, "''") : ''),
		(req.body.device.mail21 && req.body.device.mail21 !== null  ? req.body.device.mail21.replace(/'/g, "''") : ''),
		(req.body.device.mail22 && req.body.device.mail22 !== null  ? req.body.device.mail22.replace(/'/g, "''") : ''),
		(req.body.device.mail23 && req.body.device.mail23 !== null  ? req.body.device.mail23.replace(/'/g, "''") : ''),
		(req.body.device.mail24 && req.body.device.mail24 !== null  ? req.body.device.mail24.replace(/'/g, "''") : ''),
		(req.body.device.mail25 && req.body.device.mail25 !== null  ? req.body.device.mail25.replace(/'/g, "''") : ''),
		(req.body.device.mail26 && req.body.device.mail26 !== null  ? req.body.device.mail26.replace(/'/g, "''") : ''), 
		req.body.device.send_mail_periodic,
		(req.body.device.apn && req.body.device.apn !== null  ? req.body.device.apn.replace(/'/g, "''") : ''),
        req.params.id);

	     pgClient.query(sql, function(err, devices) {
	      if (err) {
	        return res.status(500).send(err);
	      }
	      
	     
	      	res.json({
				device : req.body.device
			});
	      
	    });
		
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update_user_bb_id = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	var sql = format('SELECT update_device(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
		req.params.id,
		null,
		req.body.device.device_beebeeboard_id,
		(req.body.device.device_code && req.body.device.device_code !== null ? req.body.device.device_code.replace(/'/g, "''") : ''),
		(req.body.device.device_name && req.body.device.device_name !== null ? req.body.device.device_name.replace(/'/g, "''") : ''),
		(req.body.device.lotto_produzione && req.body.device.lotto_produzione !== null ? req.body.device.lotto_produzione.replace(/'/g, "''") : ''),
		(req.body.device.mail && req.body.device.mail !== null  ? req.body.device.mail.replace(/'/g, "''") : ''),
		(req.body.device.piano_tariffario && req.body.device.piano_tariffario !== null  ? req.body.device.piano_tariffario.replace(/'/g, "''") : ''),
		req.body.device.send_parameter,
		(req.body.device.sim && req.body.device.sim !== null ? req.body.device.sim.replace(/'/g, "''") : ''),
		(req.body.device.status && req.body.device.status !== null ? req.body.device.status.replace(/'/g, "''") : ''),
		req.body.device.test_accensione,
		req.body.device.user_beebeeboard_id,
		(req.body.device.mail0 && req.body.device.mail0 !== null  ? req.body.device.mail0.replace(/'/g, "''") : ''),
		(req.body.device.mail1 && req.body.device.mail1 !== null  ? req.body.device.mail1.replace(/'/g, "''") : ''),
		(req.body.device.mail2 && req.body.device.mail2 !== null  ? req.body.device.mail2.replace(/'/g, "''") : ''),
		(req.body.device.mail3 && req.body.device.mail3 !== null  ? req.body.device.mail3.replace(/'/g, "''") : ''),
		(req.body.device.mail4 && req.body.device.mail4 !== null  ? req.body.device.mail4.replace(/'/g, "''") : ''),
		(req.body.device.mail5 && req.body.device.mail5 !== null  ? req.body.device.mail5.replace(/'/g, "''") : ''),
		(req.body.device.mail6 && req.body.device.mail6 !== null  ? req.body.device.mail6.replace(/'/g, "''") : ''),
		(req.body.device.mail7 && req.body.device.mail7 !== null  ? req.body.device.mail7.replace(/'/g, "''") : ''),
		(req.body.device.mail8 && req.body.device.mail8 !== null  ? req.body.device.mail8.replace(/'/g, "''") : ''),
		(req.body.device.mail9 && req.body.device.mail9 !== null  ? req.body.device.mail9.replace(/'/g, "''") : ''),
		(req.body.device.mail10 && req.body.device.mail10 !== null  ? req.body.device.mail10.replace(/'/g, "''") : ''),
		(req.body.device.mail11 && req.body.device.mail11 !== null  ? req.body.device.mail11.replace(/'/g, "''") : ''),
		(req.body.device.mail12 && req.body.device.mail12 !== null  ? req.body.device.mail12.replace(/'/g, "''") : ''),
		(req.body.device.mail13 && req.body.device.mail13 !== null  ? req.body.device.mail13.replace(/'/g, "''") : ''),
		(req.body.device.mail14 && req.body.device.mail14 !== null  ? req.body.device.mail14.replace(/'/g, "''") : ''),
		(req.body.device.mail15 && req.body.device.mail15 !== null  ? req.body.device.mail15.replace(/'/g, "''") : ''),
		(req.body.device.mail16 && req.body.device.mail16 !== null  ? req.body.device.mail16.replace(/'/g, "''") : ''),
		(req.body.device.mail17 && req.body.device.mail17 !== null  ? req.body.device.mail17.replace(/'/g, "''") : ''),
		(req.body.device.mail18 && req.body.device.mail18 !== null  ? req.body.device.mail18.replace(/'/g, "''") : ''),
		(req.body.device.mail19 && req.body.device.mail19 !== null  ? req.body.device.mail19.replace(/'/g, "''") : ''),
		(req.body.device.mail20 && req.body.device.mail20 !== null  ? req.body.device.mail20.replace(/'/g, "''") : ''),
		(req.body.device.mail21 && req.body.device.mail21 !== null  ? req.body.device.mail21.replace(/'/g, "''") : ''),
		(req.body.device.mail22 && req.body.device.mail22 !== null  ? req.body.device.mail22.replace(/'/g, "''") : ''),
		(req.body.device.mail23 && req.body.device.mail23 !== null  ? req.body.device.mail23.replace(/'/g, "''") : ''),
		(req.body.device.mail24 && req.body.device.mail24 !== null  ? req.body.device.mail24.replace(/'/g, "''") : ''),
		(req.body.device.mail25 && req.body.device.mail25 !== null  ? req.body.device.mail25.replace(/'/g, "''") : ''),
		(req.body.device.mail26 && req.body.device.mail26 !== null  ? req.body.device.mail26.replace(/'/g, "''") : ''), 
		req.body.device.send_mail_periodic,
		(req.body.device.apn && req.body.device.apn !== null  ? req.body.device.apn.replace(/'/g, "''") : ''));

	     pgClient.query(sql, function(err, devices) {
	      if (err) {
	        return res.status(500).send(err);
	      }
	      
	      
	      	res.json({
				device : req.body.device
			});
	      
	    });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update_bb_id = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT update_device_set_user_bbb_id(%L,%L)',
			req.params.id,
			(req.query.user_id ? req.query.user_id : null));

		console.log(sql);
		pgClient.query(sql, function(err, devices) {
          if (err) {
            return res.status(500).send(err);
          }
          
          
          	res.json({'devices':devices.rows[0].update_device_set_user_bbb_id});
         
        });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.create = function(req, res) {
  	var async = require('async');
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	var device = {};
	device['mongo_id'] = null;
	device['device_beebeeboard_id'] = req.body.device.device_beebeeboard_id;
	device['device_code'] = (req.body.device.device_code && req.body.device.device_code !== null ? req.body.device.device_code.replace(/'/g, "''") : '');
	device['device_name'] = (req.body.device.device_name && req.body.device.device_name !== null ? req.body.device.device_name.replace(/'/g, "''") : '');
	device['lotto_produzione'] = (req.body.device.lotto_produzione && req.body.device.lotto_produzione !== null ? req.body.device.lotto_produzione.replace(/'/g, "''") : '');
	device['mail'] = (req.body.device.mail && req.body.device.mail !== null  ? req.body.device.mail.replace(/'/g, "''") : '');
	device['piano_tariffario'] = (req.body.device.piano_tariffario && req.body.device.piano_tariffario !== null  ? req.body.device.piano_tariffario.replace(/'/g, "''") : '');
	device['sim'] = (req.body.device.sim && req.body.device.sim !== null ? req.body.device.sim.replace(/'/g, "''") : '');
	device['status'] = (req.body.device.status && req.body.device.status !== null ? req.body.device.status.replace(/'/g, "''") : '');
	device['user_beebeeboard_id'] = req.body.device.user_beebeeboard_id;
	device['mail0'] = (req.body.device.mail0 && req.body.device.mail0 !== null  ? req.body.device.mail0.replace(/'/g, "''") : '');
	device['mail1'] = (req.body.device.mail1 && req.body.device.mail1 !== null  ? req.body.device.mail1.replace(/'/g, "''") : '');
	device['mail2'] = (req.body.device.mail2 && req.body.device.mail2 !== null  ? req.body.device.mail2.replace(/'/g, "''") : '');
	device['mail3'] = (req.body.device.mail3 && req.body.device.mail3 !== null  ? req.body.device.mail3.replace(/'/g, "''") : '');
	device['mail4'] = (req.body.device.mail4 && req.body.device.mail4 !== null  ? req.body.device.mail4.replace(/'/g, "''") : '');
	device['mail5'] = (req.body.device.mail5 && req.body.device.mail5 !== null  ? req.body.device.mail5.replace(/'/g, "''") : '');
	device['mail6'] = (req.body.device.mail6 && req.body.device.mail6 !== null  ? req.body.device.mail6.replace(/'/g, "''") : '');
	device['mail7'] = (req.body.device.mail7 && req.body.device.mail7 !== null  ? req.body.device.mail7.replace(/'/g, "''") : '');
	device['mail8'] = (req.body.device.mail8 && req.body.device.mail8 !== null  ? req.body.device.mail8.replace(/'/g, "''") : '');
	device['mail9'] = (req.body.device.mail9 && req.body.device.mail9 !== null  ? req.body.device.mail9.replace(/'/g, "''") : '');
	device['mail10'] = (req.body.device.mail10 && req.body.device.mail10 !== null  ? req.body.device.mail10.replace(/'/g, "''") : '');
	device['mail11'] = (req.body.device.mail11 && req.body.device.mail11 !== null  ? req.body.device.mail11.replace(/'/g, "''") : '');
	device['mail12'] = (req.body.device.mail12 && req.body.device.mail12 !== null  ? req.body.device.mail12.replace(/'/g, "''") : '');
	device['mail13'] = (req.body.device.mail13 && req.body.device.mail13 !== null  ? req.body.device.mail13.replace(/'/g, "''") : '');
	device['mail14'] = (req.body.device.mail14 && req.body.device.mail14 !== null  ? req.body.device.mail14.replace(/'/g, "''") : '');
	device['mail15'] = (req.body.device.mail15 && req.body.device.mail15 !== null  ? req.body.device.mail15.replace(/'/g, "''") : '');
	device['mail16'] = (req.body.device.mail16 && req.body.device.mail16 !== null  ? req.body.device.mail16.replace(/'/g, "''") : '');
	device['mail17'] = (req.body.device.mail17 && req.body.device.mail17 !== null  ? req.body.device.mail17.replace(/'/g, "''") : '');
	device['mail18'] = (req.body.device.mail18 && req.body.device.mail18 !== null  ? req.body.device.mail18.replace(/'/g, "''") : '');
	device['mail19'] = (req.body.device.mail19 && req.body.device.mail19 !== null  ? req.body.device.mail19.replace(/'/g, "''") : '');
	device['mail20'] = (req.body.device.mail20 && req.body.device.mail20 !== null  ? req.body.device.mail20.replace(/'/g, "''") : '');
	device['mail21'] = (req.body.device.mail21 && req.body.device.mail21 !== null  ? req.body.device.mail21.replace(/'/g, "''") : '');
	device['mail22'] = (req.body.device.mail22 && req.body.device.mail22 !== null  ? req.body.device.mail22.replace(/'/g, "''") : '');
	device['mail23'] = (req.body.device.mail23 && req.body.device.mail23 !== null  ? req.body.device.mail23.replace(/'/g, "''") : '');
	device['mail24'] = (req.body.device.mail24 && req.body.device.mail24 !== null  ? req.body.device.mail24.replace(/'/g, "''") : '');
	device['mail25'] = (req.body.device.mail25 && req.body.device.mail25 !== null  ? req.body.device.mail25.replace(/'/g, "''") : '');
	device['mail26'] = (req.body.device.mail26 && req.body.device.mail26 !== null  ? req.body.device.mail26.replace(/'/g, "''") : '');
	device['send_parameter'] = req.body.device.send_parameter;
	device['test_accensione'] = req.body.device.test_accensione;
	device['send_mail_periodic'] = req.body.device.send_mail_periodic;
  	device['apn'] = (req.body.device.apn && req.body.device.apn !== null  ? req.body.device.apn.replace(/'/g, "''") : '');
      

  	var sql = format('SELECT insert_device(\''+JSON.stringify(device)+'\')');


 	 pgClient.query(sql, function(err, devices) {
      if (err) {
        return res.status(500).send(err);
      }
      
      	async.parallel({
				part1: function(callback){
						
					var param = {
					
						device_id : devices.rows[0].insert_device,
						/* Event mask enable */
						event_enable: 3,
						min0:0,
						max0:1,
						dur0:5,
						
						min1:0,
						max1:1,
						dur1:5,
						
						min2:0,
						max2:1,
						dur2:5,
						
						min3:1,
						max3:1,
						dur3:1,
						
						min4:0,
						max4:10,
						dur4:300,
						
						min5:10,
						max5:999,
						dur5:120,
						min6:null,
						max6:null,
						dur6: 300,//send_int
						min7:0,
						max7:999,
						dur7: 900,
						min8:-40,
						max8:125,
						dur8: 300,
						min9:-40,
						max9:125,
						dur9: 300,
						min10:0,
						max10:7.99,
						dur10: 60,
						min11:0,
						max11:90,
						dur11: 5,
						min12:0,
						max12:999,
						dur12: 300,
						min13:0,
						max13:999,
						dur13: 900,
						min14:0,
						max14:9999,
						dur14: 60,
						min15:0,
						max15:7.99,
						dur15: 2,
						min16:0,
						max16:9999,
						dur16: 20,
						min17:0,
						max17:9999,
						dur17: 300,
						min18:0,
						max18:9999,
						dur18: 600,
						min19:0,
						max19:9999,
						dur19: 30,
						min20:0,
						max20:9999,
						dur20: 60,
						min21:0,
						max21:9999,
						dur21: 30,
						min22:0,
						max22:9999,
						dur22: 30,
						min23:0,
						max23:9999,
						dur23: 180,
						min24:0,
						max24:9999,
						dur24: 600,
						min25:0.5,
						max25:7.99,
						dur25: 1,
						min26:null,
						max26:null,
						dur26: -1,
						min27:null,
						max27:null,
						dur27: -1,
						min28:null,
						max28:null,
						dur28: -1,
						min29:null,
						max29:null,
						dur29: -1,
						min30:null,
						max30:null,
						dur30: -1,
						min31:null,
						max31:null,
						dur31: -1,
						dur32: -1,
						max33: -1,
						
						/* distance parameter in coordinates X */
						r1_x: null,
						r2_x: null,
						r3_x: null,
						r4_x: null,
						r5_x: null,
						r6_x: null,
						r7_x: null,
						r8_x: null,
						r9_x: null,
						r10_x: null,
						q1_x: null,
						q2_x: null,
						q3_x: null,
						q4_x: null,
						q5_x: null,
						q6_x: null,
						q7_x: null,
						q8_x: null,
						q9_x: null,
						q10_x: null,
						p1_x: null,
						p2_x: null,
						p3_x: null,
						p4_x: null,
						p5_x: null,
						p6_x: null,
						p7_x: null,
						p8_x: null,
						p9_x: null,
						p10_x: null,
						p11_x: null,
						p12_x: null,
						p13_x: null,
						p14_x: null,
						p15_x: null,
						p16_x: null,
						p17_x: null,
						p18_x: null,
						p19_x: null,
						p20_x: null,
						/* distance parameter in coordinates Y */
						r1_y: null,
						r2_y: null,
						r3_y: null,
						r4_y: null,
						r5_y: null,
						r6_y: null,
						r7_y: null,
						r8_y: null,
						r9_y: null,
						r10_y: null,
						q1_y: null,
						q2_y: null,
						q3_y: null,
						q4_y: null,
						q5_y: null,
						q6_y: null,
						q7_y: null,
						q8_y: null,
						q9_y: null,
						q10_y: null,
						p1_y: null,
						p2_y: null,
						p3_y: null,
						p4_y: null,
						p5_y: null,
						p6_y: null,
						p7_y: null,
						p8_y: null,
						p9_y: null,
						p10_y: null,
						p11_y: null,
						p12_y: null,
						p13_y: null,
						p14_y: null,
						p15_y: null,
						p16_y: null,
						p17_y: null,
						p18_y: null,
						p19_y: null,
						p20_y: null
					
					};

					var sql = format('SELECT insert_parameter(\''+JSON.stringify(param)+'\')');

				 	 pgClient.query(sql, function(err, parameters) {
				      if (err) {
				        callbakc(true,null);
				      }
				      
				      callback(null, 1);
				     
				    });
					
				}
			}, function(err, results){
				if(err){

				}

				var dev_sql = format('UPDATE devices SET send_parameter = %L, mail0 = (SELECT mail from devices where id = %L),mail1 =(SELECT mail from devices where id = %L),mail2 =(SELECT mail from devices where id = %L),mail3 =(SELECT mail from devices where id = %L),mail4 =(SELECT mail from devices where id = %L),mail5 =(SELECT mail from devices where id = %L),mail6 =(SELECT mail from devices where id = %L),mail7 =(SELECT mail from devices where id = %L),mail8 =(SELECT mail from devices where id = %L),mail9 =(SELECT mail from devices where id = %L),mail10 =(SELECT mail from devices where id = %L),mail11 =(SELECT mail from devices where id = %L),mail12 =(SELECT mail from devices where id = %L),mail13 =(SELECT mail from devices where id = %L),mail14 =(SELECT mail from devices where id = %L),mail15 =(SELECT mail from devices where id = %L),mail16 =(SELECT mail from devices where id = %L),mail17 =(SELECT mail from devices where id = %L),mail18 =(SELECT mail from devices where id = %L),mail19 =(SELECT mail from devices where id = %L),mail20 =(SELECT mail from devices where id = %L),mail21 =(SELECT mail from devices where id = %L),mail22 =(SELECT mail from devices where id = %L),mail23 =(SELECT mail from devices where id = %L),mail24 =(SELECT mail from devices where id = %L),mail25 =(SELECT mail from devices where id = %L), mail26=(SELECT mail from devices where id = %L) WHERE id = %L',
		      		true,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device,
		      		devices.rows[0].insert_device);

		      	pgClient.query(dev_sql, function(err, device) {
			      if (err) {
			        return res.status(500).send(err);
			      }
			     	

			      	var sql = format('SELECT device_lists(null,null,null, %L);',
						devices.rows[0].insert_device);

					pgClient.query(sql, function(err, devices) {
			          if (err) {
			            return res.status(500).send(err);
			          }
			          
			          if(devices && devices.rows && devices.rows.length > 0){
			          	res.json({'device':devices.rows[0].device_lists[0]});
			          }
			          else
			          {
			          	res.status(404).json({});
			          }
			        });
			     
			     });
		     });
    });

	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing device searched by id
 */
exports.destroy = function(req, res) {
	
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	var sql = format('SELECT delete_device(%L,%L)',
		req.params.id,
		req.query.user_id);

	pgClient.query(sql, function(err, devices) {
      if (err) {
        return res.status(500).send(err);
      }
      
      
      	res.json(devices.rows[0].delete_device);
     
    });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};