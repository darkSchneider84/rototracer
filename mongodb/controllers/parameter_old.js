var moment = require('moment'), mongoose = require('mongoose'), Parameter = mongoose.model('Parameter'), Device = mongoose.model('Device');
var config = require('../../config');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
		Parameter.find({
			'device_id': req.query.device_id
		}, function(err, parameters){
			if (err) {
				return res.status(500).send(err);
			}
			
			res.json({
				parameters : parameters
			});
		});
	}
	else
	{
		res.status(401).send({});
	}
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific Parameter searched by id
 */
exports.find = function(req, res) {
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
		Parameter.findOne({
			'_id' : req.params.id
		}, function(err, parameter) {
			if (err) {
				return res.status(500).send(err);
			}
			if(!Parameter)
			{
				return res.status(404).send({});
			}
			
			res.json({
				parameter : parameter
			});
		});
	}
	else
	{
		res.status(401).send({});
	}
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing Parameter searched by id
 */
exports.update = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		req.body.parameter = set_limits_parameter(req.body.parameter);

		Parameter.findOneAndUpdate({
			'device_id' : req.params.id
		}, req.body.parameter,{'new':true},  function(err, parameter) {
			if (err) {
				return res.status(500).send(err);
			}
			Device.update({
                '_id':req.params.id
            },{
                '$set':{
                    'send_parameter': true
                }
            },function(err,device_updated){
                  if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        res.json({
							parameter : parameter
						});
                    }
            });

			
		});
/*	}
	else
	{
		res.status(401).send({});
	}*/
};

exports.create = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	  
		 Parameter(req.body.parameter)
	    .save(function (err, parameter) {
	      if (err) {
	        return res.status(500)
	          .send(err);
	      }

	      res.json({
	        parameter: parameter
	      });
	    });
   	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing Parameter searched by id
 */
exports.destroy = function(req, res) {
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Parameter.findOneAndRemove({
			'_id' : req.params.id
		}, function(err, parameter) {
			if (err) {
				return res.status(500).send(err);
			}
			res.status(200).send({});
		});
	}
	else
	{
		res.status(401).send({});
	}
};

exports.default = function(req, res) {
	var async = require('async');
	var searchObj = {};

	var devices = [];
	var i = 0;

	if(req.query.device !== undefined && req.query.device !== ''){
		devices = req.query.device.split(',');
		devices.forEach(function(device_id){

			var param = {
				
					device_id : device_id,
					/* Event mask enable */
					event_enable: 32895,
					min0:0,
					max0:1,
					dur0:5,
					
					min1:0,
					max1:1,
					dur1:5,
					
					min2:0,
					max2:1,
					dur2:5,
					
					min3:1,
					max3:1,
					dur3:1,
					
					min4:0,
					max4:10,
					dur4:300,
					
					min5:10,
					max5:999,
					dur5:120,
					min6:null,
					max6:null,
					dur6: 300,//send_int
					min7:0,
					max7:999,
					dur7: 900,
					min8:-40,
					max8:125,
					dur8: 300,
					min9:-40,
					max9:125,
					dur9: 300,
					min10:0,
					max10:7.99,
					dur10: 60,
					min11:0,
					max11:90,
					dur11: 5,
					min12:0,
					max12:999,
					dur12: 300,
					min13:0,
					max13:999,
					dur13: 900,
					min14:0,
					max14:9999,
					dur14: 60,
					min15:0,
					max15:7.99,
					dur15: 2,
					min16:0,
					max16:9999,
					dur16: 20,
					min17:0,
					max17:9999,
					dur17: 300,
					min18:0,
					max18:9999,
					dur18: 600,
					min19:0,
					max19:9999,
					dur19: 30,
					min20:0,
					max20:9999,
					dur20: 60,
					min21:0,
					max21:9999,
					dur21: 30,
					min22:0,
					max22:9999,
					dur22: 30,
					min23:0,
					max23:9999,
					dur23: 180,
					min24:0,
					max24:9999,
					dur24: 600,
					min25:0.5,
					max25:7.99,
					dur25: 1,
					min26:null,
					max26:null,
					dur26: -1,
					min27:null,
					max27:null,
					dur27: -1,
					min28:null,
					max28:null,
					dur28: -1,
					min29:null,
					max29:null,
					dur29: -1,
					min30:null,
					max30:null,
					dur30: -1,
					min31:null,
					max31:null,
					dur31: -1,
					dur32: -1,
					max33: -1,
					
					/* distance parameter in coordinates X */
					r1_x: null,
					r2_x: null,
					r3_x: null,
					r4_x: null,
					r5_x: null,
					r6_x: null,
					r7_x: null,
					r8_x: null,
					r9_x: null,
					r10_x: null,
					q1_x: null,
					q2_x: null,
					q3_x: null,
					q4_x: null,
					q5_x: null,
					q6_x: null,
					q7_x: null,
					q8_x: null,
					q9_x: null,
					q10_x: null,
					p1_x: null,
					p2_x: null,
					p3_x: null,
					p4_x: null,
					p5_x: null,
					p6_x: null,
					p7_x: null,
					p8_x: null,
					p9_x: null,
					p10_x: null,
					p11_x: null,
					p12_x: null,
					p13_x: null,
					p14_x: null,
					p15_x: null,
					p16_x: null,
					p17_x: null,
					p18_x: null,
					p19_x: null,
					p20_x: null,
					/* distance parameter in coordinates Y */
					r1_y: null,
					r2_y: null,
					r3_y: null,
					r4_y: null,
					r5_y: null,
					r6_y: null,
					r7_y: null,
					r8_y: null,
					r9_y: null,
					r10_y: null,
					q1_y: null,
					q2_y: null,
					q3_y: null,
					q4_y: null,
					q5_y: null,
					q6_y: null,
					q7_y: null,
					q8_y: null,
					q9_y: null,
					q10_y: null,
					p1_y: null,
					p2_y: null,
					p3_y: null,
					p4_y: null,
					p5_y: null,
					p6_y: null,
					p7_y: null,
					p8_y: null,
					p9_y: null,
					p10_y: null,
					p11_y: null,
					p12_y: null,
					p13_y: null,
					p14_y: null,
					p15_y: null,
					p16_y: null,
					p17_y: null,
					p18_y: null,
					p19_y: null,
					p20_y: null
				
			};

			Parameter.findOneAndUpdate({
				'device_id' : device_id
			}, param,  function(err, parameter) {
				if (err) {
					return res.status(500).send(err);
				}
				Device.findOne({'_id': device_id},function(err, dev){
					Device.update({
		                '_id':device_id
		            },{
		                '$set':{
		                    'send_parameter': true,
		                    'mail0': dev.mail,
		                    'mail1': dev.mail,
		                    'mail2': dev.mail,
		                    'mail3': dev.mail,
		                    'mail4': dev.mail,
		                    'mail5': dev.mail,
		                    'mail7': dev.mail,
		                    'mail8': dev.mail,
		                    'mail9': dev.mail,
		                    'mail10': dev.mail,
		                    'mail11': dev.mail,
		                    'mail12': dev.mail,
		                    'mail13': dev.mail,
		                    'mail14': dev.mail,
		                    'mail15': dev.mail,
		                    'mail16': dev.mail,
		                    'mail17': dev.mail,
		                    'mail18': dev.mail,
		                    'mail19': dev.mail,
		                    'mail20': dev.mail,
		                    'mail21': dev.mail,
		                    'mail22': dev.mail,
		                    'mail23': dev.mail,
		                    'mail24': dev.mail,
		                    'mail25': dev.mail,
		                    'mail26': dev.mail
		                }
		            },function(err,device_updated){
		                  if(err)
		                    {
		                        console.log(err);
		                    }
		                    
		            });
				});
				

			});
		});


        res.json({
			parameter : {}
		});
	                   
	}
	else{
		 res.json({
			parameter : {}
		});
	}
};

function set_limits_parameter(param){

	// Event 0
	if(parseFloat(param.max0) > 1){
		param.max0 = 1;
	}
	else if(parseFloat(param.max0) < 0){
		param.max0 = 0;
	}
	if(parseFloat(param.min0) > 1){
		param.min0 = 1;
	}
	else if(parseFloat(param.min0) < 0){
		param.min0 = 0;
	}

	// Event 1
	if(parseFloat(param.max1) > 1){
		param.max1 = 1;
	}
	else if(parseFloat(param.max1) < 0){
		param.max1 = 0;
	}
	if(parseFloat(param.min1) > 1){
		param.min1 = 1;
	}
	else if(parseFloat(param.min1)< 0){
		param.min1 = 0;
	}

	// Event 2
	if(parseFloat(param.max2) > 1){
		param.max2 = 1;
	}
	else if(parseFloat(param.max2) < 0){
		param.max2 = 0;
	}
	if(parseFloat(param.min2) > 1){
		param.min2 = 1;
	}
	else if(parseFloat(param.min2)< 0){
		param.min2 = 0;
	}

	// Event 3
	if(parseFloat(param.max3) > 1){
		param.max3 = 1;
	}
	else if(parseFloat(param.max3) < 0){
		param.max3 = 0;
	}
	if(parseFloat(param.min3) > 1){
		param.min3 = 1;
	}
	else if(parseFloat(param.min3)< 0){
		param.min3 = 0;
	}

	// Event 4
	if(parseFloat(param.max4) > 999){
		param.max4 = 999;
	}
	else if(parseFloat(param.max4) < 0){
		param.max4 = 0;
	}
	if(parseFloat(param.min4) > 999){
		param.min4 = 999;
	}
	else if(parseFloat(param.min4)< 0){
		param.min4 = 0;
	}

	// Event 5
	if(parseFloat(param.max5) > 999){
		param.max5 = 999;
	}
	else if(parseFloat(param.max5) < 0){
		param.max5 = 0;
	}
	if(parseFloat(param.min5) > 999){
		param.min5 = 999;
	}
	else if(parseFloat(param.min5)< 0){
		param.min5 = 0;
	}

	// Event 7
	if(parseFloat(param.max7) > 999){
		param.max7 = 999;
	}
	else if(parseFloat(param.max7) < 0){
		param.max7 = 0;
	}
	if(parseFloat(param.min7) > 999){
		param.min7 = 999;
	}
	else if(parseFloat(param.min7)< 0){
		param.min7 = 0;
	}

	// Event 8
	if(parseFloat(param.max8) > 125){
		param.max8 = 125;
	}
	else if(parseFloat(param.max8) < -40){
		param.max8 = -40;
	}
	if(parseFloat(param.min8) > 125){
		param.min8 = 125;
	}
	else if(parseFloat(param.min8)< -40){
		param.min8 = -40;
	}

	// Event 9
	if(parseFloat(param.max9) > 125){
		param.max9 = 125;
	}
	else if(parseFloat(param.max9) < -40){
		param.max9 = -40;
	}
	if(parseFloat(param.min9) > 125){
		param.min9 = 125;
	}
	else if(parseFloat(param.min9)< -40){
		param.min9 = -40;
	}

	// Event 10
	if(parseFloat(param.max10) > 7.99){
		param.max10 = 7.99;
	}
	else if(parseFloat(param.max10) < 0){
		param.max10 = 0;
	}
	if(parseFloat(param.min10) > 7.99){
		param.min10 = 7.99;
	}
	else if(parseFloat(param.min10)< 0){
		param.min10 = 0;
	}

	// Event 11
	if(parseFloat(param.max11) > 90){
		param.max11 = 90;
	}
	else if(parseFloat(param.max11) < 0){
		param.max11 = 0;
	}
	if(parseFloat(param.min11) > 90){
		param.min11 = 90;
	}
	else if(parseFloat(param.min11)< 0){
		param.min11 = 0;
	}

	// Event 12
	if(parseFloat(param.max12) > 999){
		param.max12 = 999;
	}
	else if(parseFloat(param.max12) < 0){
		param.max12 = 0;
	}
	if(parseFloat(param.min12) > 999){
		param.min12 = 999;
	}
	else if(parseFloat(param.min12)< 0){
		param.min12 = 0;
	}

	// Event 13
	if(parseFloat(param.max13) > 999){
		param.max13 = 999;
	}
	else if(parseFloat(param.max13) < 0){
		param.max13 = 0;
	}
	if(parseFloat(param.min13) > 999){
		param.min13 = 999;
	}
	else if(parseFloat(param.min13)< 0){
		param.min13 = 0;
	}

	// Event 14
	if(parseFloat(param.max14) > 9999){
		param.max14 = 9999;
	}
	else if(parseFloat(param.max14) < 0){
		param.max14 = 0;
	}
	if(parseFloat(param.min14) > 9999){
		param.min14 = 9999;
	}
	else if(parseFloat(param.min14)< 0){
		param.min14 = 0;
	}

	// Event 15
	if(parseFloat(param.max15) > 7.99){
		param.max15 = 7.99;
	}
	else if(parseFloat(param.max15) < 0){
		param.max15 = 0;
	}
	if(parseFloat(param.min15) > 7.99){
		param.min15 = 7.99;
	}
	else if(parseFloat(param.min15)< 0){
		param.min15 = 0;
	}

	// Event 16
	if(parseFloat(param.max16) > 9999){
		param.max16 = 9999;
	}
	else if(parseFloat(param.max16) < 0){
		param.max16 = 0;
	}
	if(parseFloat(param.min16) > 9999){
		param.min16 = 9999;
	}
	else if(parseFloat(param.min16)< 0){
		param.min16 = 0;
	}

	// Event 17
	if(parseFloat(param.max17) > 9999){
		param.max17 = 9999;
	}
	else if(parseFloat(param.max17) < 0){
		param.max17 = 0;
	}
	if(parseFloat(param.min17) > 9999){
		param.min17 = 9999;
	}
	else if(parseFloat(param.min17)< 0){
		param.min17 = 0;
	}

	// Event 18
	if(parseFloat(param.max18) > 9999){
		param.max18 = 9999;
	}
	else if(parseFloat(param.max18) < 0){
		param.max18 = 0;
	}
	if(parseFloat(param.min18) > 9999){
		param.min18 = 9999;
	}
	else if(parseFloat(param.min18)< 0){
		param.min18 = 0;
	}

	// Event 19
	if(parseFloat(param.max19) > 9999){
		param.max19 = 9999;
	}
	else if(parseFloat(param.max19) < 0){
		param.max19 = 0;
	}
	if(parseFloat(param.min19) > 9999){
		param.min19 = 9999;
	}
	else if(parseFloat(param.min19)< 0){
		param.min19 = 0;
	}

	// Event 20
	if(parseFloat(param.max20) > 9999){
		param.max20 = 9999;
	}
	else if(parseFloat(param.max20) < 0){
		param.max20 = 0;
	}
	if(parseFloat(param.min20) > 9999){
		param.min20 = 9999;
	}
	else if(parseFloat(param.min20)< 0){
		param.min20 = 0;
	}

	// Event 21
	if(parseFloat(param.max21) > 9999){
		param.max21 = 9999;
	}
	else if(parseFloat(param.max21) < 0){
		param.max21 = 0;
	}
	if(parseFloat(param.min21) > 9999){
		param.min21 = 9999;
	}
	else if(parseFloat(param.min21)< 0){
		param.min21 = 0;
	}

	// Event 22
	if(parseFloat(param.max22) > 9999){
		param.max22 = 9999;
	}
	else if(parseFloat(param.max22) < 0){
		param.max22 = 0;
	}
	if(parseFloat(param.min22) > 9999){
		param.min22 = 9999;
	}
	else if(parseFloat(param.min22)< 0){
		param.min22 = 0;
	}

	// Event 23
	if(parseFloat(param.max23) > 9999){
		param.max23 = 9999;
	}
	else if(parseFloat(param.max23) < 0){
		param.max23 = 0;
	}
	if(parseFloat(param.min23) > 9999){
		param.min23 = 9999;
	}
	else if(parseFloat(param.min23)< 0){
		param.min23 = 0;
	}

	// Event 24
	if(parseFloat(param.max24) > 9999){
		param.max24 = 9999;
	}
	else if(parseFloat(param.max24) < 0){
		param.max24 = 0;
	}
	if(parseFloat(param.min24) > 9999){
		param.min24 = 9999;
	}
	else if(parseFloat(param.min24)< 0){
		param.min24 = 0;
	}
	// Event 25
	if(parseFloat(param.max25) > 7.99){
		param.max25 = 7.99;
	}
	else if(parseFloat(param.max25) < 0){
		param.max25 = 0;
	}
	if(parseFloat(param.min25) > 7.99){
		param.min25 = 7.99;
	}
	else if(parseFloat(param.min25)< 0){
		param.min25 = 0;
	}

	return param;
}; 