var moment = require('moment'), 
moment_timezone = require('moment-timezone'),
pg = require('pg'),
format = require('pg-format'); 
var config = require('../../config');


exports.exportcsv = function(req, res) {
	var json2csv = require('json2csv');

	var _filename = 'status';
	
	var devices = [];
	var codevents = [];
	var searchObj = '';

if(req.headers['host'] == 'reserved.rototracer.com' || req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
	if(req.query.file !== undefined && req.query.file !== ''){
		_filename = req.query.file;
	}

	if(req.query.device !== undefined && req.query.device !== ''){
		devices = req.query.device.split(',');
		
	}

	if(req.query.codevent !== undefined && req.query.codevent !== ''){
		codevents = req.query.codevent.split(',');
	}

	console.log(new Date(req.query.from_date).toISOString());
	if (req.query.from_date !== undefined && req.query.from_date !== '') {
		if (req.query.to_date !== undefined && req.query.to_date !== '') {
			searchObj =
				's.created_at >= \''+ new Date(req.query.from_date).toISOString() +
				'\'::timestamp with time zone AND s.created_at <= \''+ new Date(req.query.to_date).toISOString()+'\'::timestamp with time zone';
				
		} else {
			searchObj = 's.created_at >= \'' + new Date(req.query.from_date).toISOString()+'\'::timestamp with time zone';
			
		}
	} else if (req.query.to_date !== undefined && req.query.to_date !== '') {
		searchObj = 's.created_at <= \'' + new Date(req.query.to_date).toISOString()+'\'::timestamp with time zone';	
	}

	var sql = format('WITH all_ as (SELECT s.*, e.codevent, e.timev,d.sim, d.device_code, ROW_NUMBER() OVER (PARTITION BY s.device_id) as rk from statuses as s left join events as e on e.id = s.event_id left join devices as d on d.id = s.device_id WHERE s.device_id = ANY(ARRAY[%L]::bigint[]) and e.codevent = ANY(ARRAY[%L]::character varying[]) and '+searchObj+' ORDER BY s.device_name DESC, s.created_at DESC) select * from all_ where rk < 10000',
			devices,
			codevents
		);

	pgClient.query(sql, function(err, statuses) {
      if (err) {
        return res.status(500).send(err);
      }
      
      if(statuses.rows.length == 0){
      	return res.status(200).send({});
      }
   	  else if(statuses.rows.length > 0){
   	  		var gval = 4096;
			var cnt = 0;
			var finals = [];
			statuses.rows.forEach(function(ev){
				var google = convert_to_google_coord(ev.gpsx, ev.gpsy, ev.ns, ev.ew);
				var gpsx = google.split(',')[0];
				var gpsy = google.split(',')[1];
				var acc_ist = parseFloat((Math.sqrt((ev.accxist^2) + (ev.accyist^2) + (ev.acczist^2)))/gval); 
				var accxmed = parseFloat(ev.accxsqmflt/gval);
				var accymed = parseFloat(ev.accysqmflt/gval);
				var acczmed = parseFloat(ev.acczsqmflt/gval);
				var acc_basso = 90 - (Math.atan(ev.acczflt/parseFloat(Math.sqrt((ev.accxflt*ev.accxflt) + (ev.accyflt*ev.accyflt))))*180/3.14);
				var acc_rot = ev.accxflt > 0 ? parseFloat(Math.atan(ev.accyflt/ev.accxflt)) : parseFloat(180-(Math.atan(ev.accyflt/ev.accxflt)));
				var new_stat = {};
			    new_stat.timefix = moment(new Date(ev.timefix)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.firmware_version = ev.firmware_version;
				new_stat.send_int = ev.send_int;
				new_stat.apn = ev.apn;
				new_stat.ip_address = ev.ip_address;
				new_stat.gpsx = gpsx.toString().replace('.',',');
				new_stat.gpsy = gpsy.toString().replace('.',',');
				new_stat.gpsz = ev.gpsz.toString().replace('.',',');
				new_stat.gpsd = ev.gpsd.toString().replace('.',',');
				new_stat.gpsst = ev.gpsst;
				new_stat.gpsstime = moment(new Date(ev.gpsstime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.gpssat = ev.gpssat;
				new_stat.pdop = (Math.round(ev.pdop*100)/100).toString().replace('.',',');
				new_stat.hdop = (Math.round(ev.hdop*100)/100).toString().replace('.',',');
				new_stat.vdop = (Math.round(ev.vdop*100)/100).toString().replace('.',',');
				new_stat.gpsvel = ev.gpsvel;
				new_stat.gsmlvl = ev.gsmlvl;
				new_stat.gsmst = ev.gsmst;
				new_stat.gsmstime = moment(new Date(ev.gsmstime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.gsmopn = ev.gsmopn;
				new_stat.gsmmod = ev.gsmmod;
				new_stat.acc_ist = (Math.round(acc_ist*100)/100).toString().replace('.',',');
				new_stat.accxmed = (Math.round(accxmed*100)/100).toString().replace('.',',');
				new_stat.accymed = (Math.round(accymed*100)/100).toString().replace('.',',');
				new_stat.acczmed = (Math.round(acczmed*100)/100).toString().replace('.',',');
				new_stat.acc_basso = (Math.round(acc_basso*100)/100).toString().replace('.',',');
				new_stat.acc_rot = (Math.round(acc_rot*100)/100).toString().replace('.',',');
				new_stat.accxist = ev.accxist.toString().replace('.',',');
				new_stat.accyist = ev.accyist.toString().replace('.',',');
				new_stat.acczist = ev.acczist.toString().replace('.',',');
				new_stat.accxsqm = ev.accxsqm.toString().replace('.',',');
				new_stat.accysqm = ev.accysqm.toString().replace('.',',');
				new_stat.acczsqm = ev.acczsqm.toString().replace('.',',');
				new_stat.accxflt = ev.accxflt.toString().replace('.',',');
				new_stat.accyflt = ev.accyflt.toString().replace('.',',');
				new_stat.acczflt = ev.acczflt.toString().replace('.',',');
				new_stat.accxsqmflt = ev.accxsqmflt.toString().replace('.',',');
				new_stat.accysqmflt = ev.accysqmflt.toString().replace('.',',');
				new_stat.acczsqmflt = ev.acczsqmflt.toString().replace('.',',');
				new_stat.temp = (Math.round(ev.temp*100)/100).toString().replace('.',',');
				new_stat.keyst = ev.keyst;
				new_stat.keytime = moment(new Date(ev.keytime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.pwrst = ev.pwrst;
				new_stat.pwrstime = moment(new Date(ev.pwrstime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.almst = ev.almst;
				new_stat.almstime = moment(new Date(ev.almstime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.blkst = ev. blkst;
				new_stat.blkstime =moment(new Date(ev.blkstime)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.event_id = ev.event_id;
				new_stat.sim = ev.sim;
				new_stat.device_code = ev.device_code;
				new_stat.device_name = ev.device_name;
				new_stat.codevent = ev.codevent;
				new_stat.battery = ev.battery;
				new_stat.fwvc = ev.fwvc;
				new_stat.errorcode = ev.errorcode;
				new_stat.timetx = moment(new Date(ev.timetx)).format('DD/MM/YYYY HH:mm:ss');
				
				//var timezone = moment_timezone.tz(moment(ev.created_at), 'Europe/Rome').format('Z');

				//var hours = parseInt(timezone.split(':')[0]);

				//new_stat.created_at = moment(ev.created_at).add(hours,'h').format('DD/MM/YYYY HH:mm:ss');
				new_stat.created_at = moment(new Date(ev.created_at)).format('DD/MM/YYYY HH:mm:ss');
				new_stat.nrip = ev.nrip;
				new_stat.timev = moment(new Date(ev.timev)).format('DD/MM/YYYY HH:mm:ss');
				finals.push(new_stat);

				if(cnt++ >= statuses.rows.length - 1){
						
					var fieldsNames = ['Created_at','Nome','Iccid','SIM','FW_V','Send_int','IP','Tx time','nrip','Event Time','Fix time','Gps X','Gps Y','Gps Z','Gps D','Gps Status','Gps Status time','Gps Sat','Pdop','Hdop','Vdop','Vel','Gsm Level','Gsm Status','Gsm Status Time','Gsm Operator','Gsm Mod','Acc X ist','Acc Y ist','Acc Z ist','Acc X Sqm','Acc Y Sqm','Acc Z Sqm','Acc X filtered','Acc Y filtered','Acc Z filtered','Acc X Sqm Filtered','Acc Y Sqm Filtered','Acc Z Sqm Filtered','Temperature','Key Status','Key time','Power Status','Power status time','Alarm status','alarm status time','Blink Status','Blink status time','event id','codice evento','Batteria','Firmware VC', 'Error Code'];
					json2csv({data:finals, fields:['created_at','device_name','device_code','sim','firmware_version','send_int','ip_address','timetx','nrip', 'timev','timefix','gpsx','gpsy','gpsz','gpsd','gpsst','gpsstime','gpssat','pdop','hdop','vdop','gpsvel','gsmlvl','gsmst','gsmstime','gsmopn','gsmmod','accxist','accyist','acczist','accxsqm','accysqm','acczsqm','accxflt','accyflt','acczflt','accxsqmflt','accysqmflt','acczsqmflt','temp','keyst','keytime','pwrst','pwrstime','almst','almstime','blkst','blkstime','event_id','codevent','battery','fwvc','errorcode'],fieldsNames:fieldsNames, del:';'},function(err, csv){
						res.writeHead(200, {
							'Content-Type' : 'text/plain',
							'Content-Disposition' : 'attachment; filename='+_filename+'.txt'
						});
						res.end(csv);
					});
				}
			});
      }
    });
		
	}
	else
	{
		res.status(401).send({});
	}

}; 

exports.kml = function(req, res) {
	var async = require('async');
	var searchObj = {};

	var _filename = 'Map_export';
	
	var devices = [];
	var codevents = [];
	var last_device = '';
	var last_day = '';
	var coordinates = '';
	var i = 0;

	if(req.query.file !== undefined && req.query.file !== ''){
		_filename = req.query.file;
	}

	if(req.query.device !== undefined && req.query.device !== ''){
		devices = req.query.device.split(',');
		
	}

	if(req.query.codevent !== undefined && req.query.codevent !== ''){
		codevents = req.query.codevent.split(',');
	}

	if (req.query.from_date !== undefined && req.query.from_date !== '') {
		if (req.query.to_date !== undefined && req.query.to_date !== '') {
			searchObj =
				's.created_at >= \''+ new Date(req.query.from_date).toISOString() +
				'\'::timestamp with time zone AND s.created_at <= \''+ new Date(req.query.to_date).toISOString()+'\'::timestamp with time zone';
				
		} else {
			searchObj = 's.created_at >= \'' + new Date(req.query.from_date).toISOString()+'\'::timestamp with time zone';
			
		}
	} else if (req.query.to_date !== undefined && req.query.to_date !== '') {
		searchObj = 's.created_at <= \'' + new Date(req.query.to_date).toISOString()+'\'::timestamp with time zone';	
	}

	//if(req.headers['host'] == 'reserved.rototracer.com' || req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		


	var sql = format('WITH all_ as (SELECT s.*, e.codevent, e.timev, d.sim, d.device_code, ROW_NUMBER() OVER (PARTITION BY s.device_id) as rk from statuses as s left join events as e on e.id = s.event_id left join devices as d on d.id = s.device_id WHERE s.device_id = ANY(ARRAY[%L]::bigint[]) and e.codevent = ANY(ARRAY[%L]::character varying[]) and '+searchObj+' ORDER BY s.device_name DESC, s.created_at DESC) select * from all_ where rk < 10000',
		devices,
		codevents
	);

	console.log(sql);

	pgClient.query(sql, function(err, statuses) {
      if (err) {
        return res.status(500).send(err);
      }
      
      if(statuses.rows.length == 0){
      	return res.status(200).send({});
      }
   	  else if(statuses.rows.length > 0){
   	  		
	
	    	last_device = statuses.rows[0].device_name;

	    	last_day = moment(new Date(statuses.rows[0].created_at)).format('DD/MM/YYYY');
	    	var response = '<?xml version="1.0" encoding="UTF-8"?>'+
						'<kml xmlns="http://www.opengis.net/kml/2.2">'+
						'<Document>'+
						'	<name>export.kml</name>'+
						'<Style id="downArrowIcon">'+
					      '<IconStyle>'+
					        '<Icon>'+
					          '<href>https://asset.beebeeboard.com/rototracer/siren.png</href>'+
					        '</Icon><hotSpot x="0"  y="0" xunits="pixels" yunits="pixels"/> '+
					     '</IconStyle>'+
					     ' <LineStyle>'+
					     	'<color>ffffffff</color>'+
				        		'<width>3</width>'+
							'</LineStyle>'+
					    '</Style><Folder><name>'+last_device+'</name><Folder><name>'+last_day+'</name>';
			
	    	//async.eachSeries(ev,
	    	//	function(evento, callback){
	    	var cnt = 0;
			statuses.rows.forEach(function(evento){
	    		
				var gval = 4096;
	    		var description = '';
				var placemark = '';
				
	    		//Status.findOne({
				//	'event_id': evento._id
				//}, function(err, status){
				//	if(err){
				//		return res.status(500).send(err);
				//	}
					//console.log(moment(new Date(evento.created_at)).format('DD/MM/YYYY'));
				if(moment(new Date(evento.created_at)).format('DD/MM/YYYY') != last_day && evento.device_name != last_device){
	    			
	    			response += '<Placemark>'+
							'<name>'+last_day+'</name>'+
							'<styleUrl>#downArrowIcon</styleUrl>'+
							'<LineString>'+
							'	<tessellate>1</tessellate>'+
							'	<coordinates>'+coordinates+
							'	</coordinates>'+
							'</LineString>'+
						'</Placemark>';
					var timezone = moment_timezone.tz(moment(evento.created_at), 'Europe/Rome').format('Z');
					var hours = parseInt(timezone.split(':')[0]);
					response += '</Folder></Folder><Folder><name>'+evento.device_name+'</name><Folder><name>'+moment(evento.created_at).add(hours,'h').format('DD/MM/YYYY HH:mm:ss')+'</name>';	
					
					coordinates = '';
					last_day = moment(new Date(evento.created_at)).format('DD/MM/YYYY');
					last_device = evento.device_name;

	    		}
	    		else if(moment(new Date(evento.created_at)).format('DD/MM/YYYY') != last_day && evento.device_name == last_device)
	    		{
	    			response += '<Placemark>'+
	    			
							'<name>'+last_day+'</name>'+
							'<styleUrl>#downArrowIcon</styleUrl>'+
							'<LineString>'+
							'	<tessellate>1</tessellate>'+
							'	<coordinates>'+coordinates+
							'	</coordinates>'+
							'</LineString>'+
						'</Placemark>';
					response += '</Folder><Folder><name>'+moment(new Date(evento.created_at)).format('DD/MM/YYYY')+'</name>';	
					
					coordinates = '';
					last_day = moment(new Date(evento.created_at)).format('DD/MM/YYYY');
	    		}
	    		else if(moment(new Date(evento.created_at)).format('DD/MM/YYYY') == last_day && evento.device_name != last_device){
	    			response += '</Folder></Folder><Folder><name>'+evento.device_name+'</name><Folder><name>'+moment_timezone.tz(ev.created_at,'YYYY-MM-DDThh:mm:ssZ','Europe/Rome').format('DD/MM/YYYY HH:mm:ss')+'</name>';
	    			last_device = evento.device_name;
	    		}
	    		if(evento && evento.device_name != undefined)
				{	
					
					var name = '<name>'+(evento.codevent == 9999 ? 'Accensione - ' :'')+moment(new Date(evento.timev)).format('DD/MM HH:mm')+'</name>';
					var google = convert_to_google_coord(evento.gpsx, evento.gpsy, evento.ns, evento.ew);
					var gpsx = google.split(',')[0];
					var gpsy = google.split(',')[1];
					var acc_ist = parseFloat((Math.sqrt((evento.accxist^2) + (evento.accyist^2) + (evento.acczist^2)))/gval); 
					var accxmed = parseFloat(evento.accxsqmflt/gval);
					var accymed = parseFloat(evento.accysqmflt/gval);
					var acczmed = parseFloat(evento.acczsqmflt/gval);
					/*console.log('radice: '+parseFloat((Math.sqrt((evento.accxflt^2) + (evento.accyflt^2)))));
					console.log('divisione: '+parseFloat(evento.acczflt/parseFloat((Math.sqrt((evento.accxflt^2) + (evento.accyflt^2))))));
					console.log('atan: '+parseFloat((Math.atan(parseFloat(evento.acczflt/parseFloat((Math.sqrt((evento.accxflt^2) + (evento.accyflt^2)))))))));
				*/
					var acc_basso = 90 - (parseFloat((Math.atan(parseFloat(evento.acczflt/parseFloat((Math.sqrt((evento.accxflt*evento.accxflt) + (evento.accyflt*evento.accyflt))))))))*180/3.14);
					var acc_rot = evento.accxflt > 0 ? (Math.atan(evento.accyflt/evento.accxflt)) : (180-Math.atan(evento.accyflt/evento.accxflt));
					
					placemark = '<Placemark>'+ name + 
					
				        '<visibility>0</visibility>'+
				        '<description>';

					description = '<![CDATA[<br><br><font size=5 color="blue">'+evento.device_name+'</font><br/><br/>'+
						'ICCID: '+ evento.device_code+'</br>'+
						'SIM: '+ evento.sim+'</br>'+
						'Tipo di evento: '+ (evento.codevent == 9999 ? 'Accensione':'Periodico')+'</br>'+
						'Tempo dell\'evento: '+ moment(new Date(evento.timev)).format('DD/MM/YYYY HH:mm') +
						'</br></br><font size=4>Stato Dispositivo</font></br></br>'+
						'Input/Output </br>'+
						'<table padding="1">'+
						'	<tr><td>Lampeggiante</td><td>Stato: '+evento.blkst+'</td><td>alle '+moment(new Date(evento.blkstime)).format('DD/MM/YYYY HH:mm')+'</td></tr>'+
						'	<tr><td>Chiave Motore</td><td>Stato: '+evento.keyst+'</td><td>alle '+moment(new Date(evento.keytime)).format('DD/MM/YYYY HH:mm')+'</td></tr>'+
						'	<tr><td>Power Extra</td><td>Stato: '+evento.pwrst+'</td><td>alle '+moment(new Date(evento.pwrstime)).format('DD/MM/YYYY HH:mm')+'</td></tr>'+
						'	<tr><td>Allarme</td><td>Stato: '+evento.almst+'</td><td>alle '+moment(new Date(evento.almstime)).format('DD/MM/YYYY HH:mm')+'</td></tr>'+							
						'</table>'+
						'</br></br>'+
						'Gsm </br>'+
						'<table padding="1">'+
						'	<tr><td>Segnale</td><td>Livello: '+evento.gsmlvl+'</td><td> pari al '+parseInt(evento.gsmlvl*100/31)+'%</td></tr>'+
						'	<tr><td>Modalità</td><td>'+evento.gsmmod+'</td><td>'+(evento.gsmmod == 0 ? 'Non registrato' : 'Registrato')+' sul Network</td></tr>'+
						'	<tr><td>Operatore</td><td>'+(evento.gsmopn !== undefined ? evento.gsmopn : 'Non definito')+'</td><td></td></tr>'+
						'</table>'+
						'</br></br>'+
						'Moto </br>'+
						'<table padding="1">'+
						'	<tr><td>Coordinate GPS</td><td>[' + gpsx + ',</td><td> ' + gpsy + ']</td></tr><tr><td>Hdop: '+ (Math.round(evento.hdop*100)/100)+'</td></tr>'+
						'	<tr><td>Quota GPS</td><td>'+parseInt(evento.gpsz)+'</td></tr><tr><td>Vdop: '+(Math.round(evento.vdop*100)/100)+'</td></tr>'+
						'	<tr><td>Direzione GPS</td><td>'+parseInt(evento.gpsd)+'°</td><td></td></tr>'+
						'	<tr><td>Velocità GPS</td><td>'+evento.gpsvel+' km/h</td><td></td></tr>'+
						'	<tr><td>Satelliti GPS</td><td>'+evento.gpssat+'</td><td></td></tr>'+
						'	<tr><td>Stato GPS</td><td>'+evento.gpsst+'</td><td>'+(evento.gpsst == 1 ? 'No Fix' : (evento.gpsst == 2 ? '2D' : '3D'))+'</td></tr>'+
						'</table>'+
						'</br></br>'+
						'Accelerazioni dispositivo </br>'+
						'<table padding="1">'+
						'	<tr><td>Istantanea</td><td>'+(Math.round(acc_ist*100)/100)+' g</td><td></td></tr>'+
						'	<tr><td>Medie </td><td>X: '+(Math.round(evento.accxflt/gval*100)/100)+'</td><td>Y: '+(Math.round(evento.accyflt/gval*100)/100)+'</td><td>Z: '+(Math.round(evento.acczflt/gval*100)/100)+'</td></tr>'+
						'	<tr><td>Osc Media </td><td>X: '+(Math.round(accxmed*100)/100)+'</td><td>Y: '+(Math.round(accymed*100)/100)+'</td><td>Z: '+(Math.round(acczmed*100)/100)+'</td></tr>'+
						'	<tr><td>Incl. basso</td><td>'+(Math.round(acc_basso*100)/100)+'°</td><td></td></tr>'+
						'	<tr><td>Rotazione oraria</td><td>'+(Math.round(acc_rot*100)/100)+'°</td><td></td></tr>'+
						'</table>'+
						'</br></br>'+
						'Altri dati </br>'+
						'<table padding="1">'+
						'	<tr><td>Temperatura</td><td>'+(parseInt(evento.temp)-4)+' °C</td><td></td></tr>'+
						'	<tr><td>Battery Level</td><td>'+evento.battery+'</td><td></td></tr>'+
						'	<tr><td>FW Vc</td><td>'+evento.fwvc+'</td><td></td></tr>'+
						'	<tr><td>FW version</td><td>'+evento.firmware_version+'</td><td></td></tr>'+
						
							
						'</table>'+
						']]>';

						placemark += description + '</description>';

						if(parseInt(gpsx) != 0 && parseInt(gpsy) != 0){
							placemark += '<LookAt>'+
							     '  <longitude>'+gpsy +'</longitude>'+
							     '  <latitude>'+gpsx +'</latitude></LookAt>'+
						    	  '<Point>'+
						          '<coordinates>'+gpsy +','+gpsx +',0</coordinates>'+
						        '</Point><styleUrl>#downArrowIcon</styleUrl>';
						    coordinates += gpsy + ','+gpsx+',0 ';
						}
						placemark += '</Placemark>';
				        response += placemark; 
				}
						
	    		
	    		if(cnt++ >= statuses.rows.length-1){
	    			response += '<Placemark>'+
	    			
						'<name>'+last_day+'</name>'+
						'<styleUrl>#downArrowIcon</styleUrl>'+
						'<LineString>'+
						'	<tessellate>1</tessellate>'+
						'	<coordinates>'+coordinates+
						'	</coordinates>'+
						'</LineString>'+
					'</Placemark>';
	    			response += '</Folder></Folder></Document></kml>';
	    			res.writeHead(200, {
						'Content-Type' : 'application/vnd.google-earth.kml+xml',
						'Content-Disposition' : 'attachment; filename='+_filename+'.kml'
					});
					res.end(response);
	    		}
	    	//);
	    	});
				
		}
	});
	/*else
	{
		res.status(401).send({});
	}*/

}; 

exports.immediate_mail = function(req, res){
	
	var mandrill = require('mandrill-api/mandrill');
	var async = require('async');
	var searchObj = {};
	
	var timezone = require('moment-timezone');
	var moment = require('moment');
	var gval = 4096;
	moment.locale('it');
	var devices = [];
	var i = 0;

	if(req.query.device !== undefined && req.query.device !== ''){
		devices = req.query.device.split(',');
	}

	//if(req.headers['host'] == 'reserved.rototracer.com' || req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('WITH all_ as (SELECT s.device_name as dev_name,s.*, e.*,d.*,p.*, ROW_NUMBER() OVER(PARTITION BY s.device_id order by s.created_at DESC) as rk from statuses as s left join devices as d on d.id = s.device_id left join events as e on e.id = s.event_id left join parameters as p on p.device_id = d.id where s.device_id =ANY(ARRAY[%L]::bigint[]) and e.codevent <> %L) SELECT * from all_ where rk = 1',
			devices,
			'8888'
			);


		pgClient.query(sql, function(err, statuses) {
	      if (err) {
	        return res.status(500).send(err);
	      }
	      
	      if(statuses.rows.length > 0)
	      {
	      	var cnt = 0;
	      	statuses.rows.forEach(function(status){
	      		var tipo_evento = find_event_name(parseInt(status.codevent));
				google = convert_to_google_coord(status.gpsx, status.gpsy, status.ns, status.ew);
				var gpsx = google.split(',')[0];
				var gpsy = google.split(',')[1];
				var acc_ist = parseFloat((Math.sqrt((status.accxist*status.accxist) + (status.accyist*status.accyist) + (status.acczist*status.acczist)))/gval); 
				var accxmed = parseFloat(status.accxsqmflt/gval);
				var accymed = parseFloat(status.accysqmflt/gval);
				var acczmed = parseFloat(status.acczsqmflt/gval);
				var acc_basso = 90 - (parseFloat((Math.atan(parseFloat(status.acczflt/parseFloat((Math.sqrt((status.accxflt*status.accxflt) + (status.accyflt*status.accyflt))))))))*180/3.14);
				var acc_rot = status.accxflt > 0 ? (Math.atan(status.accyflt/status.accxflt)) : (180-Math.atan(status.accyflt/status.accxflt));
				
				var to_mail = [];
				var mails = find_mail_for_event(status, parseInt(status.codevent));

				if(mails){
					mails.forEach(function(m){
					
						var to = {};
						to.email = m;
						to.type = 'to';
						to.name=m;
						to_mail.push(to);
						
					});
					if(mails.length > 0){
						
						var dur_evento = find_duration_event(status, parseInt(status.codevent)); 
						var template_content = [{
							"name" : "SIM",
							"content" : status.sim
						}, {
							"name" : "ICCID",
							"content" : status.device_code
						}, {
							"name" : "DEVICE_NAME",
							"content" : status.dev_name
						}, {
							"name" : "EVENTO",
							"content" : tipo_evento
						},{
							"name" : "GSMLVL",
							"content" : status.gsmlvl
						}, {
							"name" : "GSMLVL_PERC",
							"content" : parseInt(status.gsmlvl*100/31)
						}, {
							"name" : "GSMMOD",
							"content" : status.gsmmod
						}, {
							"name" : "GSMMOD_REG",
							"content" : status.gsmmod == 0 ? 'Non registrato' : 'Registrato'
						}, {
							"name" : "GSMOPN",
							"content" : status.gsmopn
						}, {
							"name" : "DATE",
							"content" : status.timev ? moment(new Date(status.timev)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "BLKST",
							"content" : status.blkst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "BLKSTIME",
							"content" :  status.blkstime ? moment(new Date(status.blkstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_BLKSTIME",
							"content" : moment(new Date(status.blkstime)).from(new Date(status.timev))
						}, {
							"name" : "KEYST",
							"content" : status.keyst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "KEYSTIME",
							"content" :  status.keytime ? moment(new Date(status.keytime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_KEYSTIME",
							"content" : moment(new Date(status.keytime)).from(new Date(status.timev))
						}, {
							"name" : "PWRST",
							"content" : status.pwrst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "PWRSTIME",
							"content" :  status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_PWRSTIME",
							"content" : moment(new Date(status.pwrstime)).from(new Date(status.timev))
						}, {
							"name" : "ALMST",
							"content" : status.almst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "ALMSTIME",
							"content" : status.almstime ? moment(new Date(status.almstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_ALMSTIME",
							"content" : moment(new Date(status.almstime)).from(new Date(status.timev))
						}, {
							"name" : "PWRST",
							"content" : status.pwrst == 0 ? 'OFF' : 'ON'
						},{
							"name" : "PWRSTIME",
							"content" : status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_PWRSTIME",
							"content" : moment(new Date(status.pwrstime)).from(new Date(status.timev))
						}, {
							"name" : "POSIZIONE",
							"content" : "[" + gpsx + ", " + gpsy + "]"
						}, {
							"name" : "MAPSOURCE",
							"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
						}, {
							"name" : "MAPSOURCEGENERAL",
							"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=10&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
						}, {
							"name" : "MAPSOURCELINK",
							"content" : "https://www.google.com/maps?q="+google+"&ll="+google+"&z=15"
						}, {
							"name" : "MAPSOURCEGENERALLINK",
							"content" : "https://www.google.com/maps?q="+google+"&ll="+google+"&z=10"
						},{ 
							"name" : "GPSX",
							"content" : gpsx
						}, {
							"name" : "GPSY",
							"content" : gpsy
						}, { 
							"name" : "GPSHDOP",
							"content" : Math.round(status.hdop*100)/100
						}, { 
							"name" : "GPSZ",
							"content" : parseInt(status.gpsz)
						}, { 
							"name" : "GPSVDOP",
							"content" : Math.round(status.vdop*100)/100
						}, { 
							"name" : "GPSD",
							"content" : parseInt(status.gpsd)
						}, { 
							"name" : "GPSVEL",
							"content" : status.gpsvel
						}, { 
							"name" : "GPSSAT",
							"content" : status.gpssat
						}, { 
							"name" : "GPSST",
							"content" : status.gpsst
						}, { 
							"name" : "GPSST_DEF",
							"content" : status.gpsst == 1 ? 'No Fix' : (status.gpsst == 2 ? '2D' : '3D')
						}, { 
							"name" : "ACC_IST",
							"content" : Math.round(acc_ist*100)/100
						}, { 
							"name" : "ACCXFLT",
							"content" : Math.round(status.accxflt/gval*100)/100
						}, { 
							"name" : "ACCYFLT",
							"content" : Math.round(status.accyflt/gval*100)/100
						}, { 
							"name" : "ACCZFLT",
							"content" : Math.round(status.acczflt/gval*100)/100
						}, { 
							"name" : "ACCXMED",
							"content" : Math.round(accxmed*100)/100
						}, { 
							"name" : "ACCYMED",
							"content" : Math.round(accymed*100)/100
						}, { 
							"name" : "ACCZMED",
							"content" : Math.round(acczmed*100)/100
						}, { 
							"name" : "BASSO",
							"content" : Math.round(acc_basso*100)/100
						}, { 
							"name" : "ROTAZIONE",
							"content" : Math.round(acc_rot*100)/100
						}, { 
							"name" : "TEMP",
							"content" : parseInt(status.temp)-4
						}, { 
							"name" : "FIRMWARE",
							"content" : (status.firmware_version ? status.firmware_version : 'Information not avalaible')
						}, { 
							"name" : "FWVC",
							"content" : (status.fwvc ? status.fwvc : 'Information not avalaible')
						},
						], message = {
							"global_merge_vars" : template_content,
							"subject" : status.dev_name+' - '+ tipo_evento+' - '+(status.timev ? moment(new Date(status.timev)).format('DD/MM/YYYY HH:mm') : '-') +' ' + dur_evento,
							"from_email" : "no-reply@move2web.it",
							"from_name" : 'Rototracer App',
							"to" : to_mail,
							"headers" : {
								"Reply-To" : 'rototracer@move2web.it'
							}
						};
						
						var mandrill_client = new mandrill.Mandrill(config.mandrill.api);
						mandrill_client.messages.sendTemplate({
							"template_name" : "rototracer-event-mail",
							"template_content" : template_content,
							"message" : message,
							"async" : true,
							"send_at" : false
						}, function(result) {
							
							result[0].device_name = status.dev_name;
							result[0].status_id = status.id;
							result[0].device_id = status.device_id;
							var mandr_sql = format('INSERT INTO mandrills (status_id,device_id,email, status,device_name) VALUES(%L,%L,%L,%L,%L)',
									result[0].status_id,
									result[0].device_id,
									JSON.stringify(to_mail),
									result[0].status,
									result[0].device_name
								);
							 pgClient.query(mandr_sql, function(err, product) {
						        if (err ) {
						         	console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mail event -> Mandrill product' + err);
								}
							  });
							if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
								console.log('Mail sended');
								if(cnt++ >= statuses.rows.length-1){
									res.status(200).send({});
								}
							}

						});
						
					}
				}
				
	      	});
		  }
		  else
		  {
		  	res.status(404).json({});
		  }
	    });

};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New status call
 */
exports.index = function(req, res) {
	
  
};

function find_mail_for_event(dev,code){
	var m = [];
	switch(code){
		case 9999:
			m = dev.mail ? dev.mail.split(';') : null;
		break;
		case 100:
			m = dev.mail0 ? dev.mail0.split(';') : null;
		break;
		case 101:
			m = dev.mail1 ? dev.mail1.split(';') : null;
		break;
		case 102:
			m = dev.mail2 ? dev.mail2.split(';') : null;
		break;
		case 103:
			m = dev.mail3 ? dev.mail3.split(';') : null;
		break;
		case 104:
			m = dev.mail4 ? dev.mail4.split(';') : null;
		break;
		case 105:
			m = dev.mail5 ? dev.mail5.split(';') : null;
		break;
		case 107:
			m = dev.mail7 ? dev.mail7.split(';') : null;
		break;
		case 108:
			m = dev.mail8 ? dev.mail8.split(';') : null;
		break;
		case 109:
			m = dev.mail9 ? dev.mail9.split(';') : null;
		break;
		case 110:
			m = dev.mail10 ? dev.mail10.split(';') : null;
		break;
		case 111:
			m = dev.mail11 ? dev.mail11.split(';') : null;
		break;
		case 112:
			m = dev.mail12 ? dev.mail12.split(';') : null;
		break;
		case 113:
			m = dev.mail13 ? dev.mail13.split(';') : null;
		break;
		case 114:
			m = dev.mail14 ? dev.mail14.split(';') : null;
		break;
		case 115:
			m = dev.mail15 ? dev.mail15.split(';') : null;
		break;
		case 116:
			m = dev.mail16 ? dev.mail16.split(';') : null;
		break;
		case 117:
			m = dev.mail17 ? dev.mail17.split(';') : null;
		break;
		case 118:
			m = dev.mail18 ? dev.mail18.split(';') : null;
		break;
		case 119:
			m = dev.mail19 ? dev.mail19.split(';') : null;
		break;
		case 120:
			m = dev.mail20 ? dev.mail20.split(';') : null;
		break;
		case 121:
			m = dev.mail21 ? dev.mail21.split(';') : null;
		break;
		case 122:
			m = dev.mail22 ? dev.mail22.split(';') : null;
		break;
		case 123:
			m = dev.mail23 ? dev.mail23.split(';') : null;
		break;
		case 124:
			m = dev.mail24 ? dev.mail24.split(';') : null;
		break;
		case 125:
			m = dev.mail25 ? dev.mail25.split(';') : null;
		break;
		case 126:
			m = dev.mail26 ? dev.mail26.split(';') : null;
		break;
		default:
			m = dev.mail ? dev.mail.split(';') : null;
		break;
	}
	console.log(m);
	return m;
	
};

function find_event_name(code){
	var m = '';
	switch(code){
		case 9999:
			m = 'ACCENSIONE';
		break;
		case 100:
			m = 'KEY';
		break;
		case 101:
			m = 'BLINK';
		break;
		case 102:
			m = 'POWER';
		break;
		case 103:
			m = 'ALARM';
		break;
		case 104:
			m = 'STOP';
		break;
		case 105:
			m = 'GO';
		break;
		case 106:
			m = 'PERIODIC';
		break;
		case 107:
			m = 'JAM';
		break;
		case 108:
			m = 'WARMUP';
		break;
		case 109:
			m = 'WARMDOWN';
		break;
		case 110:
			m = 'FLUTTER';
		break;
		case 111:
			m = 'TILT';
		break;
		case 112:
			m = 'VELOVER';
		break;
		case 113:
			m = 'VELLOW';
		break;
		case 114:
			m = 'ALTOVER';
		break;
		case 115:
			m = 'CRASH';
		break;
		case 116:
			m = 'OUT1';
		break;
		case 117:
			m = 'OUT2';
		break;
		case 118:
			m = 'OUT3';
		break;
		case 119:
			m = 'FENCE1';
		break;
		case 120:
			m = 'FENCE2';
		break;
		case 121:
			m = 'FENCE3';
		break;
		case 122:
			m = 'EXIT F1';
		break;
		case 123:
			m = 'EXIT F2';
		break;
		case 124:
			m = 'EXIT F3';
		break;
		case 125:
			m = 'CRASH 2';
		break;
		case 126:
			m = 'LOW BATTERY';
		break;
		default:
			m = '';
		break;
	}
	return m;
	
};

function find_duration_event(parameter,code){
	var m = '';
	if(parameter){
		switch(code){
			case 100:
				m = 'osservato da '+parameter.dur0 +' sec';
			break;
			case 101:
				m = 'osservato da '+parameter.dur1 +' sec';
			break;
			case 102:
				m = 'osservato da '+parameter.dur2 +' sec';
			break;
			case 103:
				m = 'osservato da '+parameter.dur3 +' sec';
			break;
			case 104:
				m = 'osservato da '+parameter.dur4 +' sec';
			break;
			case 105:
				m = 'osservato da '+parameter.dur5 +' sec';
			break;
			case 107:
				m = 'osservato da '+parameter.dur7 +' sec';
			break;
			case 108:
				m = 'osservato da '+parameter.dur8 +' sec';
			break;
			case 109:
				m = 'osservato da '+parameter.dur9 +' sec';
			break;
			case 110:
				m = 'osservato da '+parameter.dur10 +' sec';
			break;
			case 111:
				m = 'osservato da '+parameter.dur11 +' sec';
			break;
			case 112:
				m = 'osservato da '+parameter.dur12+' sec';
			break;
			case 113:
				m = 'osservato da '+parameter.dur13 +' sec';
			break;
			case 114:
				m = 'osservato da '+parameter.dur14 +' sec';
			break;
			case 115:
				m = 'osservato da '+parameter.dur15 +' sec';
			break;
			case 116:
				m = 'osservato da '+parameter.dur16 +' sec';
			break;
			case 117:
				m = 'osservato da '+parameter.dur17 +' sec';
			break;
			case 118:
				m = 'osservato da '+parameter.dur18 +' sec';
			break;
			case 119:
				m = 'osservato da '+parameter.dur19 +' sec';
			break;
			case 120:
				m = 'osservato da '+parameter.dur20 +' sec';
			break;
			case 121:
				m = 'osservato da '+parameter.dur21 +' sec';
			break;
			case 122:
				m = 'osservato da '+parameter.dur22 +' sec';
			break;
			case 123:
				m = 'osservato da '+parameter.dur23 +' sec';
			break;
			case 124:
				m = 'osservato da '+parameter.dur24 +' sec';
			break;
			case 125:
				m = 'osservato da '+parameter.dur25 +' sec';
			break;
			case 126:
				m = 'osservato da '+parameter.dur26 +' sec';
			break;
			default:
				m = '';
			break;
		}
	}
	
	return m;
	
};

function convert_to_google_coord(gpsx, gpsy, ns, ew){

	s = gpsx + ","+ns+","+gpsy+","+ew;
	//s = '4501.93701171875,N,738.6063842773438,E'
	g = s.split(",");

	if(parseInt(g[0])<10){
		g[0] = '000'+g[0];
	}
	else if(parseInt(g[0])<100){
		g[0] = '00'+g[0];
	}
	else if(parseInt(g[0]) <1000){
		g[0] = '0'+g[0];
	}

	gradi_lat = g[0].substr(0, 2);
	if(g[1] == 'N'){
		lat = parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60);
	}
	else
	{
		lat = -1 * (parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60));
	}

	if(parseInt(g[2])<10){
		g[2] = '0000'+g[2];
	}
	else if(parseInt(g[2])<100){
		g[2] = '000'+g[2];
	}
	else if(parseInt(g[2]) <1000){
		g[2] = '00'+g[2];
	}
	else
	{
		g[2] = '0'+g[2];
	}

	gradi_lon = g[2].substr(0, 3);
	if(g[3] == 'W'){
		lon = -1 * (parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60));
	}
	else
	{	
		lon = parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60);
	}
	return lat.toFixed(8)+","+lon.toFixed(8);


};
