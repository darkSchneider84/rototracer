var moment = require('moment'), mongoose = require('mongoose'), Device = mongoose.model('Device');
var config = require('../../config');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index_admin = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_admin){
			
		Device.find({
		}, function(err, devices){
			if (err) {
				return res.status(500).send(err);
			}
			
			res.json({
				devices : devices
			});
		}).sort({
		  	'device_name': 1
		 });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Device.find({
			'user_beebeeboard_id': parseInt(req.query.user_id)
		}, function(err, devices){
			if (err) {
				console.log(err);
				return res.status(500).send(err);
			}
			
			res.json({
				devices : devices
			});
		}).sort({
		  	'device_name': 1
		 });;
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.find_from_device_bb = function(req, res){
	var Parameter = mongoose.model('Parameter'), Status = mongoose.model('Status');
	var moment = require('moment');
	Device.findOne({
		'device_beebeeboard_id': req.params.id
	}, function(err, device){
		if (err) {
			return res.status(500).send(err);
		}
		
		Parameter.findOne({
			device_id: device._id
		}, function(err,parameters){
			if (err) {
				return res.status(500).send(err);
			}

			Status.findOne({
				device_id: device._id
			}, function(err, status){
				if (err) {
					return res.status(500).send(err);
				}
				if(status){
					status.timev = moment(new Date(status.timev)).subtract(2,'hours');
				}
				res.json({
					device : device,
					parameters : parameters,
					status: status
				});
			}).sort({
				'created_at':-1
			});

		});

		
	});

};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.find_from_iccid = function(req, res) {

//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Device.find({
			'device_code': req.query.device_code
		}, function(err, devices){
			if (err) {
				return res.status(500).send(err);
			}
			
			res.json({
				devices : devices
			});
		});
/*	}
	else
	{
		res.status(401).send({});
	}*/
};



/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific device searched by id
 */
exports.find = function(req, res) {
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Device.findOne({
			'user_beebeeboard_id' : req.query.user_id,
			'_id' : req.params.id
		}, function(err, device) {
			if (err) {
				return res.status(500).send(err);
			}
			if(!device)
			{
				return res.status(404).send({});
			}
			
			res.json({
				device : device
			});
		});
	}
	else
	{
		res.status(401).send({});
	}
};

exports.update_from_bb = function(req, res){
	Device.findOneAndUpdate({
		'device_beebeeboard_id' : req.params.id
	}, req.body.device,{'new':true},  function(err, device) {
		if (err) {
			return res.status(500).send(err);
		}

		res.json({
			device : device
		});
	});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update = function(req, res) {
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Device.findOneAndUpdate({
			'user_beebeeboard_id' : req.query.user_id,
			'_id' : req.params.id
		}, req.body.device,{'new':true},  function(err, device) {
			if (err) {
				return res.status(500).send(err);
			}

			res.json({
				device : device
			});
		});
	}
	else
	{
		res.status(401).send({});
	}
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update_user_bb_id = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		Device.findOneAndUpdate({
			'_id' : req.params.id
		}, req.body.device,{'new':true},  function(err, device) {
			if (err) {
				return res.status(500).send(err);
			}

			res.json({
				device : device
			});
		});
	/*}
	else
	{
		res.status(401).send({});
	}*/
};


/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing device searched by id
 */
exports.update_bb_id = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		
		Device.findOneAndUpdate({
			'device_beebeeboard_id' : req.params.id
		}, {
			'$set':{
				'user_beebeeboard_id': req.query.user_id
			}
		},{'new':true},  function(err, device) {
			if (err) {
				return res.status(500).send(err);
			}
			res.json({
				device : device
			});
		});
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.create = function(req, res) {
  
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
		Device(req.body.device)
	    .save(function (err, device) {
	      if (err) {
	        return res.status(500)
	          .send(err);
	      }

	      res.json({
	        device: device
	      });
	    });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing device searched by id
 */
exports.destroy = function(req, res) {
	
	if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
		Device.findOneAndRemove({
			'user_beebeeboard_id' : req.query.user_id,
			'_id' : req.params.id
		}, function(err, device) {
			if (err) {
				return res.status(500).send(err);
			}
			res.status(200).send({});
		});
	}
	else
	{
		res.status(401).send({});
	}
};