var moment = require('moment'),
pg = require('pg'),
format = require('pg-format'); 
var config = require('../../config');
/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Index page filter by tenant_id and other possibles parameters
 * Paginate by 50
 */
exports.index = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	
		var sql = format('SELECT parameter_lists(%L, null);',
			req.query.device_id);

		pgClient.query(sql, function(err, parameters) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(parameters && parameters.rows && parameters.rows.length > 0){
          	res.json(parameters.rows[0].parameter_lists);
          }
          else
          {
          	res.status(404).json({});
          }
        });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Find a specific Parameter searched by id
 */
exports.find = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var sql = format('SELECT parameter_lists(null, %L);',
			req.params.id);

		pgClient.query(sql, function(err, parameters) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(parameters && parameters.rows && parameters.rows.length > 0){
          	res.json(parameters.rows[0].parameter_lists);
          }
          else
          {
          	res.status(404).json({});
          }
        });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Update an existing Parameter searched by id
 */
exports.update = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
		var async = require('async');
		req.body.parameter = set_limits_parameter(req.body.parameter);

		async.parallel({
			part1: function(callback){
				var sql = format('SELECT update_parameter_from_device_id_part_1(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
					req.params.id,
					null,
					req.body.parameter.device_name,
					req.body.parameter.event_enable,
					req.body.parameter.dur0,
					req.body.parameter.dur1,
					req.body.parameter.dur10,
					req.body.parameter.dur11,
					req.body.parameter.dur12,
					req.body.parameter.dur13,
					req.body.parameter.dur14,
					req.body.parameter.dur15,
					req.body.parameter.dur2,
					req.body.parameter.dur26,
					req.body.parameter.dur3,
					req.body.parameter.dur4,
					req.body.parameter.dur5,
					req.body.parameter.dur6,
					req.body.parameter.dur7,
					req.body.parameter.dur8,
					req.body.parameter.dur9,
					req.body.parameter.max0,
					req.body.parameter.max1,
					req.body.parameter.max10,
					req.body.parameter.max11,
					req.body.parameter.max12,
					req.body.parameter.max13,
					req.body.parameter.max14,
					req.body.parameter.max15,
					req.body.parameter.max2,
					req.body.parameter.max26,
					req.body.parameter.max3,
					req.body.parameter.max4,
					req.body.parameter.max5,
					req.body.parameter.max6,
					req.body.parameter.max7,
					req.body.parameter.max8,
					req.body.parameter.max9,
					req.body.parameter.min0,
					req.body.parameter.min1,
					req.body.parameter.min10,
					req.body.parameter.min11,
					req.body.parameter.min12,
					req.body.parameter.min13,
					req.body.parameter.min14,
					req.body.parameter.min15,
					req.body.parameter.min2,
					req.body.parameter.min26,
					req.body.parameter.min3,
					req.body.parameter.min4,
					req.body.parameter.min5,
					req.body.parameter.min6,
					req.body.parameter.min7,
					req.body.parameter.min8,
					req.body.parameter.min9,
					req.body.parameter.p10_x,
					req.body.parameter.p10_y,
					req.body.parameter.p11_x,
					req.body.parameter.p11_y,
					req.body.parameter.p12_x,
					req.body.parameter.p12_y,
					req.body.parameter.p13_x,
					req.body.parameter.p13_y,
					req.body.parameter.p14_x,
					req.body.parameter.p14_y,
					req.body.parameter.p15_x,
					req.body.parameter.p15_y,
					req.body.parameter.p16_x,
					req.body.parameter.p16_y,
					req.body.parameter.p17_x,
					req.body.parameter.p17_y,
					req.body.parameter.p18_x,
					req.body.parameter.p18_y,
					req.body.parameter.p19_x,
					req.body.parameter.p19_y);


				     pgClient.query(sql, function(err, parameters) {
				      if (err) {
				        callback(true,null);
				      }
				      else
				      {
				      	 callback(null, 1);
				      }
				     
				   	});

			},
			part2: function(callback){
				var sql = format('SELECT update_parameter_from_device_id_part_2(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
					req.params.id,
					req.body.parameter.p1_x,
					req.body.parameter.p1_y,
					req.body.parameter.p20_x,
					req.body.parameter.p20_y,
					req.body.parameter.p2_x,
					req.body.parameter.p2_y,
					req.body.parameter.p3_x,
					req.body.parameter.p3_y,
					req.body.parameter.p4_x,
					req.body.parameter.p4_y,
					req.body.parameter.p5_x,
					req.body.parameter.p5_y,
					req.body.parameter.p6_x,
					req.body.parameter.p6_y,
					req.body.parameter.p7_x,
					req.body.parameter.p7_y,
					req.body.parameter.p8_x,
					req.body.parameter.p8_y,
					req.body.parameter.p9_x,
					req.body.parameter.p9_y,
					req.body.parameter.q10_x,
					req.body.parameter.q10_y,
					req.body.parameter.q1_x,
					req.body.parameter.q1_y,
					req.body.parameter.q2_x,
					req.body.parameter.q2_y,
					req.body.parameter.q3_x,
					req.body.parameter.q3_y,
					req.body.parameter.q4_x,
					req.body.parameter.q4_y,
					req.body.parameter.q5_x,
					req.body.parameter.q5_y,
					req.body.parameter.q6_x,
					req.body.parameter.q6_y,
					req.body.parameter.q7_x,
					req.body.parameter.q7_y,
					req.body.parameter.q8_x,
					req.body.parameter.q8_y,
					req.body.parameter.q9_x,
					req.body.parameter.q9_y,
					req.body.parameter.r10_x,
					req.body.parameter.r10_y,
					req.body.parameter.r1_x,
					req.body.parameter.r1_y,
					req.body.parameter.r2_x,
					req.body.parameter.r2_y,
					req.body.parameter.r3_x,
					req.body.parameter.r3_y,
					req.body.parameter.r4_x,
					req.body.parameter.r4_y,
					req.body.parameter.r5_x,
					req.body.parameter.r5_y,
					req.body.parameter.r6_x,
					req.body.parameter.r6_y,
					req.body.parameter.r7_x,
					req.body.parameter.r7_y,
					req.body.parameter.r8_x,
					req.body.parameter.r8_y,
					req.body.parameter.r9_x,
					req.body.parameter.r9_y,
					req.body.parameter.send_int
					);


				     pgClient.query(sql, function(err, parameters) {
				      if (err) {
				        callback(true,null);
				      }
						else
				      {
				      	 callback(null, 1);
				      }
				   	});
			},
			part3: function(callback){
				var sql = format('SELECT update_parameter_from_device_id_part_3(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
					req.params.id,
					req.body.parameter.dur16,
					req.body.parameter.dur17,
					req.body.parameter.dur18,
					req.body.parameter.dur19,
					req.body.parameter.dur20,
					req.body.parameter.dur21,
					req.body.parameter.dur22,
					req.body.parameter.dur23,
					req.body.parameter.dur24,
					req.body.parameter.dur25,
					req.body.parameter.dur27,
					req.body.parameter.dur28,
					req.body.parameter.dur29,
					req.body.parameter.dur30,
					req.body.parameter.dur31,
					req.body.parameter.dur32,
					req.body.parameter.max16,
					req.body.parameter.max17,
					req.body.parameter.max18,
					req.body.parameter.max19,
					req.body.parameter.max20,
					req.body.parameter.max21,
					req.body.parameter.max22,
					req.body.parameter.max23,
					req.body.parameter.max24,
					req.body.parameter.max25,
					req.body.parameter.max27,
					req.body.parameter.max28,
					req.body.parameter.max29,
					req.body.parameter.max30,
					req.body.parameter.max31,
					req.body.parameter.max33,
					req.body.parameter.min16,
					req.body.parameter.min17,
					req.body.parameter.min18,
					req.body.parameter.min19,
					req.body.parameter.min20,
					req.body.parameter.min21,
					req.body.parameter.min22,
					req.body.parameter.min23,
					req.body.parameter.min24,
					req.body.parameter.min25,
					req.body.parameter.min27,
					req.body.parameter.min28,
					req.body.parameter.min29,
					req.body.parameter.min30,
					req.body.parameter.min31);


				     pgClient.query(sql, function(err, parameters) {
				      if (err) {
				        callback(true,null);
				      }

				      callback(null, 1);
				   	});

			},
		}, function(err, results){
			if(err){

			}

			var dev_sql = format('UPDATE devices SET send_parameter = %L WHERE id = %L',
	      		true,
	      		req.params.id);
	      	pgClient.query(dev_sql, function(err, device) {
		      if (err) {
		        return res.status(500).send(err);
		      }
		      
		      	res.json({
					parameter : req.body.parameter
				});
		      
		     });
		});
		

/*	}
	else
	{
		res.status(401).send({});
	}*/
};

exports.create = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	/*  
	var parameter = {};
	parameter['mongo_id'] = null;
	parameter['device_name'] = req.body.parameter.device_name;
	parameter['event_enable'] = req.body.parameter.event_enable;
	parameter['dur0'] = req.body.parameter.dur0;
	parameter['dur1'] = req.body.parameter.dur1;
	parameter['dur10'] = req.body.parameter.dur10;
	parameter['dur11'] = req.body.parameter.dur11;
	parameter['dur12'] = req.body.parameter.dur12;
	parameter['dur13'] = req.body.parameter.dur13;
	parameter['dur14'] = req.body.parameter.dur14;
	parameter['dur15'] = req.body.parameter.dur15;
	parameter['dur16'] = req.body.parameter.dur16;
	parameter['dur17'] = req.body.parameter.dur17;
	parameter['dur18'] = req.body.parameter.dur18;
	parameter['dur19'] = req.body.parameter.dur19;
	parameter['dur20'] = req.body.parameter.dur20;
	parameter['dur21'] = req.body.parameter.dur21;
	parameter['dur22'] = req.body.parameter.dur22;
	parameter['dur23'] = req.body.parameter.dur23;
	parameter['dur24'] = req.body.parameter.dur24;
	parameter['dur25'] = req.body.parameter.dur25;
	parameter['dur27'] = req.body.parameter.dur27;
	parameter['dur28'] = req.body.parameter.dur28;
	parameter['dur29'] = req.body.parameter.dur29;
	parameter['dur30'] = req.body.parameter.dur30;
	parameter['dur31'] = req.body.parameter.dur31;
	parameter['dur32'] = req.body.parameter.dur32;
	parameter['dur2'] = req.body.parameter.dur2;
	parameter['dur26'] = req.body.parameter.dur26;
	parameter['dur3'] = req.body.parameter.dur3;
	parameter['dur4'] = req.body.parameter.dur4;
	parameter['dur5'] = req.body.parameter.dur5;
	parameter['dur6'] = req.body.parameter.dur6;
	parameter['dur7'] = req.body.parameter.dur7;
	parameter['dur8'] = req.body.parameter.dur8;
	parameter['dur9'] = req.body.parameter.dur9;
	parameter['max0'] = req.body.parameter.max0;
	parameter['max1'] = req.body.parameter.max1;
	parameter['max10'] = req.body.parameter.max10;
	parameter['max11'] = req.body.parameter.max11;
	parameter['max12'] = req.body.parameter.max12;
	parameter['max13'] = req.body.parameter.max13;
	parameter['max14'] = req.body.parameter.max14;
	parameter['max15'] = req.body.parameter.max15;
	parameter['max16'] = req.body.parameter.max16;
	parameter['max17'] = req.body.parameter.max17;
	parameter['max18'] = req.body.parameter.max18;
	parameter['max19'] = req.body.parameter.max19;
	parameter['max20'] = req.body.parameter.max20;
	parameter['max21'] = req.body.parameter.max21;
	parameter['max22'] = req.body.parameter.max22;
	parameter['max23'] = req.body.parameter.max23;
	parameter['max24'] = req.body.parameter.max24;
	parameter['max25'] = req.body.parameter.max25;
	parameter['max27'] = req.body.parameter.max27;
	parameter['max28'] = req.body.parameter.max28;
	parameter['max29'] = req.body.parameter.max29;
	parameter['max30'] = req.body.parameter.max30;
	parameter['max31'] = req.body.parameter.max31;
	parameter['max33'] = req.body.parameter.max33;
	parameter['max2'] = req.body.parameter.max2;
	parameter['max26'] = req.body.parameter.max26;
	parameter['max3'] = req.body.parameter.max3;
	parameter['max4'] = req.body.parameter.max4;
	parameter['max5'] = req.body.parameter.max5;
	parameter['max6'] = req.body.parameter.max6;
	parameter['max7'] = req.body.parameter.max7;
	parameter['max8'] = req.body.parameter.max8;
	parameter['max9'] = req.body.parameter.max9;
	parameter['min0'] = req.body.parameter.min0;
	parameter['min1'] = req.body.parameter.min1;
	parameter['min10'] = req.body.parameter.min10;
	parameter['min11'] = req.body.parameter.min11;
	parameter['min12'] = req.body.parameter.min12;
	parameter['min13'] = req.body.parameter.min13;
	parameter['min14'] = req.body.parameter.min14;
	parameter['min15'] = req.body.parameter.min15;
	parameter['min16'] = req.body.parameter.min16;
	parameter['min17'] = req.body.parameter.min17;
	parameter['min18'] = req.body.parameter.min18;
	parameter['min19'] = req.body.parameter.min19;
	parameter['min20'] = req.body.parameter.min20;
	parameter['min21'] = req.body.parameter.min21;
	parameter['min22'] = req.body.parameter.min22;
	parameter['min23'] = req.body.parameter.min23;
	parameter['min24'] = req.body.parameter.min24;
	parameter['min25'] = req.body.parameter.min25;
	parameter['min27'] = req.body.parameter.min27;
	parameter['min28'] = req.body.parameter.min28;
	parameter['min29'] = req.body.parameter.min29;
	parameter['min30'] = req.body.parameter.min30;
	parameter['min31'] = req.body.parameter.min31;
	parameter['min2'] = req.body.parameter.min2;
	parameter['min26'] = req.body.parameter.min26;
	parameter['min3'] = req.body.parameter.min3;
	parameter['min4'] = req.body.parameter.min4;
	parameter['min5'] = req.body.parameter.min5;
	parameter['min6'] = req.body.parameter.min6;
	parameter['min7'] = req.body.parameter.min7;
	parameter['min8'] = req.body.parameter.min8;
	parameter['min9'] = req.body.parameter.min9;
	parameter['p10_x'] = req.body.parameter.p10_x;
	parameter['p10_y'] = req.body.parameter.p10_y;
	parameter['p11_x'] = req.body.parameter.p11_x;
	parameter['p11_y'] = req.body.parameter.p11_y;
	parameter['p12_x'] = req.body.parameter.p12_x;
	parameter['p12_y'] = req.body.parameter.p12_y;
	parameter['p13_x'] = req.body.parameter.p13_x;
	parameter['p13_y'] = req.body.parameter.p13_y;
	parameter['p14_x'] = req.body.parameter.p14_x;
	parameter['p14_y'] = req.body.parameter.p14_y;
	parameter['p15_x'] = req.body.parameter.p15_x;
	parameter['p15_y'] = req.body.parameter.p15_y;
	parameter['p16_x'] = req.body.parameter.p16_x;
	parameter['p16_y'] = req.body.parameter.p16_y;
	parameter['p17_x'] = req.body.parameter.p17_x;
	parameter['p17_y'] = req.body.parameter.p17_y;
	parameter['p18_x'] = req.body.parameter.p18_x;
	parameter['p18_y'] = req.body.parameter.p18_y;
	parameter['p19_x'] = req.body.parameter.p19_x;
	parameter['p19_y'] = req.body.parameter.p19_y;
	parameter['p1_x'] = req.body.parameter.p1_x;
	parameter['p1_y'] = req.body.parameter.p1_y;
	parameter['p20_x'] = req.body.parameter.p20_x;
	parameter['p20_y'] = req.body.parameter.p20_y;
	parameter['p2_x'] = req.body.parameter.p2_x;
	parameter['p2_y'] = req.body.parameter.p2_y;
	parameter['p3_x'] = req.body.parameter.p3_x;
	parameter['p3_y'] = req.body.parameter.p3_y;
	parameter['p4_x'] = req.body.parameter.p4_x;
	parameter['p4_y'] = req.body.parameter.p4_y;
	parameter['p5_x'] = req.body.parameter.p5_x;
	parameter['p5_y'] = req.body.parameter.p5_y;
	parameter['p6_x'] = req.body.parameter.p6_x;
	parameter['p6_y'] = req.body.parameter.p6_y;
	parameter['p7_x'] = req.body.parameter.p7_x;
	parameter['p7_y'] = req.body.parameter.p7_y;
	parameter['p8_x'] = req.body.parameter.p8_x;
	parameter['p8_y'] = req.body.parameter.p8_y;
	parameter['p9_x'] = req.body.parameter.p9_x;
	parameter['p9_y'] = req.body.parameter.p9_y;
	parameter['q10_x'] = req.body.parameter.q10_x;
	parameter['q10_y'] = req.body.parameter.q10_y;
	parameter['q1_x'] = req.body.parameter.q1_x;
	parameter['q1_y'] = req.body.parameter.q1_y;
	parameter['q2_x'] = req.body.parameter.q2_x;
	parameter['q2_y'] = req.body.parameter.q2_y;
	parameter['q3_x'] = req.body.parameter.q3_x;
	parameter['q3_y'] = req.body.parameter.q3_y;
	parameter['q4_x'] = req.body.parameter.q4_x;
	parameter['q4_y'] = req.body.parameter.q4_y;
	parameter['q5_x'] = req.body.parameter.q5_x;
	parameter['q5_y'] = req.body.parameter.q5_y;
	parameter['q6_x'] = req.body.parameter.q6_x;
	parameter['q6_y'] = req.body.parameter.q6_y;
	parameter['q7_x'] = req.body.parameter.q7_x;
	parameter['q7_y'] = req.body.parameter.q7_y;
	parameter['q8_x'] = req.body.parameter.q8_x;
	parameter['q8_y'] = req.body.parameter.q8_y;
	parameter['q9_x'] = req.body.parameter.q9_x;
	parameter['q9_y'] = req.body.parameter.q9_y;
	parameter['r10_x'] = req.body.parameter.r10_x;
	parameter['r10_y'] = req.body.parameter.r10_y;
	parameter['r1_x'] = req.body.parameter.r1_x;
	parameter['r1_y'] = req.body.parameter.r1_y;
	parameter['r2_x'] = req.body.parameter.r2_x;
	parameter['r2_y'] = req.body.parameter.r2_y;
	parameter['r3_x'] = req.body.parameter.r3_x;
	parameter['r3_y'] = req.body.parameter.r3_y;
	parameter['r4_x'] = req.body.parameter.r4_x;
	parameter['r4_y'] = req.body.parameter.r4_y;
	parameter['r5_x'] = req.body.parameter.r5_x;
	parameter['r5_y'] = req.body.parameter.r5_y;
	parameter['r6_x'] = req.body.parameter.r6_x;
	parameter['r6_y'] = req.body.parameter.r6_y;
	parameter['r7_x'] = req.body.parameter.r7_x;
	parameter['r7_y'] = req.body.parameter.r7_y;
	parameter['r8_x'] = req.body.parameter.r8_x;
	parameter['r8_y'] = req.body.parameter.r8_y;
	parameter['r9_x'] = req.body.parameter.r9_x;
	parameter['r9_y'] = req.body.parameter.r9_y;
	parameter['send_int'] = req.body.parameter.send_int;

  	var sql = format('SELECT insert_parameter(\''+JSON.stringify(parameter)+'\')');

 	 pgClient.query(sql, function(err, parameters) {
      if (err) {
        return res.status(500).send(err);
      }
      var sql = format('SELECT parameter_lists(null, %L);',
			parameters.rows[0].insert_parameter);

		pgClient.query(sql, function(err, parameters) {
          if (err) {
            return res.status(500).send(err);
          }
          
          if(parameters && parameters.rows && parameters.rows.length > 0){
          	res.json(parameters.rows[0].parameter_lists);
          }
          else
          {
          	res.status(404).json({});
          }
        });
     
    });
   	/*}
	else
	{
		res.status(401).send({});
	}*/
	res.json({});
};

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * Delete an existing Parameter searched by id
 */
exports.destroy = function(req, res) {
	//if(req.headers && req.headers['origin'] && req.headers['origin'].split(".")[0] == config.constants.url_user || req.headers['origin'].split(".")[0] == config.constants.url_admin){
	var sql = format('SELECT delete_parameter(%L)',
		req.params.id);

	pgClient.query(sql, function(err, parameters) {
      if (err) {
        return res.status(500).send(err);
      }
      
      if(parameters && parameters.rows && parameters.rows.length > 0){
      	res.json({'parameter':parameters.rows[0].delete_parameter});
      }
      else
      {
      	res.status(404).json({});
      }
    });
	/*}
	else
	{
		res.status(401).send({});
	}*/
};

exports.default = function(req, res) {
	var async = require('async');
	var searchObj = {};

	var devices = [];
	var i = 0;

	if(req.query.device !== undefined && req.query.device !== ''){
		devices = req.query.device.split(',');
		devices.forEach(function(device_id){

			async.parallel({
				part1: function(callback){
					var sql = format('SELECT update_parameter_set_default_part_1(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
						device_id,
						32895,
						5,
						5,
						60,
						5,
						300,
						900,
						60,
						2,
						2,
						300,
						1,
						300,
						120,
						300,
						900,
						300,
						300,
						1,
						1,
						2,
						55,
						999,
						80,
						9999,
						7.99,
						1,
						9,
						1,
						10,
						999,
						30,
						125,
						1,
						0,
						0,
						0.5,
						-55,
						93,
						50,
						1500,
						0.5,
						0,
						1,
						1,
						0,
						5,
						2,
						50,
						-40,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null);


					     pgClient.query(sql, function(err, parameters) {
					      if (err) {
					        callback(true,null);
					      }

					      callback(null, 1);
					   	});
				},
				part2: function(callback){
					var sql = format('SELECT update_parameter_set_default_part_2(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
						device_id,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null,
						null
						);


					     pgClient.query(sql, function(err, parameters) {
					      if (err) {
					        callback(true,null);
					      }

					      callback(null, 1);
					   	});
				},
				part3: function(callback){
					var sql = format('SELECT update_parameter_set_default_part_3(%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L,%L);',
						device_id,
						20,
						300,
						600,
						30,
						60,
						30,
						30,
						180,
						600,
						1,
						-1,
						-1,
						-1,
						-1,
						-1,
						-1,
						9999,
						9999,
						9999,
						20,
						50,
						200,
						0,
						0,
						0,
						7.99,
						null,
						null,
						null,
						null,
						null,
						-1,
						30,
						100,
						500,
						0,
						0,
						0,
						0,
						0,
						0,
						1.4,
						null,
						null,
						null,
						null,
						null,
						2);


					     pgClient.query(sql, function(err, parameters) {
					      if (err) {
					        callback(true,null);
					      }

					      callback(null, 1);
					   	});
				}
			}, function(err, results){
				if(err){

				}

				var dev_sql = format('UPDATE devices SET send_parameter = %L, mail0 = (SELECT mail from devices where id = %L),mail1 =(SELECT mail from devices where id = %L),mail2 =(SELECT mail from devices where id = %L),mail3 =(SELECT mail from devices where id = %L),mail4 =(SELECT mail from devices where id = %L),mail5 =(SELECT mail from devices where id = %L),mail6 =(SELECT mail from devices where id = %L),mail7 =(SELECT mail from devices where id = %L),mail8 =(SELECT mail from devices where id = %L),mail9 =(SELECT mail from devices where id = %L),mail10 =(SELECT mail from devices where id = %L),mail11 =(SELECT mail from devices where id = %L),mail12 =(SELECT mail from devices where id = %L),mail13 =(SELECT mail from devices where id = %L),mail14 =(SELECT mail from devices where id = %L),mail15 =(SELECT mail from devices where id = %L),mail16 =(SELECT mail from devices where id = %L),mail17 =(SELECT mail from devices where id = %L),mail18 =(SELECT mail from devices where id = %L),mail19 =(SELECT mail from devices where id = %L),mail20 =(SELECT mail from devices where id = %L),mail21 =(SELECT mail from devices where id = %L),mail22 =(SELECT mail from devices where id = %L),mail23 =(SELECT mail from devices where id = %L),mail24 =(SELECT mail from devices where id = %L),mail25 =(SELECT mail from devices where id = %L), mail26=(SELECT mail from devices where id = %L) WHERE id = %L',
		      		true,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		device_id,
		      		req.params.id);

		      	pgClient.query(dev_sql, function(err, device) {
			      if (err) {
			        return res.status(500).send(err);
			      }
			     
			      	res.json({
						parameter : {}
					});
			     
			     });
		     });
			});
				}

};

function set_limits_parameter(param){

	// Event 0
	if(parseFloat(param.max0) > 1){
		param.max0 = 1;
	}
	else if(parseFloat(param.max0) < 0){
		param.max0 = 0;
	}
	if(parseFloat(param.min0) > 1){
		param.min0 = 1;
	}
	else if(parseFloat(param.min0) < 0){
		param.min0 = 0;
	}

	// Event 1
	if(parseFloat(param.max1) > 1){
		param.max1 = 1;
	}
	else if(parseFloat(param.max1) < 0){
		param.max1 = 0;
	}
	if(parseFloat(param.min1) > 1){
		param.min1 = 1;
	}
	else if(parseFloat(param.min1)< 0){
		param.min1 = 0;
	}

	// Event 2
	if(parseFloat(param.max2) > 1){
		param.max2 = 1;
	}
	else if(parseFloat(param.max2) < 0){
		param.max2 = 0;
	}
	if(parseFloat(param.min2) > 1){
		param.min2 = 1;
	}
	else if(parseFloat(param.min2)< 0){
		param.min2 = 0;
	}

	// Event 3
	if(parseFloat(param.max3) > 1){
		param.max3 = 1;
	}
	else if(parseFloat(param.max3) < 0){
		param.max3 = 0;
	}
	if(parseFloat(param.min3) > 1){
		param.min3 = 1;
	}
	else if(parseFloat(param.min3)< 0){
		param.min3 = 0;
	}

	// Event 4
	if(parseFloat(param.max4) > 999){
		param.max4 = 999;
	}
	else if(parseFloat(param.max4) < 0){
		param.max4 = 0;
	}
	if(parseFloat(param.min4) > 999){
		param.min4 = 999;
	}
	else if(parseFloat(param.min4)< 0){
		param.min4 = 0;
	}

	// Event 5
	if(parseFloat(param.max5) > 999){
		param.max5 = 999;
	}
	else if(parseFloat(param.max5) < 0){
		param.max5 = 0;
	}
	if(parseFloat(param.min5) > 999){
		param.min5 = 999;
	}
	else if(parseFloat(param.min5)< 0){
		param.min5 = 0;
	}

	// Event 7
	if(parseFloat(param.max7) > 999){
		param.max7 = 999;
	}
	else if(parseFloat(param.max7) < 0){
		param.max7 = 0;
	}
	if(parseFloat(param.min7) > 999){
		param.min7 = 999;
	}
	else if(parseFloat(param.min7)< 0){
		param.min7 = 0;
	}

	// Event 8
	if(parseFloat(param.max8) > 125){
		param.max8 = 125;
	}
	else if(parseFloat(param.max8) < -40){
		param.max8 = -40;
	}
	if(parseFloat(param.min8) > 125){
		param.min8 = 125;
	}
	else if(parseFloat(param.min8)< -40){
		param.min8 = -40;
	}

	// Event 9
	if(parseFloat(param.max9) > 125){
		param.max9 = 125;
	}
	else if(parseFloat(param.max9) < -40){
		param.max9 = -40;
	}
	if(parseFloat(param.min9) > 125){
		param.min9 = 125;
	}
	else if(parseFloat(param.min9)< -40){
		param.min9 = -40;
	}

	// Event 10
	if(parseFloat(param.max10) > 7.99){
		param.max10 = 7.99;
	}
	else if(parseFloat(param.max10) < 0){
		param.max10 = 0;
	}
	if(parseFloat(param.min10) > 7.99){
		param.min10 = 7.99;
	}
	else if(parseFloat(param.min10)< 0){
		param.min10 = 0;
	}

	// Event 11
	if(parseFloat(param.max11) > 90){
		param.max11 = 90;
	}
	else if(parseFloat(param.max11) < 0){
		param.max11 = 0;
	}
	if(parseFloat(param.min11) > 90){
		param.min11 = 90;
	}
	else if(parseFloat(param.min11)< 0){
		param.min11 = 0;
	}

	// Event 12
	if(parseFloat(param.max12) > 999){
		param.max12 = 999;
	}
	else if(parseFloat(param.max12) < 0){
		param.max12 = 0;
	}
	if(parseFloat(param.min12) > 999){
		param.min12 = 999;
	}
	else if(parseFloat(param.min12)< 0){
		param.min12 = 0;
	}

	// Event 13
	if(parseFloat(param.max13) > 999){
		param.max13 = 999;
	}
	else if(parseFloat(param.max13) < 0){
		param.max13 = 0;
	}
	if(parseFloat(param.min13) > 999){
		param.min13 = 999;
	}
	else if(parseFloat(param.min13)< 0){
		param.min13 = 0;
	}

	// Event 14
	if(parseFloat(param.max14) > 9999){
		param.max14 = 9999;
	}
	else if(parseFloat(param.max14) < 0){
		param.max14 = 0;
	}
	if(parseFloat(param.min14) > 9999){
		param.min14 = 9999;
	}
	else if(parseFloat(param.min14)< 0){
		param.min14 = 0;
	}

	// Event 15
	if(parseFloat(param.max15) > 7.99){
		param.max15 = 7.99;
	}
	else if(parseFloat(param.max15) < 0){
		param.max15 = 0;
	}
	if(parseFloat(param.min15) > 7.99){
		param.min15 = 7.99;
	}
	else if(parseFloat(param.min15)< 0){
		param.min15 = 0;
	}

	// Event 16
	if(parseFloat(param.max16) > 9999){
		param.max16 = 9999;
	}
	else if(parseFloat(param.max16) < 0){
		param.max16 = 0;
	}
	if(parseFloat(param.min16) > 9999){
		param.min16 = 9999;
	}
	else if(parseFloat(param.min16)< 0){
		param.min16 = 0;
	}

	// Event 17
	if(parseFloat(param.max17) > 9999){
		param.max17 = 9999;
	}
	else if(parseFloat(param.max17) < 0){
		param.max17 = 0;
	}
	if(parseFloat(param.min17) > 9999){
		param.min17 = 9999;
	}
	else if(parseFloat(param.min17)< 0){
		param.min17 = 0;
	}

	// Event 18
	if(parseFloat(param.max18) > 9999){
		param.max18 = 9999;
	}
	else if(parseFloat(param.max18) < 0){
		param.max18 = 0;
	}
	if(parseFloat(param.min18) > 9999){
		param.min18 = 9999;
	}
	else if(parseFloat(param.min18)< 0){
		param.min18 = 0;
	}

	// Event 19
	if(parseFloat(param.max19) > 9999){
		param.max19 = 9999;
	}
	else if(parseFloat(param.max19) < 0){
		param.max19 = 0;
	}
	if(parseFloat(param.min19) > 9999){
		param.min19 = 9999;
	}
	else if(parseFloat(param.min19)< 0){
		param.min19 = 0;
	}

	// Event 20
	if(parseFloat(param.max20) > 9999){
		param.max20 = 9999;
	}
	else if(parseFloat(param.max20) < 0){
		param.max20 = 0;
	}
	if(parseFloat(param.min20) > 9999){
		param.min20 = 9999;
	}
	else if(parseFloat(param.min20)< 0){
		param.min20 = 0;
	}

	// Event 21
	if(parseFloat(param.max21) > 9999){
		param.max21 = 9999;
	}
	else if(parseFloat(param.max21) < 0){
		param.max21 = 0;
	}
	if(parseFloat(param.min21) > 9999){
		param.min21 = 9999;
	}
	else if(parseFloat(param.min21)< 0){
		param.min21 = 0;
	}

	// Event 22
	if(parseFloat(param.max22) > 9999){
		param.max22 = 9999;
	}
	else if(parseFloat(param.max22) < 0){
		param.max22 = 0;
	}
	if(parseFloat(param.min22) > 9999){
		param.min22 = 9999;
	}
	else if(parseFloat(param.min22)< 0){
		param.min22 = 0;
	}

	// Event 23
	if(parseFloat(param.max23) > 9999){
		param.max23 = 9999;
	}
	else if(parseFloat(param.max23) < 0){
		param.max23 = 0;
	}
	if(parseFloat(param.min23) > 9999){
		param.min23 = 9999;
	}
	else if(parseFloat(param.min23)< 0){
		param.min23 = 0;
	}

	// Event 24
	if(parseFloat(param.max24) > 9999){
		param.max24 = 9999;
	}
	else if(parseFloat(param.max24) < 0){
		param.max24 = 0;
	}
	if(parseFloat(param.min24) > 9999){
		param.min24 = 9999;
	}
	else if(parseFloat(param.min24)< 0){
		param.min24 = 0;
	}
	// Event 25
	if(parseFloat(param.max25) > 7.99){
		param.max25 = 7.99;
	}
	else if(parseFloat(param.max25) < 0){
		param.max25 = 0;
	}
	if(parseFloat(param.min25) > 7.99){
		param.min25 = 7.99;
	}
	else if(parseFloat(param.min25)< 0){
		param.min25 = 0;
	}

	return param;
}; 