var moment = require('moment'), mongoose = require('mongoose'), Event = mongoose.model('Event');

/**
 * 
 * @param {Object} req
 * @param {Object} res
 * 
 * New event call
 */
exports.index = function(req, res) {
	
};

exports.exportcsv = function(req, res) {
	var json2csv = require('nice-json2csv');
	var searchObj = {};

	var _filename = 'status';

	if(req.query.file !== undefined && req.query.file !== ''){
		_filename = req.query.file;
	}
	
	if(req.query.device !== undefined && req.query.device !== ''){
		searchObj['device_id'] = req.query.device;
	}

	if (req.query.from_date !== undefined && req.query.from_date !== '') {
		if (req.query.to_date !== undefined && req.query.to_date !== '') {
			searchObj['$and'] = [{
				'created_at' : {
					'$gte' : req.query.from_date
				}
			}, {
			   'created_at' : {
					'$lte' : req.query.to_date,
				}
			}];
		} else {
			searchObj['created_at'] = {
				'$gte' : req.query.from_date
			};
		}
	} else if (req.query.to_date !== undefined && req.query.to_date !== '') {
		searchObj['created_at'] = {
			'$lte' : req.query.to_date
		};
	}

	Event.find(searchObj, function(err, events) {
		if (err) {
			return res.status(500).send(err);
		}
		if(events.length == 0)
		{
			return res.status(404).send({});
		}

		var csv = json2csv.convert(events, ['_id','created_at','device_name','codevent','timetx','timev','nrip','numdata','timedata','stringdata']);
		res.writeHead(200, {
			'Content-Type' : 'text/csv',
			'Content-Disposition' : 'attachment; filename='+_filename+'.csv'
		});
		res.end(csv);
	});

}; 
