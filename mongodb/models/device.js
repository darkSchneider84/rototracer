var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
var mongoosepaginate = require('mongoose-paginate');
var DeviceSchema = new Schema({

	device_beebeeboard_id: Number,
	created_at : Date,
	updated_at : Date,
	device_code: String,
	apn:String,
	piano_tariffario:String,
	sim: String,
	device_name: String,
	test_accensione: Boolean,
	lotto_produzione: String,
	status: String,
	user_id : Schema.Types.ObjectId,
	user_beebeeboard_id: Number,
	mail: String,
	send_parameter: Boolean,
	send_mail_periodic: Boolean,
	mail0: String,
	mail1: String,
	mail2: String,
	mail3: String,
	mail4: String,
	mail5: String,
	mail6: String,
	mail7: String,
	mail8: String,
	mail9: String,
	mail10: String,
	mail11: String,
	mail12: String,
	mail13: String,
	mail14: String,
	mail15: String,
	mail16: String,
	mail17: String,
	mail18: String,
	mail19: String,
	mail20: String,
	mail21: String,
	mail22: String,
	mail23: String,
	mail24: String,
	mail25: String,
	mail26: String

}).set('toJSON', {
	transform : function(doc, ret, options) {
		return ret;
	}
});
DeviceSchema.plugin(save_at);
DeviceSchema.plugin(mongoosepaginate);
mongoose.model('Device', DeviceSchema);
DeviceSchema.set('collection', 'devices');
