var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
var mongoosepaginate = require('mongoose-paginate');

var UserSchema = new Schema({

	user_beebeeboard_id: Number,
	organization: String,	
	created_at : Date,
	updated_at : Date,
	username : String,
	password : String,
	name : String,
	mail : String

}).set('toJSON', {
	transform : function(doc, ret, options) {
		
		return ret;
	}
});
UserSchema.plugin(save_at);
UserSchema.plugin(mongoosepaginate);
mongoose.model('User', UserSchema);
