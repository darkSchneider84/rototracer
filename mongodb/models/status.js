var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
var mongoosepaginate = require('mongoose-paginate');
var StatusSchema = new Schema({

	created_at : Date,
	updated_at : Date,
	timetx : Date,
	device_id : Schema.Types.ObjectId,
	device_name: String,
	firmware_version: String,
	send_int: Number,
	apn: String,
	ip_address: String,
	nrip: Number,
	timev: Date,
	timefix : Date,
	gpsx: Number,
	ns: String,
	ew:String,
	gpsy: Number,
	gpsz: Number,
	gpsd: Number,
	gpsst: Number,
	gpsstime: Date,
	gpssat: Number,
	pdop: Number,
	hdop: Number,
	vdop: Number,
	gpsvel: Number,
	gsmlvl: Number,
	gsmst: Number,
	gsmstime: Date,
	gsmopn: String,
	gsmmod: Number,
	accxist: Number,
	accyist: Number,
	acczist: Number,
	accxsqm: Number,	
	accysqm: Number,	
	acczsqm: Number,
	accxflt: Number,	
	accyflt: Number,	
	acczflt: Number,
	accxsqmflt: Number,	
	accysqmflt: Number,	
	acczsqmflt: Number,		
	temp: Number,
	keyst: Number,
	keytime: Date,
	pwrst: Number,
	pwrstime: Date,
	almst: Number,
	almstime: Date,
	blkst: Number,
	blkstime: Date,
	crc16:Number,
	event_id: [{type: Schema.Types.ObjectId, ref: 'Event' }]
}).set('toJSON', {
	transform : function(doc, ret, options) {
		return ret;
	}
});
StatusSchema.plugin(save_at);
StatusSchema.plugin(mongoosepaginate);
mongoose.model('Status', StatusSchema);
StatusSchema.set('collection', 'status');