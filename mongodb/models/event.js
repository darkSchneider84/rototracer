var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
var mongoosepaginate = require('mongoose-paginate');
var EventSchema = new Schema({

	created_at : Date,
	updated_at : Date,

	codevent : Number,
	device_id: Schema.Types.ObjectId,
	device_name: String,
	device_code: String,
	sim: String,
	timetx: Date,
	timev: Date,
	nrip: Number,
	mail: String,
	mail_sended: Boolean,

	numdata: [{
		name: String,
		data: Number
	}],
	
	timedata: [{
		name: String,
		data: Date
	}],
	
	stringdata: [{
		name: String,
		data: String
	}],
	
	
}).set('toJSON', {
	transform : function(doc, ret, options) {
		return ret;
	}
});
EventSchema.plugin(save_at);
EventSchema.plugin(mongoosepaginate);
mongoose.model('Event', EventSchema);
EventSchema.set('collection', 'events');
