var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
var MandrillSchema = new Schema({

	_tenant_id : Schema.Types.ObjectId,
	created_at : Date,
	updated_at : Date,
	device_name: String,
	status_id: Schema.Types.ObjectId,
	device_id: Schema.Types.ObjectId,
	email : String,
	status : String,
	reject_reason : String,
	_id : String

}).set('toJSON', {
	transform : function(doc, ret, options) {
		delete ret._tenant_id;
		return ret;
	}
});
MandrillSchema.plugin(save_at);
mongoose.model('Mandrill', MandrillSchema);