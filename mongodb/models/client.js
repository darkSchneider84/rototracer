var mongoose = require('mongoose'), Schema = mongoose.Schema;
var save_at = require('../save_at');
/**
 * 
 * Client Model
 * 
 * for the token authorization oauth2
 * 
 */
var ClientSchema = new Schema({

	_tenant_id : Schema.Types.ObjectId,
	created_at : Date,
	updated_at : Date,
	name : String,
	clientId : String,
	clientSecret : String

}).set('toJSON', {
	transform : function(doc, ret, options) {
		delete ret._tenant_id;
		return ret;
	}
});
ClientSchema.plugin(save_at);
mongoose.model('Client', ClientSchema);
