var mongoose = require('mongoose'), Schema = mongoose.Schema;

var TokenSchema = new Schema({

	"token" : String,
	"userID" : Schema.Types.ObjectId,
	"expirationDate" : Date,
	"clientID" : Schema.Types.ObjectId,
	"scope" : String,
	"redirectURI" : String

});

mongoose.model('Token', TokenSchema);
