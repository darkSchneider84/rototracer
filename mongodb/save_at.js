var mongoose = require('mongoose'), Schema = mongoose.Schema;

module.exports = exports = function saveAtPlugin (schema, options) {
   schema.pre('save', function(next){

	var timezone = require('moment-timezone');
	if ( !this.created_at ) {
	    this.created_at = timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss');
	}
	next();
	  
  });


};