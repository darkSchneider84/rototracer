var express = require('express'), config = require('./config'),mongoose = require('mongoose'),path = require('path'), dbToConnect = process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || (config.db.dbUrl + config.db.dbName);
var mandrill = require('mandrill-api/mandrill'),  models_path = __dirname + '/mongodb/models', fs = require('fs');
// Bootstrap models

var https = require('https');
var walk = function(path) {
	fs.readdirSync(path).forEach(function(file) {
		var newPath = path + '/' + file;
		var stat = fs.statSync(newPath);
		if (stat.isFile()) {
			if (/(.*)\.(js$|coffee$)/.test(file)) {
				require(newPath);
			}
		} else if (stat.isDirectory()) {
			walk(newPath);
		}
	});
};
walk(models_path);

mongoose.connect(dbToConnect);
var db = mongoose.connection.on('error', function() {
	throw new Error('unable to connect to database at ' + process.env.MONGOHQ_URL);
});

var Event = mongoose.model('Event');
var Status = mongoose.model('Status');
var Mandrill = mongoose.model('Mandrill');
var Device = mongoose.model('Device');
var timezone = require('moment-timezone');
var moment = require('moment');
var gval = 4096;
moment.locale('it');

//ogni ora schedulo l'invio di una mail
setInterval(function() {
	
	console.log('Scheduler time');
	Device.find({}, function(err, devices){
		if(err)
			console.log(err);

		devices.forEach(function(device){
			Status.findOne({'device_id': device._id}, function(err, status){
				if(err){
					console.log(err);
				}
				else
				{
					
					if(status && status != undefined && moment(new Date(status.timetx)).add(2, 'hours').isAfter(new Date())
						&& moment(new Date(status.created_at)).add(2, 'hours').isAfter(new Date())){
						Event.findOne({'_id': status.event_id, 'codevent': {'$ne':9999}, 'mail_sended':false}, function(err, ev){
							if(err || !ev)
							{
								console.log(err);
							}
							else
							{
								var tipo_evento = ev.codevent == 9999 ? 'ACCENSIONE' : 'PERIODICO';
								google = convert_to_google_coord(status.gpsx, status.gpsy, status.ns, status.ew);
								var gpsx = google.split(',')[0];
								var gpsy = google.split(',')[1];
								var acc_ist = parseFloat((Math.sqrt((status.accxist^2) + (status.accyist^2) + (status.acczist^2)))/gval); 
								var accxmed = parseFloat(status.accxsqmflt/gval);
								var accymed = parseFloat(status.accysqmflt/gval);
								var acczmed = parseFloat(status.acczsqmflt/gval);
								var acc_basso = 90 - (Math.atan(status.acczflt/parseFloat(Math.sqrt((status.accxflt^2) + (status.accyflt^2)))));
								var acc_rot = status.accxflt > 0 ? (Math.atan(status.accyflt/status.accxflt)) : (180-Math.atan(status.accyflt/status.accxflt));
								
								var template_content = [{
									"name" : "SIM",
									"content" : device.sim
								}, {
									"name" : "ICCID",
									"content" : device.device_code
								}, {
									"name" : "DEVICE_NAME",
									"content" : status.device_name
								}, {
									"name" : "EVENTO",
									"content" : tipo_evento
								},{
									"name" : "GSMLVL",
									"content" : status.gsmlvl
								}, {
									"name" : "GSMLVL_PERC",
									"content" : parseInt(status.gsmlvl*100/31)
								}, {
									"name" : "GSMMOD",
									"content" : status.gsmmod
								}, {
									"name" : "GSMMOD_REG",
									"content" : status.gsmmod == 0 ? 'Non registrato' : 'Registrato'
								}, {
									"name" : "GSMOPN",
									"content" : status.gsmopn
								}, {
									"name" : "DATE",
									"content" : ev.timev ? moment(new Date(ev.timev)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "BLKST",
									"content" : status.blkst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "BLKSTIME",
									"content" :  status.blkstime ? moment(new Date(status.blkstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_BLKSTIME",
									"content" : moment(new Date(status.blkstime)).from(new Date(ev.timev))
								}, {
									"name" : "KEYST",
									"content" : status.keyst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "KEYSTIME",
									"content" :  status.keytime ? moment(new Date(status.keytime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_KEYSTIME",
									"content" : moment(new Date(status.keytime)).from(new Date(ev.timev))
								}, {
									"name" : "PWRST",
									"content" : status.pwrst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "PWRSTIME",
									"content" :  status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_PWRSTIME",
									"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
								}, {
									"name" : "ALMST",
									"content" : status.almst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "ALMSTIME",
									"content" : status.almstime ? moment(new Date(status.almstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_ALMSTIME",
									"content" : moment(new Date(status.almstime)).from(new Date(ev.timev))
								}, {
									"name" : "PWRST",
									"content" : status.pwrst == 0 ? 'OFF' : 'ON'
								},{
									"name" : "PWRSTIME",
									"content" : status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_PWRSTIME",
									"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
								}, {
									"name" : "POSIZIONE",
									"content" : "[" + gpsx + ", " + gpsy + "]"
								}, {
									"name" : "MAPSOURCE",
									"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
								}, {
									"name" : "MAPSOURCEGENERAL",
									"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=10&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
								}, {
									"name" : "MAPSOURCELINK",
									"content" : "https://www.google.com/maps?q="+google+"&ll="+google+"&z=15"
								}, {
									"name" : "MAPSOURCEGENERALLINK",
									"content" : "https://www.google.com/maps?q="+google+"&ll="+google+"&z=10"
								},{ 
									"name" : "GPSX",
									"content" : gpsx
								}, {
									"name" : "GPSY",
									"content" : gpsy
								}, { 
									"name" : "GPSHDOP",
									"content" : Math.round(status.hdop*100)/100
								}, { 
									"name" : "GPSZ",
									"content" : parseInt(status.gpsz)
								}, { 
									"name" : "GPSVDOP",
									"content" : Math.round(status.vdop*100)/100
								}, { 
									"name" : "GPSD",
									"content" : parseInt(status.gpsd)
								}, { 
									"name" : "GPSVEL",
									"content" : status.gpsvel
								}, { 
									"name" : "GPSSAT",
									"content" : status.gpssat
								}, { 
									"name" : "GPSST",
									"content" : status.gpsst
								}, { 
									"name" : "GPSST_DEF",
									"content" : status.gpsst == 1 ? 'No Fix' : (status.gpsst == 2 ? '2D' : '3D')
								}, { 
									"name" : "ACC_IST",
									"content" : Math.round(acc_ist*100)/100
								}, { 
									"name" : "ACCXFLT",
									"content" : Math.round(status.accxflt/gval*100)/100
								}, { 
									"name" : "ACCYFLT",
									"content" : Math.round(status.accyflt/gval*100)/100
								}, { 
									"name" : "ACCZFLT",
									"content" : Math.round(status.acczflt/gval*100)/100
								}, { 
									"name" : "ACCXMED",
									"content" : Math.round(accxmed*100)/100
								}, { 
									"name" : "ACCYMED",
									"content" : Math.round(accymed*100)/100
								}, { 
									"name" : "ACCZMED",
									"content" : Math.round(acczmed*100)/100
								}, { 
									"name" : "BASSO",
									"content" : Math.round(acc_basso*100)/100
								}, { 
									"name" : "ROTAZIONE",
									"content" : Math.round(acc_rot*100)/100
								}, { 
									"name" : "TEMP",
									"content" : parseInt(status.temp)-4
								}, { 
									"name" : "FIRMWARE",
									"content" : status.firmware_version ? status.firmware_version : 'Information not avalaible'
								},
								], message = {
									"global_merge_vars" : template_content,
									"subject" : 'Device: '+status.device_name+' - Evento Periodico [1 h] - Replay da App',
									"from_email" : "no-reply@rototracer.com",
									"from_name" : 'Rototracer App',
									"to" : [{
										"email" : ev.mail,
										"name" : ev.mail,
										"type" : "to"
									}],
									"headers" : {
										"Reply-To" : 'rototracer@gmail.com'
									}
								};
								
								var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

								mandrill_client.messages.sendTemplate({
									"template_name" : "rototracer-event-mail",
									"template_content" : template_content,
									"message" : message,
									"async" : true,
									"send_at" : false
								}, function(result) {
									
									result[0].device_name = device.device_name;
									result[0].status_id = status._id;
									result[0].device_id = device._id;
									Mandrill(result[0]).save(function(err, product) {
										if (err) {
											console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mail event -> Mandrill product' + err);
										}
									});

									if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
										console.log('Mail sended');
										Event.update({
											'_id': ev._id
										}, {
											'$set':{
												'mail_sended': true
											}
										}, function(err,e){
											if(err)
											{
												console.log(err);
											}
											else
											{
												console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + ' Event updated mail sended');
											}
										});

									}

								});
							}
						});
					}
					
				}
			}).sort({'created_at':-1}).limit(1);
		});	
	});
}, 3600000);

setInterval(function() {
	
	console.log('Scheduler Events');
	Device.find({}, function(err, devices){
		if(err)
			console.log(err);

		devices.forEach(function(device){
			Status.findOne({'device_id': device._id}, function(err, status){
				if(err){
					console.log(err);
				}
				else
				{
					
					if(status && status != undefined){
						Event.findOne({'_id': status.event_id, 'codevent': 9999, 'mail_sended':false}, function(err, ev){
							if(err)
							{
								console.log(err);
							}
							else if(ev && ev != undefined)
							{
								var tipo_evento = 'ACCENSIONE';
								
								var template_content = [{
									"name" : "SIM",
									"content" : device.sim
								}, {
									"name" : "ICCID",
									"content" : device.device_code
								}, {
									"name" : "DEVICE_NAME",
									"content" : status.device_name
								}, {
									"name" : "EVENTO",
									"content" : tipo_evento
								},{
									"name" : "GSMLVL",
									"content" : status.gsmlvl
								}, {
									"name" : "GSMLVL_PERC",
									"content" : parseInt(status.gsmlvl*100/31)
								}, {
									"name" : "GSMMOD",
									"content" : status.gsmmod
								}, {
									"name" : "GSMMOD_REG",
									"content" : status.gsmmod == 0 ? 'Non registrato' : 'Registrato'
								}, {
									"name" : "GSMOPN",
									"content" : status.gsmopn
								}, {
									"name" : "DATE",
									"content" : ev.timev ? moment(new Date(ev.timev)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "BLKST",
									"content" : status.blkst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "BLKSTIME",
									"content" :  status.blkstime ? moment(new Date(status.blkstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_BLKSTIME",
									"content" : moment(new Date(status.blkstime)).from(new Date(ev.timev))
								}, {
									"name" : "KEYST",
									"content" : status.keyst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "KEYSTIME",
									"content" :  status.keytime ? moment(new Date(status.keytime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_KEYSTIME",
									"content" : moment(new Date(status.keytime)).from(new Date(ev.timev))
								}, {
									"name" : "PWRST",
									"content" : status.pwrst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "PWRSTIME",
									"content" :  status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_PWRSTIME",
									"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
								}, {
									"name" : "ALMST",
									"content" : status.almst == 0 ? 'OFF' : 'ON'
								}, {
									"name" : "ALMSTIME",
									"content" : status.almstime ? moment(new Date(status.almstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_ALMSTIME",
									"content" : moment(new Date(status.almstime)).from(new Date(ev.timev))
								}, {
									"name" : "PWRST",
									"content" : status.pwrst == 0 ? 'OFF' : 'ON'
								},{
									"name" : "PWRSTIME",
									"content" : status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
								}, {
									"name" : "DIFF_PWRSTIME",
									"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
								}, { 
									"name" : "TEMP",
									"content" : parseInt(status.temp)-4
								}, { 
									"name" : "FIRMWARE",
									"content" : status.firmware_version ? status.firmware_version : 'Information not avalaible'
								},
								], message = {
									"global_merge_vars" : template_content,
									"subject" : 'Device: '+status.device_name+' - Evento '+ tipo_evento+' [1 h] - Replay da App',
									"from_email" : "no-reply@rototracer.com",
									"from_name" : 'Rototracer App',
									"to" : [{
										"email" : ev.mail,
										"name" : ev.mail,
										"type" : "to"
									}],
									"headers" : {
										"Reply-To" : 'rototracer@gmail.com'
									}
								};
								
								var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

								mandrill_client.messages.sendTemplate({
									"template_name" : "rototracer-event-on",
									"template_content" : template_content,
									"message" : message,
									"async" : true,
									"send_at" : false
								}, function(result) {
									
									result[0].device_name = device.device_name;
									result[0].status_id = status._id;
									result[0].device_id = device._id;
									Mandrill(result[0]).save(function(err, product) {
										if (err) {
											console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mail event -> Mandrill product' + err);
										}
									});

									if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
										console.log('Mail sended');
										Event.update({
											'_id': ev._id
										}, {
											'$set':{
												'mail_sended': true
											}
										}, function(err,e){
											if(err)
											{
												console.log(err);
											}
											else
											{
												console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + ' Event updated mail sended');
											}
										});

									}

								});
							}
						});
					}
					
				}
			}).sort({'created_at':-1}).limit(1);
		});	
	});
}, 20000);

function convert_to_google_coord(gpsx, gpsy, ns, ew){

	s = gpsx + ","+ns+","+gpsy+","+ew;
	//s = '4501.93701171875,N,738.6063842773438,E'
	g = s.split(",");

	if(parseInt(g[0])<10){
		g[0] = '000'+g[0];
	}
	else if(parseInt(g[0])<100){
		g[0] = '00'+g[0];
	}
	else if(parseInt(g[0]) <1000){
		g[0] = '0'+g[0];
	}

	gradi_lat = g[0].substr(0, 2);
	if(g[1] == 'N'){
		lat = parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60);
	}
	else
	{
		lat = -1 * (parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60));
	}

	if(parseInt(g[2])<10){
		g[2] = '0000'+g[2];
	}
	else if(parseInt(g[2])<100){
		g[2] = '000'+g[2];
	}
	else if(parseInt(g[2]) <1000){
		g[2] = '00'+g[2];
	}
	else
	{
		g[2] = '0'+g[2];
	}

	gradi_lon = g[2].substr(0, 3);
	if(g[3] == 'E'){
		lon = parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60);
	}
	else
	{
		lon = -1 * (parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60));
	}
	return lat.toFixed(8)+","+lon.toFixed(8);


};
