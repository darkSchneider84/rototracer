//
// The configuration options of the server
//

/**
 * Configuration of access tokens.
 *
 * expiresIn - The time in seconds before the access token expires
 * calculateExpirationDate - A simple function to calculate the absolute
 * time that th token is going to expire in.
 * authorizationCodeLength - The length of the authorization code
 * accessTokenLength - The length of the access token
 * refreshTokenLength - The length of the refresh token
 */
exports.token = {
	expiresIn : 43200,
	calculateExpirationDate : function() {
		return new Date(new Date().getTime() + (this.expiresIn * 1000));
	},
	authorizationCodeLength : 16,
	accessTokenLength : 256,
	refreshTokenLength : 256
};

/**
 * Database configuration for access and refresh tokens.
 *
 * timeToCheckExpiredTokens - The time in seconds to check the database
 * for expired access tokens.  For example, if it's set to 3600, then that's
 * one hour to check for expired access tokens.
 * type - The type of database to use.  "db" for "in-memory", or
 * "mongodb" for the mongo database store.
 * dbName - The database name to use.
 */
exports.db = {
	timeToCheckExpiredTokens : 3600,
	type : "mongodb",
	dbName : "rototracer_db",
	//dbUrl : "mongodb://127.0.0.1:27017/"
	dbUrl: "mongodb://roto_admin:506065wf45@dogen.mongohq.com:10082/"
};

/**
 * Session configuration
 *
 * type - The type of session to use.  MemoryStore for "in-memory",
 * or MongoStore for the mongo database store
 * maxAge - The maximum age in milliseconds of the session.  Use null for
 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
 * for a year.
 * secret - The session secret that you should change to what you want
 * dbName - The database name if you're using Mongo
 */
exports.session = {
	type : "MongoStore",
	maxAge : 3600000 * 24 * 7 * 52,
	secret : "Fj029ID9jwjwkdOI8hdsf",
	dbName : "Session"
};


/**
 * Mandrill API
 * test API 
 */
exports.mandrill = {
	api : process.env.MANDRILL_API || 'fYjU4daptGBYOE3D-CB5kw'   
}; 

/**
* Error message response
*/
exports.errors = {
	invalid_record: {
	    "meta": {
	        "success": false,
	        "statusCode": 422
	    },
	    "errorMessage": "Invalid record.",
	    "errorCode": "INVALID_RECORD",
	    "validationErrors": []
	}
};

exports.constants = {
	STX_OLD : 9182,
	STX_NEW : 9183,
	STX_BATT: 9186,
	STX: 9184,
	STX_LOG: 9185,
	STX_RES : 6574,
	EOT : 7364,
	url_user: 'https://user_rototracer',
	url_admin: 'https://rototracer'
};
