		     
module.exports = function(app, site, oauth2, token) {
	var mongoose = require('mongoose');
	/**
	 * Authorization Necessary for all models
	 */
	/*app.all('/api/*', passport.authenticate('bearer', {
		session : false
	}));*/

	/**
	 * Default Authentication
	 */
	/*
	//app.get('/', oauth2.logout);
	//app.get('/login', site.loginForm);
	//app.post('/login', site.login);
	app.get('/api/logout', oauth2.logout);
	//app.get('/account', site.account);

	app.get('/api/dialog/authorize', oauth2.authorization);
	app.post('/api/dialog/authorize/decision', oauth2.decision);
	app.post('/api/oauth/token', oauth2.token);

	app.get('/api/tokeninfo', token.info);
	*/
	
	/**
	 * Device
	 */
	var device = require('../mongodb/controllers/device');
	app.get('/api/devices', device.index);
	app.get('/api/devices_from_code/', device.find_from_iccid);
	app.get('/api/devices_admin', device.index_admin);
	app.get('/api/devices/:id', device.find);
	app.post('/api/devices', device.create);
	app.put('/api/devices/:id', device.update);
	app.put('/api/devices_from_bb/:id', device.update_from_bb);
	app.put('/api/devices_user_bb_id/:id', device.update_user_bb_id);
	app.put('/api/devices_bb_id/:id', device.update_bb_id);
	app.get('/api/devices_bb_id/:id', device.find_from_device_bb);
	app.
	delete ('/api/devices/:id', device.destroy);

	/**
	 * Parameter
	 */
	var parameter = require('../mongodb/controllers/parameter');
	app.get('/api/parameters', parameter.index);
	app.get('/api/parameters/:id', parameter.find);
	app.post('/api/parameters', parameter.create);
	app.put('/api/parameters/:id', parameter.update);
	app.
	delete ('/api/parameters/:id', parameter.destroy);
	app.get('/api/parameters_default', parameter.default);
	
	/**
	 * Event
	 */
	var event = require('../mongodb/controllers/event');
	//app.get('/api/events', event.index);
	app.get('/api/event/export', event.exportcsv);
	
	/**
	 * Status
	 */
	var status = require('../mongodb/controllers/status');
	//app.get('/api/status', status.index);
	app.get('/api/status/export', status.exportcsv);
	//app.get('/api/status/export_csv', status.exportnewcsv);
	app.get('/api/status/export_kml',  status.kml);
	app.get('/api/status/mail',  status.immediate_mail);

};



