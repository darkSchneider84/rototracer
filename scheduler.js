var express = require('express'), 
config = require('./config'),
pg = require('pg'),
format = require('pg-format'),
path = require('path');
var mandrill = require('mandrill-api/mandrill'),  
models_path = __dirname + '/mongodb/models', 
fs = require('fs');
// Bootstrap models

var https = require('https');
var walk = function(path) {
	fs.readdirSync(path).forEach(function(file) {
		var newPath = path + '/' + file;
		var stat = fs.statSync(newPath);
		if (stat.isFile()) {
			if (/(.*)\.(js$|coffee$)/.test(file)) {
				require(newPath);
			}
		} else if (stat.isDirectory()) {
			walk(newPath);
		}
	});
};
walk(models_path);

var conPgString = "postgres://rototracer:506065wf45@rototracerdb.cx0chub3aq5f.eu-central-1.rds.amazonaws.com:5432/rototracer_db";

var pgClient = new pg.Client(conPgString);
pgClient.connect();

var timezone = require('moment-timezone');
var moment = require('moment');
var gval = 4096;
moment.locale('it');
								

//ogni ora schedulo l'invio di una mail
setInterval(function() {
	
	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')+' Scheduler time');

	 var sql = format('SELECT * from devices',null);

     pgClient.query(sql, function(err, devices) {
      if (err) {
        console.log(err);
        
      }


		devices.rows.forEach(function(device){
			var stat_sql = format('SELECT * from statuses WHERE device_id = %L ORDER BY created_at DESC LIMIT 1',
		      device.id);

		     pgClient.query(stat_sql, function(err, statuses) {
		        if (err) {
		          console.log(err);
		       
		        }
				else
				{
					var status = statuses.rows[0];
					if(status && status != undefined && moment(new Date(status.timetx)).add(2, 'hours').isAfter(new Date())
						&& moment(new Date(status.created_at)).add(2, 'hours').isAfter(new Date()) && device.send_mail_periodic){
						var ev_sql = format('SELECT * from events where id = %L and codevent <> %L',
								status.event_id,
								9999
							);
						 pgClient.query(ev_sql, function(err, events) {
					        var ev = events.rows[0];
					        if (err || !ev) {
					          console.log(err);
					       
					        }
							else
							{

								var tipo_evento = ev.codevent == '9999' ? 'ACCENSIONE' : 'PERIODICO';
								google = convert_to_google_coord(status.gpsx, status.gpsy, status.ns, status.ew);
								var gpsx = google.split(',')[0];
								var gpsy = google.split(',')[1];
								var acc_ist = parseFloat((Math.sqrt((status.accxist*status.accxist) + (status.accyist*status.accyist) + (status.acczist*status.acczist)))/gval); 
								var accxmed = parseFloat(status.accxsqmflt/gval);
								var accymed = parseFloat(status.accysqmflt/gval);
								var acczmed = parseFloat(status.acczsqmflt/gval);
								var acc_basso = 90 - (parseFloat((Math.atan(parseFloat(status.acczist/parseFloat((Math.sqrt((status.accxist*status.accxist) + (status.accyist*status.accyist))))))))*180/3.14);
								var acc_rot = status.accyist > 0 ? (Math.atan(status.accyist/status.accxist)) : (status.accxist > 0 ? (180+Math.atan(status.accyist/status.accxist)) : ((Math.atan(status.accyist/status.accxist))-180));
								
								var to_mail = [];
								var mails = ev.mail.split(';');

								mails.forEach(function(m){
									
									var to = {};
									to.email = m;
									to.type = 'to';
									to.name=m;
									to_mail.push(to);
									
								});

								var param_sql = format('SELECT * from parameters where device_id = %L',
										device.id
									);

								 pgClient.query(param_sql, function(err, parameters) {
							        if (err) {
							          console.log(err);
							       
							        }

							        var parameter = parameters.rows[0];
									var dur_evento = find_duration_event(parameter, ev.codevent); 

									var template_content = [{
										"name" : "SIM",
										"content" : device.sim
									}, {
										"name" : "ICCID",
										"content" : device.device_code
									}, {
										"name" : "DEVICE_NAME",
										"content" : status.device_name
									}, {
										"name" : "EVENTO",
										"content" : tipo_evento
									},{
										"name" : "GSMLVL",
										"content" : status.gsmlvl
									}, {
										"name" : "GSMLVL_PERC",
										"content" : parseInt(status.gsmlvl*100/31)
									}, {
										"name" : "GSMMOD",
										"content" : status.gsmmod
									}, {
										"name" : "GSMMOD_REG",
										"content" : status.gsmmod == 0 ? 'Non registrato' : 'Registrato'
									}, {
										"name" : "GSMOPN",
										"content" : status.gsmopn
									}, {
										"name" : "DATE",
										"content" : ev.timev ? moment(new Date(ev.timev)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "BLKST",
										"content" : status.blkst == 0 ? 'OFF' : 'ON'
									}, {
										"name" : "BLKSTIME",
										"content" :  status.blkstime ? moment(new Date(status.blkstime)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "DIFF_BLKSTIME",
										"content" : moment(new Date(status.blkstime)).from(new Date(ev.timev))
									}, {
										"name" : "KEYST",
										"content" : status.keyst == 0 ? 'OFF' : 'ON'
									}, {
										"name" : "KEYSTIME",
										"content" :  status.keytime ? moment(new Date(status.keytime)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "DIFF_KEYSTIME",
										"content" : moment(new Date(status.keytime)).from(new Date(ev.timev))
									}, {
										"name" : "PWRST",
										"content" : status.pwrst == 0 ? 'OFF' : 'ON'
									}, {
										"name" : "PWRSTIME",
										"content" :  status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "DIFF_PWRSTIME",
										"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
									}, {
										"name" : "ALMST",
										"content" : status.almst == 0 ? 'OFF' : 'ON'
									}, {
										"name" : "ALMSTIME",
										"content" : status.almstime ? moment(new Date(status.almstime)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "DIFF_ALMSTIME",
										"content" : moment(new Date(status.almstime)).from(new Date(ev.timev))
									}, {
										"name" : "PWRST",
										"content" : status.pwrst == 0 ? 'OFF' : 'ON'
									},{
										"name" : "PWRSTIME",
										"content" : status.pwrstime ? moment(new Date(status.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
									}, {
										"name" : "DIFF_PWRSTIME",
										"content" : moment(new Date(status.pwrstime)).from(new Date(ev.timev))
									}, {
										"name" : "POSIZIONE",
										"content" : "[" + gpsx + ", " + gpsy + "]"
									}, {
										"name" : "MAPSOURCE",
										"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
									}, {
										"name" : "MAPSOURCEGENERAL",
										"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=10&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
									}, {
										"name" : "MAPSOURCELINK",
										"content" : "https://maps.google.com/maps?q="+google+"&ll="+google+"&z=15"
									}, {
										"name" : "MAPSOURCEGENERALLINK",
										"content" : "https://maps.google.com/maps?q="+google+"&ll="+google+"&z=10"
									},{ 
										"name" : "GPSX",
										"content" : gpsx
									}, {
										"name" : "GPSY",
										"content" : gpsy
									}, { 
										"name" : "GPSHDOP",
										"content" : Math.round(status.hdop*100)/100
									}, { 
										"name" : "GPSZ",
										"content" : parseInt(status.gpsz)
									}, { 
										"name" : "GPSVDOP",
										"content" : Math.round(status.vdop*100)/100
									}, { 
										"name" : "GPSD",
										"content" : parseInt(status.gpsd)
									}, { 
										"name" : "GPSVEL",
										"content" : status.gpsvel
									}, { 
										"name" : "GPSSAT",
										"content" : status.gpssat
									}, { 
										"name" : "GPSST",
										"content" : status.gpsst
									}, { 
										"name" : "GPSST_DEF",
										"content" : status.gpsst == 1 ? 'No Fix' : (status.gpsst == 2 ? '2D' : '3D')
									}, { 
										"name" : "ACC_IST",
										"content" : Math.round(acc_ist*100)/100
									}, { 
										"name" : "ACCXFLT",
										"content" : Math.round(status.accxflt/gval*100)/100
									}, { 
										"name" : "ACCYFLT",
										"content" : Math.round(status.accyflt/gval*100)/100
									}, { 
										"name" : "ACCZFLT",
										"content" : Math.round(status.acczflt/gval*100)/100
									}, { 
										"name" : "ACCXMED",
										"content" : Math.round(accxmed*100)/100
									}, { 
										"name" : "ACCYMED",
										"content" : Math.round(accymed*100)/100
									}, { 
										"name" : "ACCZMED",
										"content" : Math.round(acczmed*100)/100
									}, { 
										"name" : "BASSO",
										"content" :  acc_basso ? Math.round(acc_basso*100)/100 : 'Non significativo'
									}, { 
										"name" : "ROTAZIONE",
										"content" : Math.round(acc_rot*100)/100
									}, { 
										"name" : "TEMP",
										"content" : parseInt(status.temp)-4
									}, { 
										"name" : "FIRMWARE",
										"content" : (status.firmware_version ? status.firmware_version : 'Information not avalaible')
									}, { 
										"name" : "FWVC",
										"content" : (status.fwvc ? status.fwvc : 'Information not avalaible')
									},
									], message = {
										"global_merge_vars" : template_content,
										"subject" : status.device_name+' - '+ tipo_evento+' - '+(ev.timev ? moment(new Date(ev.timev)).format('DD/MM/YYYY HH:mm') : '-') + ' ' + dur_evento,
										"from_email" : "no-reply@move2web.it",
										"from_name" : 'Rototracer App',
										"to" : to_mail,
										"headers" : {
											"Reply-To" : 'rototracer@move2web.it'
										}
									};
									
									var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

									mandrill_client.messages.sendTemplate({
										"template_name" : "rototracer-event-mail",
										"template_content" : template_content,
										"message" : message,
										"async" : true,
										"send_at" : false
									}, function(result) {
										
										result[0].device_name = device.device_name;
										result[0].status_id = status.id;
										result[0].device_id = device.id;

										var mandr_sql = format('INSERT INTO mandrills (status_id,device_id,email, status,device_name) VALUES(%L,%L,%L,%L,%L)',
												result[0].status_id,
												result[0].device_id,
												to_mail,
												result[0].status,
												result[0].device_name
											);
										 pgClient.query(mandr_sql, function(err, product) {
									        if (err ) {
									         	console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mail event -> Mandrill product' + err);
											}
										  });

										if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
											console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')+' Mail sended');
											
											var upd_ev_sql = format('UPDATE events SET mail_sended = %L WHERE id = %L',
													true,
													ev.id
												);
											 pgClient.query(upd_ev_sql, function(err, upd_ev_sql) {
									        	if (err ) {
													console.log(err);
												}
												else
												{
													console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + ' Event updated mail sended');
												}
											});

										}

									});
								});

								
							}
						});
					}
					
				}
			});
		});	
	});
}, 3600000);

setInterval(function() {


	var codevents = ['9999','100','101','102','103','104','105','107','108','109','110','111','112','113','114','115','116','117','118','119','120','121','122','123','124','125','126'];
	console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')+' Scheduler Events');

var stat_sql = format('with last_status As(SELECT s.device_name as dev_name,s.*, ROW_NUMBER() OVER (PARTITION BY s.device_id order by s.created_at desc) as rk from statuses as s order by created_at Desc) select s.*, e.codevent, e.timev, e.mail_sended, d.sim, d.device_code, d.mail, d.mail0, d.mail1, d.mail2, d.mail3,d.mail4, d.mail5, d.mail6, d.mail7,d.mail8, d.mail9, d.mail10,d.mail11, d.mail12, d.mail13,d.mail14, d.mail15, d.mail16, d.mail17,d.mail18, d.mail19, d.mail20,d.mail21, d.mail22, d.mail23,d.mail24, d.mail25, d.mail26 from last_status as s left join events as e on e.id = s.event_id left join devices as d on d.id = e.device_id WHERE e.codevent = ANY(ARRAY[%L]::character varying[])and rk = 1 and e.mail_sended is false',
  codevents);

 pgClient.query(stat_sql, function(err, statuses) {
    if (err) {
      console.log(err);
   
    }
	else
	{

		status = statuses.rows[0];
		if(status && status != undefined ){
			
			if(status.event_id && !status.mail_sended && status.codevent)
			{
				
				var to_mail = [];
				var mails = find_mail_for_event(status, parseInt(status.codevent));
				if(mails){
					mails.forEach(function(m){
						
						var to = {};
						to.email = m;
						to.type = 'to';
						to.name = m;
						to_mail.push(to);
						
					});
					
					google = convert_to_google_coord(status.gpsx, status.gpsy, status.ns, status.ew);
					var gpsx = google.split(',')[0];
					var gpsy = google.split(',')[1];
					var tipo_evento = find_event_name(parseInt(status.codevent));
					var acc_ist = parseFloat((Math.sqrt((status.accxist*status.accxist) + (status.accyist*status.accyist) + (status.acczist*status.acczist)))/gval); 
					var accxmed = parseFloat(status.accxsqmflt/gval);
					var accymed = parseFloat(status.accysqmflt/gval);
					var acczmed = parseFloat(status.acczsqmflt/gval);
					var acc_basso = 90 - (parseFloat((Math.atan(parseFloat(status.acczist/parseFloat((Math.sqrt((status.accxist*status.accxist) + (status.accyist*status.accyist))))))))*180/3.14);
					var acc_rot = status.accyist > 0 ? (Math.atan(status.accyist/status.accxist)) : (status.accxist > 0 ? (180+Math.atan(status.accyist/status.accxist)) : ((Math.atan(status.accyist/status.accxist))-180));
								
					var device_name = status.dev_name; 
					var param_sql = format('SELECT * from parameters where device_id = %L',
							status.device_id
						);

					 var sta = status;
					 pgClient.query(param_sql, function(err, parameters) {
				        if (err) {
				          console.log(err);
				       
				        }

				        var parameter = parameters.rows[0];
						var dur_evento = find_duration_event(parameter, parseInt(sta.codevent)); 

						var template_content = [{
							"name" : "SIM",
							"content" : sta.sim
						}, {
							"name" : "ICCID",
							"content" : sta.device_code
						}, {
							"name" : "DEVICE_NAME",
							"content" : device_name
						}, {
							"name" : "EVENTO",
							"content" : tipo_evento
						},{
							"name" : "GSMLVL",
							"content" : sta.gsmlvl
						}, {
							"name" : "GSMLVL_PERC",
							"content" : parseInt(sta.gsmlvl*100/31)
						}, {
							"name" : "GSMMOD",
							"content" : sta.gsmmod
						}, {
							"name" : "GSMMOD_REG",
							"content" : sta.gsmmod == 0 ? 'Non registrato' : 'Registrato'
						}, {
							"name" : "GSMOPN",
							"content" : sta.gsmopn
						}, {
							"name" : "DATE",
							"content" : sta.timev ? moment(new Date(sta.timev)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "BLKST",
							"content" : sta.blkst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "BLKSTIME",
							"content" :  sta.blkstime ? moment(new Date(sta.blkstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_BLKSTIME",
							"content" : moment(new Date(sta.blkstime)).from(new Date(sta.timev))
						}, {
							"name" : "KEYST",
							"content" : sta.keyst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "KEYSTIME",
							"content" :  sta.keytime ? moment(new Date(sta.keytime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_KEYSTIME",
							"content" : moment(new Date(sta.keytime)).from(new Date(sta.timev))
						}, {
							"name" : "PWRST",
							"content" : sta.pwrst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "PWRSTIME",
							"content" :  sta.pwrstime ? moment(new Date(sta.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_PWRSTIME",
							"content" : moment(new Date(sta.pwrstime)).from(new Date(sta.timev))
						}, {
							"name" : "ALMST",
							"content" : sta.almst == 0 ? 'OFF' : 'ON'
						}, {
							"name" : "ALMSTIME",
							"content" : sta.almstime ? moment(new Date(sta.almstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_ALMSTIME",
							"content" : moment(new Date(sta.almstime)).from(new Date(sta.timev))
						}, {
							"name" : "PWRST",
							"content" : sta.pwrst == 0 ? 'OFF' : 'ON'
						},{
							"name" : "PWRSTIME",
							"content" : sta.pwrstime ? moment(new Date(sta.pwrstime)).format('DD/MM/YYYY HH:mm') : '-'
						}, {
							"name" : "DIFF_PWRSTIME",
							"content" : moment(new Date(sta.pwrstime)).from(new Date(sta.timev))
						}, {
							"name" : "POSIZIONE",
							"content" : "[" + gpsx + ", " + gpsy + "]"
						}, {
							"name" : "MAPSOURCE",
							"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
						}, {
							"name" : "MAPSOURCEGENERAL",
							"content" : "https://maps.googleapis.com/maps/api/staticmap?zoom=10&size=600x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C"+google+"&key=AIzaSyDVyEfQvDm-vlecTwE-vQCPk4gLAMTRfrE"
						}, {
							"name" : "MAPSOURCELINK",
							"content" : "https://maps.google.com/maps?q="+google+"&ll="+google+"&z=15"
						}, {
							"name" : "MAPSOURCEGENERALLINK",
							"content" : "https://maps.google.com/maps?q="+google+"&ll="+google+"&z=10"
						},{ 
							"name" : "GPSX",
							"content" : gpsx
						}, {
							"name" : "GPSY",
							"content" : gpsy
						}, { 
							"name" : "GPSHDOP",
							"content" : Math.round(sta.hdop*100)/100
						}, { 
							"name" : "GPSZ",
							"content" : parseInt(sta.gpsz)
						}, { 
							"name" : "GPSVDOP",
							"content" : Math.round(sta.vdop*100)/100
						}, { 
							"name" : "GPSD",
							"content" : parseInt(sta.gpsd)
						}, { 
							"name" : "GPSVEL",
							"content" : sta.gpsvel
						}, { 
							"name" : "GPSSAT",
							"content" : sta.gpssat
						}, { 
							"name" : "GPSST",
							"content" : sta.gpsst
						}, { 
							"name" : "GPSST_DEF",
							"content" : sta.gpsst == 1 ? 'No Fix' : (sta.gpsst == 2 ? '2D' : '3D')
						}, { 
							"name" : "ACC_IST",
							"content" : Math.round(acc_ist*100)/100
						}, { 
							"name" : "ACCXFLT",
							"content" : Math.round(sta.accxflt/gval*100)/100
						}, { 
							"name" : "ACCYFLT",
							"content" : Math.round(sta.accyflt/gval*100)/100
						}, { 
							"name" : "ACCZFLT",
							"content" : Math.round(sta.acczflt/gval*100)/100
						}, { 
							"name" : "ACCXMED",
							"content" : Math.round(accxmed*100)/100
						}, { 
							"name" : "ACCYMED",
							"content" : Math.round(accymed*100)/100
						}, { 
							"name" : "ACCZMED",
							"content" : Math.round(acczmed*100)/100
						}, { 
							"name" : "BASSO",
							"content" :  acc_basso ? Math.round(acc_basso*100)/100 : 'Non significativo'
						}, { 
							"name" : "ROTAZIONE",
							"content" : Math.round(acc_rot*100)/100
						}, { 
							"name" : "TEMP",
							"content" : parseInt(sta.temp)-4
						}, { 
							"name" : "FIRMWARE",
							"content" : (sta.firmware_version ? sta.firmware_version : 'Information not avalaible')
						}, { 
							"name" : "FWVC",
							"content" : (sta.fwvc ? sta.fwvc : 'Information not avalaible')
						},
						], message = {
							"global_merge_vars" : template_content,
							"subject" : device_name+' - '+ tipo_evento+' - '+(sta.timev ? moment(new Date(sta.timev)).format('DD/MM/YYYY HH:mm') : '-') +' ' + dur_evento,
							"from_email" : "no-reply@move2web.it",
							"from_name" : 'Rototracer App',
							"to" : to_mail,
							"headers" : {
								"Reply-To" : 'rototracer@move2web.it'
							}
						};
						
						var mandrill_client = new mandrill.Mandrill(config.mandrill.api);

						mandrill_client.messages.sendTemplate({
							"template_name" : "rototracer-event-mail",
							"template_content" : template_content,
							"message" : message,
							"async" : true,
							"send_at" : false
						}, function(result) {
							
							result[0].device_name = device_name;
							result[0].status_id = sta.id;
							result[0].device_id = sta.device_id;
							
							var mandr_sql = format('INSERT INTO mandrills (status_id, device_id, email, status, device_name) VALUES(%L, %L, %L, %L, %L)',
									result[0].status_id,
									result[0].device_id,
									to_mail,
									result[0].status.replace(',',''),
									result[0].device_name.replace(',','')
								);
							console.log(mandr_sql);
							 pgClient.query(mandr_sql, function(err, product) {
						        if (err ) { 
						         	console.log(moment(new Date()).format('YYYY/MM/DD HH:mm:ss') + ' Mail event -> Mandrill product ' + err);
								}
							  });

							if (result[0].status !== 'rejected' && result[0].status !== 'invalid' && result[0].status !== 'error') {
								console.log(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')+' Mail sended');
								var upd_ev_sql = format('UPDATE events SET mail_sended = %L WHERE id = %L',
										true,
										sta.event_id
									);
								 pgClient.query(upd_ev_sql, function(err, upd_ev_sql) {
						        	if (err ) {
										console.log(err);
									}
									else
									{
										console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + ' Event updated mail sended');
									}
								});

							}

						});
					});
				}else
				{
					var upd_ev_sql = format('UPDATE events SET mail_sended = %L WHERE id = %L',
							true,
							status.event_id
						);
					 pgClient.query(upd_ev_sql, function(err, upd_ev_sql) {
			        	if (err ) {
							console.log(err);
						}
						else
						{
							console.log(timezone(new Date()).tz('Europe/Rome').format('YYYY/MM/DD HH:mm:ss') + ' Event updated mail not filled');
						}
					});
				}
			}
		}
		
	}
 });
		
}, 35000);

function find_mail_for_event(dev,code){
	var m = [];
	switch(code){
		case 9999:
			m = dev.mail ? dev.mail.split(';') : null;
		break;
		case 100:
			m = dev.mail0 ? dev.mail0.split(';') : null;
		break;
		case 101:
			m = dev.mail1 ? dev.mail1.split(';') : null;
		break;
		case 102:
			m = dev.mail2 ? dev.mail2.split(';') : null;
		break;
		case 103:
			m = dev.mail3 ? dev.mail3.split(';') : null;
		break;
		case 104:
			m = dev.mail4 ? dev.mail4.split(';') : null;
		break;
		case 105:
			m = dev.mail5 ? dev.mail5.split(';') : null;
		break;
		case 107:
			m = dev.mail7 ? dev.mail7.split(';') : null;
		break;
		case 108:
			m = dev.mail8 ? dev.mail8.split(';') : null;
		break;
		case 109:
			m = dev.mail9 ? dev.mail9.split(';') : null;
		break;
		case 110:
			m = dev.mail10 ? dev.mail10.split(';') : null;
		break;
		case 111:
			m = dev.mail11 ? dev.mail11.split(';') : null;
		break;
		case 112:
			m = dev.mail12 ? dev.mail12.split(';') : null;
		break;
		case 113:
			m = dev.mail13 ? dev.mail13.split(';') : null;
		break;
		case 114:
			m = dev.mail14 ? dev.mail14.split(';') : null;
		break;
		case 115:
			m = dev.mail15 ? dev.mail15.split(';') : null;
		break;
		case 116:
			m = dev.mail16 ? dev.mail16.split(';') : null;
		break;
		case 117:
			m = dev.mail17 ? dev.mail17.split(';') : null;
		break;
		case 118:
			m = dev.mail18 ? dev.mail18.split(';') : null;
		break;
		case 119:
			m = dev.mail19 ? dev.mail19.split(';') : null;
		break;
		case 120:
			m = dev.mail20 ? dev.mail20.split(';') : null;
		break;
		case 121:
			m = dev.mail21 ? dev.mail21.split(';') : null;
		break;
		case 122:
			m = dev.mail22 ? dev.mail22.split(';') : null;
		break;
		case 123:
			m = dev.mail23 ? dev.mail23.split(';') : null;
		break;
		case 124:
			m = dev.mail24 ? dev.mail24.split(';') : null;
		break;
		case 125:
			m = dev.mail25 ? dev.mail25.split(';') : null;
		break;
		case 126:
			m = dev.mail26 ? dev.mail26.split(';') : null;
		break;
		default:
			m = dev.mail ? dev.mail.split(';') : null;
		break;
	}
	console.log(m);
	return m;
	
};

function find_event_name(code){
	var m = '';
	switch(code){
		case 9999:
			m = 'ACCENSIONE';
		break;
		case 100:
			m = 'KEY';
		break;
		case 101:
			m = 'BLINK';
		break;
		case 102:
			m = 'POWER';
		break;
		case 103:
			m = 'ALARM';
		break;
		case 104:
			m = 'STOP';
		break;
		case 105:
			m = 'GO';
		break;
		case 107:
			m = 'JAM';
		break;
		case 108:
			m = 'WARMUP';
		break;
		case 109:
			m = 'WARMDOWN';
		break;
		case 110:
			m = 'FLUTTER';
		break;
		case 111:
			m = 'TILT';
		break;
		case 112:
			m = 'VELOVER';
		break;
		case 113:
			m = 'VELLOW';
		break;
		case 114:
			m = 'ALTOVER';
		break;
		case 115:
			m = 'CRASH';
		break;
		case 116:
			m = 'OUT1';
		break;
		case 117:
			m = 'OUT2';
		break;
		case 118:
			m = 'OUT3';
		break;
		case 119:
			m = 'FENCE1';
		break;
		case 120:
			m = 'FENCE2';
		break;
		case 121:
			m = 'FENCE3';
		break;
		case 122:
			m = 'EXIT F1';
		break;
		case 123:
			m = 'EXIT F2';
		break;
		case 124:
			m = 'EXIT F3';
		break;
		case 125:
			m = 'CRASH 2';
		break;
		case 126:
			m = 'LOW BATTERY';
		break;
		default:
			m = '';
		break;
	}
	return m;
	
};

function find_duration_event(parameter,code){
	var m = '';
	if(parameter){
		switch(code){
			case 100:
				m = 'osservato da '+parameter.dur0 +' sec';
			break;
			case 101:
				m = 'osservato da '+parameter.dur1 +' sec';
			break;
			case 102:
				m = 'osservato da '+parameter.dur2 +' sec';
			break;
			case 103:
				m = 'osservato da '+parameter.dur3 +' sec';
			break;
			case 104:
				m = 'osservato da '+parameter.dur4 +' sec';
			break;
			case 105:
				m = 'osservato da '+parameter.dur5 +' sec';
			break;
			case 107:
				m = 'osservato da '+parameter.dur7 +' sec';
			break;
			case 108:
				m = 'osservato da '+parameter.dur8 +' sec';
			break;
			case 109:
				m = 'osservato da '+parameter.dur9 +' sec';
			break;
			case 110:
				m = 'osservato da '+parameter.dur10 +' sec';
			break;
			case 111:
				m = 'osservato da '+parameter.dur11 +' sec';
			break;
			case 112:
				m = 'osservato da '+parameter.dur12+' sec';
			break;
			case 113:
				m = 'osservato da '+parameter.dur13 +' sec';
			break;
			case 114:
				m = 'osservato da '+parameter.dur14 +' sec';
			break;
			case 115:
				m = 'osservato da '+parameter.dur15 +' sec';
			break;
			case 116:
				m = 'osservato da '+parameter.dur16 +' sec';
			break;
			case 117:
				m = 'osservato da '+parameter.dur17 +' sec';
			break;
			case 118:
				m = 'osservato da '+parameter.dur18 +' sec';
			break;
			case 119:
				m = 'osservato da '+parameter.dur19 +' sec';
			break;
			case 120:
				m = 'osservato da '+parameter.dur20 +' sec';
			break;
			case 121:
				m = 'osservato da '+parameter.dur21 +' sec';
			break;
			case 122:
				m = 'osservato da '+parameter.dur22 +' sec';
			break;
			case 123:
				m = 'osservato da '+parameter.dur23 +' sec';
			break;
			case 124:
				m = 'osservato da '+parameter.dur24 +' sec';
			break;
			case 125:
				m = 'osservato da '+parameter.dur25 +' sec';
			break;
			case 126:
				m = 'osservato da '+parameter.dur26 +' sec';
			break;
			default:
				m = '';
			break;
		}
	}
	
	return m;
	
};

function convert_to_google_coord(gpsx, gpsy, ns, ew){

	s = gpsx + ","+ns+","+gpsy+","+ew;
	//s = '4501.93701171875,N,738.6063842773438,E'
	g = s.split(",");

	if(parseInt(g[0])<10){
		g[0] = '000'+g[0];
	}
	else if(parseInt(g[0])<100){
		g[0] = '00'+g[0];
	}
	else if(parseInt(g[0]) <1000){
		g[0] = '0'+g[0];
	}

	gradi_lat = g[0].substr(0, 2);
	if(g[1] == 'N'){
		lat = parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60);
	}
	else
	{
		lat = -1 * (parseInt(gradi_lat) + (parseFloat(g[0].substr(2,8))/60));
	}

	if(parseInt(g[2])<10){
		g[2] = '0000'+g[2];
	}
	else if(parseInt(g[2])<100){
		g[2] = '000'+g[2];
	}
	else if(parseInt(g[2]) <1000){
		g[2] = '00'+g[2];
	}
	else
	{
		g[2] = '0'+g[2];
	}

	gradi_lon = g[2].substr(0, 3);
	if(g[3] == 'W'){
		lon = -1 * (parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60));
	}
	else
	{
		lon = parseInt(gradi_lon) + (parseFloat(g[2].substr(3,8))/60);
	}
	return lat.toFixed(8)+","+lon.toFixed(8);


};
